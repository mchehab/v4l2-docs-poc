V4L2\_PIX\_FMT\_NV12 ('NV12'), V4L2\_PIX\_FMT\_NV21 ('NV21')
MANVOL
V4L2\_PIX\_FMT\_NV12
V4L2\_PIX\_FMT\_NV21
Formats with ½ horizontal and vertical chroma resolution, also known as
YUV 4:2:0. One luminance and one chrominance plane with alternating
chroma samples as opposed to
V4L2\_PIX\_FMT\_YVU420
Description
===========

These are two-plane versions of the YUV 4:2:0 format. The three
components are separated into two sub-images or planes. The Y plane is
first. The Y plane has one byte per pixel. For ``V4L2_PIX_FMT_NV12``, a
combined CbCr plane immediately follows the Y plane in memory. The CbCr
plane is the same width, in bytes, as the Y plane (and of the image),
but is half as tall in pixels. Each CbCr pair belongs to four pixels.
For example, Cb\ :sub:`0`/Cr:sub:`0` belongs to Y'\ :sub:`00`,
Y'\ :sub:`01`, Y'\ :sub:`10`, Y'\ :sub:`11`. ``V4L2_PIX_FMT_NV21`` is
the same except the Cb and Cr bytes are swapped, the CrCb plane starts
with a Cr byte.

If the Y plane has pad bytes after each row, then the CbCr plane has as
many pad bytes after its rows.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 4:                                                               |
+--------------------------------------------------------------------------+
| start + 8:                                                               |
+--------------------------------------------------------------------------+
| start + 12:                                                              |
+--------------------------------------------------------------------------+
| start + 16:                                                              |
+--------------------------------------------------------------------------+
| start + 20:                                                              |
+--------------------------------------------------------------------------+

**Color Sample Location..**

+-----+-----+-----+-----+----+-----+-----+-----+
|     | 0   |     | 1   |    | 2   |     | 3   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 0   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
|     |     | C   |     |    |     | C   |     |
+-----+-----+-----+-----+----+-----+-----+-----+
| 1   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
+-----+-----+-----+-----+----+-----+-----+-----+
| 2   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
|     |     | C   |     |    |     | C   |     |
+-----+-----+-----+-----+----+-----+-----+-----+
| 3   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
