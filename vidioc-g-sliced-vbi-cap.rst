ioctl VIDIOC\_G\_SLICED\_VBI\_CAP
MANVOL
VIDIOC\_G\_SLICED\_VBI\_CAP
Query sliced VBI capabilities
int
ioctl
int
fd
int
request
struct v4l2\_sliced\_vbi\_cap \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_SLICED\_VBI\_CAP

``argp``

Description
===========

To find out which data services are supported by a sliced VBI capture or
output device, applications initialize the type field of a
V4L2-SLICED-VBI-CAP, clear the reserved array and call the
``VIDIOC_G_SLICED_VBI_CAP`` ioctl. The driver fills in the remaining
fields or returns an EINVAL if the sliced VBI API is unsupported or type
is invalid.

Note the type field was added, and the ioctl changed from read-only to
write-read, in Linux 2.6.19.

+--------------------+--------------------+--------------+--------------+--------------+
| \_\_u16            | service\_set       | A set of all |
|                    |                    | data         |
|                    |                    | services     |
|                    |                    | supported by |
|                    |                    | the driver.  |
|                    |                    | Equal to the |
|                    |                    | union of all |
|                    |                    | elements of  |
|                    |                    | the          |
|                    |                    | service\_lin |
|                    |                    | es           |
|                    |                    | array.       |
+--------------------+--------------------+--------------+--------------+--------------+
| \_\_u16            | service\_lines[2][ | Each element |
|                    | 24]                | of this      |
|                    |                    | array        |
|                    |                    | contains a   |
|                    |                    | set of data  |
|                    |                    | services the |
|                    |                    | hardware can |
|                    |                    | look for or  |
|                    |                    | insert into  |
|                    |                    | a particular |
|                    |                    | scan line.   |
|                    |                    | Data         |
|                    |                    | services are |
|                    |                    | defined in   |
|                    |                    | ?. Array     |
|                    |                    | indices map  |
|                    |                    | to ITU-R     |
|                    |                    | line numbers |
|                    |                    | (see also ?  |
|                    |                    | and ?) as    |
|                    |                    | follows:     |
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | Element      | 525 line     | 625 line     |
|                    |                    |              | systems      | systems      |
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | service\_lin | 1            | 1            |
|                    |                    | es[0][1]     |              |              |
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | service\_lin | 23           | 23           |
|                    |                    | es[0][23]    |              |              |
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | service\_lin | 264          | 314          |
|                    |                    | es[1][1]     |              |              |
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | service\_lin | 286          | 336          |
|                    |                    | es[1][23]    |              |              |
+--------------------+--------------------+--------------+--------------+--------------+
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | The number   |
|                    |                    | of VBI lines |
|                    |                    | the hardware |
|                    |                    | can capture  |
|                    |                    | or output    |
|                    |                    | per frame,   |
|                    |                    | or the       |
|                    |                    | number of    |
|                    |                    | services it  |
|                    |                    | can identify |
|                    |                    | on a given   |
|                    |                    | line may be  |
|                    |                    | limited. For |
|                    |                    | example on   |
|                    |                    | PAL line 16  |
|                    |                    | the hardware |
|                    |                    | may be able  |
|                    |                    | to look for  |
|                    |                    | a VPS or     |
|                    |                    | Teletext     |
|                    |                    | signal, but  |
|                    |                    | not both at  |
|                    |                    | the same     |
|                    |                    | time.        |
|                    |                    | Applications |
|                    |                    | can learn    |
|                    |                    | about these  |
|                    |                    | limits using |
|                    |                    | the          |
|                    |                    | VIDIOC-S-FMT |
|                    |                    | ioctl as     |
|                    |                    | described in |
|                    |                    | ?.           |
+--------------------+--------------------+--------------+--------------+--------------+
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | Drivers must |
|                    |                    | set          |
|                    |                    | service\_lin |
|                    |                    | es[0][0]     |
|                    |                    | and          |
|                    |                    | service\_lin |
|                    |                    | es[1][0]     |
|                    |                    | to zero.     |
+--------------------+--------------------+--------------+--------------+--------------+
| \_\_u32            | type               | Type of the  |
|                    |                    | data stream, |
|                    |                    | see ?.       |
|                    |                    | Should be    |
|                    |                    | ``V4L2_BUF_T |
|                    |                    | YPE_SLICED_V |
|                    |                    | BI_CAPTURE`` |
|                    |                    | or           |
|                    |                    | ``V4L2_BUF_T |
|                    |                    | YPE_SLICED_V |
|                    |                    | BI_OUTPUT``. |
+--------------------+--------------------+--------------+--------------+--------------+
| \_\_u32            | reserved[3]        | This array   |
|                    |                    | is reserved  |
|                    |                    | for future   |
|                    |                    | extensions.  |
|                    |                    | Applications |
|                    |                    | and drivers  |
|                    |                    | must set it  |
|                    |                    | to zero.     |
+--------------------+--------------------+--------------+--------------+--------------+

Table: struct v4l2\_sliced\_vbi\_cap

+--------------------+-----------+-----------+--------------------+--------------------+
| Symbol             | Value     | Reference | Lines, usually     | Payload            |
+====================+===========+===========+====================+====================+
| ``V4L2_SLICED_TELE | 0x0001    | ?, ?      | PAL/SECAM line     | Last 42 of the 45  |
| TEXT_B``           |           |           | 7-22, 320-335      | byte Teletext      |
| (Teletext System   |           |           | (second field      | packet, that is    |
| B)                 |           |           | 7-22)              | without clock      |
|                    |           |           |                    | run-in and framing |
|                    |           |           |                    | code, lsb first    |
|                    |           |           |                    | transmitted.       |
+--------------------+-----------+-----------+--------------------+--------------------+
| ``V4L2_SLICED_VPS` | 0x0400    | ?         | PAL line 16        | Byte number 3 to   |
| `                  |           |           |                    | 15 according to    |
|                    |           |           |                    | Figure 9 of        |
|                    |           |           |                    | ETS 300 231, lsb   |
|                    |           |           |                    | first transmitted. |
+--------------------+-----------+-----------+--------------------+--------------------+
| ``V4L2_SLICED_CAPT | 0x1000    | ?         | NTSC line 21, 284  | Two bytes in       |
| ION_525``          |           |           | (second field 21)  | transmission       |
|                    |           |           |                    | order, including   |
|                    |           |           |                    | parity bit, lsb    |
|                    |           |           |                    | first transmitted. |
+--------------------+-----------+-----------+--------------------+--------------------+
| ``V4L2_SLICED_WSS_ | 0x4000    | ?, ?      | PAL/SECAM line 23  | ::                 |
| 625``              |           |           |                    |                    |
|                    |           |           |                    |     Byte        0  |
|                    |           |           |                    |                 1  |
|                    |           |           |                    |          msb       |
|                    |           |           |                    |    lsb  msb        |
|                    |           |           |                    |     lsb            |
|                    |           |           |                    |     Bit  7 6 5 4 3 |
|                    |           |           |                    |  2 1 0  x x 13 12  |
|                    |           |           |                    | 11 10 9            |
+--------------------+-----------+-----------+--------------------+--------------------+
| ``V4L2_SLICED_VBI_ | 0x1000    | Set of    |
| 525``              |           | services  |
|                    |           | applicabl |
|                    |           | e         |
|                    |           | to 525    |
|                    |           | line      |
|                    |           | systems.  |
+--------------------+-----------+-----------+--------------------+--------------------+
| ``V4L2_SLICED_VBI_ | 0x4401    | Set of    |
| 625``              |           | services  |
|                    |           | applicabl |
|                    |           | e         |
|                    |           | to 625    |
|                    |           | line      |
|                    |           | systems.  |
+--------------------+-----------+-----------+--------------------+--------------------+

Table: Sliced VBI services

RETURN-VALUE

EINVAL
    The value in the type field is wrong.
