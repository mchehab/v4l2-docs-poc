V4L2\_PIX\_FMT\_SBGGR10ALAW8 ('aBA8'), V4L2\_PIX\_FMT\_SGBRG10ALAW8
('aGA8'), V4L2\_PIX\_FMT\_SGRBG10ALAW8 ('agA8'),
V4L2\_PIX\_FMT\_SRGGB10ALAW8 ('aRA8'),
MANVOL
V4L2\_PIX\_FMT\_SBGGR10ALAW8
V4L2\_PIX\_FMT\_SGBRG10ALAW8
V4L2\_PIX\_FMT\_SGRBG10ALAW8
V4L2\_PIX\_FMT\_SRGGB10ALAW8
10-bit Bayer formats compressed to 8 bits
Description
===========

These four pixel formats are raw sRGB / Bayer formats with 10 bits per
color compressed to 8 bits each, using the A-LAW algorithm. Each color
component consumes 8 bits of memory. In other respects this format is
similar to ?.
