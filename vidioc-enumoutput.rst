ioctl VIDIOC\_ENUMOUTPUT
MANVOL
VIDIOC\_ENUMOUTPUT
Enumerate video outputs
int
ioctl
int
fd
int
request
struct v4l2\_output \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_ENUMOUTPUT

``argp``

Description
===========

To query the attributes of a video outputs applications initialize the
index field of V4L2-OUTPUT and call the ``VIDIOC_ENUMOUTPUT`` ioctl with
a pointer to this structure. Drivers fill the rest of the structure or
return an EINVAL when the index is out of bounds. To enumerate all
outputs applications shall begin at index zero, incrementing by one
until the driver returns EINVAL.

+---------------+----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32       | index          | Identifies the output, set by the application.                                                                                                                                                                                                                                                               |
+---------------+----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u8        | name[32]       | Name of the video output, a NUL-terminated ASCII string, for example: "Vout". This information is intended for the user, preferably the connector label on the device itself.                                                                                                                                |
+---------------+----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32       | type           | Type of the output, see ?.                                                                                                                                                                                                                                                                                   |
+---------------+----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32       | audioset       | Drivers can enumerate up to 32 video and audio outputs. This field shows which audio outputs were selectable as the current output if this was the currently selected video output. It is a bit mask. The LSB corresponds to audio output 0, the MSB to output 31. Any number of bits can be set, or none.   |
|               |                |                                                                                                                                                                                                                                                                                                              |
|               |                | When the driver does not enumerate audio outputs no bits must be set. Applications shall not interpret this as lack of audio support. Drivers may automatically select audio outputs without enumerating them.                                                                                               |
|               |                |                                                                                                                                                                                                                                                                                                              |
|               |                | For details on audio outputs and how to select the current output see ?.                                                                                                                                                                                                                                     |
+---------------+----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32       | modulator      | Output devices can have zero or more RF modulators. When the type is ``V4L2_OUTPUT_TYPE_MODULATOR`` this is an RF connector and this field identifies the modulator. It corresponds to V4L2-MODULATOR field index. For details on modulators see ?.                                                          |
+---------------+----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| V4L2-STD-ID   | std            | Every video output supports one or more different video standards. This field is a set of all supported standards. For details on video standards and how to switch see ?.                                                                                                                                   |
+---------------+----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32       | capabilities   | This field provides capabilities for the output. See ? for flags.                                                                                                                                                                                                                                            |
+---------------+----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32       | reserved[3]    | Reserved for future extensions. Drivers must set the array to zero.                                                                                                                                                                                                                                          |
+---------------+----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_output

+-----------------------------------------+-----+-----------------------------------------------------------------------+
| ``V4L2_OUTPUT_TYPE_MODULATOR``          | 1   | This output is an analog TV modulator.                                |
+-----------------------------------------+-----+-----------------------------------------------------------------------+
| ``V4L2_OUTPUT_TYPE_ANALOG``             | 2   | Analog baseband output, for example Composite / CVBS, S-Video, RGB.   |
+-----------------------------------------+-----+-----------------------------------------------------------------------+
| ``V4L2_OUTPUT_TYPE_ANALOGVGAOVERLAY``   | 3   | [?]                                                                   |
+-----------------------------------------+-----+-----------------------------------------------------------------------+

Table: Output Type

+--------------------------------+--------------+----------------------------------------------------------------------------------------------------------------+
| ``V4L2_OUT_CAP_DV_TIMINGS``    | 0x00000002   | This output supports setting video timings by using VIDIOC\_S\_DV\_TIMINGS.                                    |
+--------------------------------+--------------+----------------------------------------------------------------------------------------------------------------+
| ``V4L2_OUT_CAP_STD``           | 0x00000004   | This output supports setting the TV standard by using VIDIOC\_S\_STD.                                          |
+--------------------------------+--------------+----------------------------------------------------------------------------------------------------------------+
| ``V4L2_OUT_CAP_NATIVE_SIZE``   | 0x00000008   | This output supports setting the native size using the ``V4L2_SEL_TGT_NATIVE_SIZE`` selection target, see ?.   |
+--------------------------------+--------------+----------------------------------------------------------------------------------------------------------------+

Table: Output capabilities

RETURN-VALUE

EINVAL
    The V4L2-OUTPUT index is out of bounds.
