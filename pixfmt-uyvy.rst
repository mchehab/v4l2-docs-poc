V4L2\_PIX\_FMT\_UYVY ('UYVY')
MANVOL
V4L2\_PIX\_FMT\_UYVY
Variation of
V4L2\_PIX\_FMT\_YUYV
with different order of samples in memory
Description
===========

In this format each four bytes is two pixels. Each four bytes is two
Y's, a Cb and a Cr. Each Y goes to one of the pixels, and the Cb and Cr
belong to both pixels. As you can see, the Cr and Cb components have
half the horizontal resolution of the Y component.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 8:                                                               |
+--------------------------------------------------------------------------+
| start + 16:                                                              |
+--------------------------------------------------------------------------+
| start + 24:                                                              |
+--------------------------------------------------------------------------+

**Color Sample Location..**

+-----+-----+-----+-----+----+-----+-----+-----+
|     | 0   |     | 1   |    | 2   |     | 3   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 0   | Y   | C   | Y   |    | Y   | C   | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 1   | Y   | C   | Y   |    | Y   | C   | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 2   | Y   | C   | Y   |    | Y   | C   | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 3   | Y   | C   | Y   |    | Y   | C   | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
