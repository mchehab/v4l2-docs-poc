ioctl VIDIOC\_ENUMAUDIO
MANVOL
VIDIOC\_ENUMAUDIO
Enumerate audio inputs
int
ioctl
int
fd
int
request
struct v4l2\_audio \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_ENUMAUDIO

``argp``

Description
===========

To query the attributes of an audio input applications initialize the
index field and zero out the reserved array of a V4L2-AUDIO and call the
``VIDIOC_ENUMAUDIO`` ioctl with a pointer to this structure. Drivers
fill the rest of the structure or return an EINVAL when the index is out
of bounds. To enumerate all audio inputs applications shall begin at
index zero, incrementing by one until the driver returns EINVAL.

See ? for a description of V4L2-AUDIO.

RETURN-VALUE

EINVAL
    The number of the audio input is out of bounds.
