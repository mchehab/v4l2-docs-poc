ioctl VIDIOC\_QUERY\_DV\_TIMINGS
MANVOL
VIDIOC\_QUERY\_DV\_TIMINGS
VIDIOC\_SUBDEV\_QUERY\_DV\_TIMINGS
Sense the DV preset received by the current input
int
ioctl
int
fd
int
request
struct v4l2\_dv\_timings \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_QUERY\_DV\_TIMINGS, VIDIOC\_SUBDEV\_QUERY\_DV\_TIMINGS

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

The hardware may be able to detect the current DV timings automatically,
similar to sensing the video standard. To do so, applications call
``VIDIOC_QUERY_DV_TIMINGS`` with a pointer to a V4L2-DV-TIMINGS. Once
the hardware detects the timings, it will fill in the timings structure.

Please note that drivers shall *not* switch timings automatically if new
timings are detected. Instead, drivers should send the
``V4L2_EVENT_SOURCE_CHANGE`` event (if they support this) and expect
that userspace will take action by calling ``VIDIOC_QUERY_DV_TIMINGS``.
The reason is that new timings usually mean different buffer sizes as
well, and you cannot change buffer sizes on the fly. In general,
applications that receive the Source Change event will have to call
``VIDIOC_QUERY_DV_TIMINGS``, and if the detected timings are valid they
will have to stop streaming, set the new timings, allocate new buffers
and start streaming again.

If the timings could not be detected because there was no signal, then
ENOLINK is returned. If a signal was detected, but it was unstable and
the receiver could not lock to the signal, then ENOLCK is returned. If
the receiver could lock to the signal, but the format is unsupported
(e.g. because the pixelclock is out of range of the hardware
capabilities), then the driver fills in whatever timings it could find
and returns ERANGE. In that case the application can call
VIDIOC-DV-TIMINGS-CAP to compare the found timings with the hardware's
capabilities in order to give more precise feedback to the user.

RETURN-VALUE

ENODATA
    Digital video timings are not supported for this input or output.

ENOLINK
    No timings could be detected because no signal was found.

ENOLCK
    The signal was unstable and the hardware could not lock on to it.

ERANGE
    Timings were found, but they are out of range of the hardware
    capabilities.
