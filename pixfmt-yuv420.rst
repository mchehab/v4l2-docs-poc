V4L2\_PIX\_FMT\_YVU420 ('YV12'), V4L2\_PIX\_FMT\_YUV420 ('YU12')
MANVOL
V4L2\_PIX\_FMT\_YVU420
V4L2\_PIX\_FMT\_YUV420
Planar formats with ½ horizontal and vertical chroma resolution, also
known as YUV 4:2:0
Description
===========

These are planar formats, as opposed to a packed format. The three
components are separated into three sub- images or planes. The Y plane
is first. The Y plane has one byte per pixel. For
``V4L2_PIX_FMT_YVU420``, the Cr plane immediately follows the Y plane in
memory. The Cr plane is half the width and half the height of the Y
plane (and of the image). Each Cr belongs to four pixels, a two-by-two
square of the image. For example, Cr\ :sub:`0` belongs to Y'\ :sub:`00`,
Y'\ :sub:`01`, Y'\ :sub:`10`, and Y'\ :sub:`11`. Following the Cr plane
is the Cb plane, just like the Cr plane. ``V4L2_PIX_FMT_YUV420`` is the
same except the Cb plane comes first, then the Cr plane.

If the Y plane has pad bytes after each row, then the Cr and Cb planes
have half as many pad bytes after their rows. In other words, two Cx
rows (including padding) is exactly as long as one Y row (including
padding).

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 4:                                                               |
+--------------------------------------------------------------------------+
| start + 8:                                                               |
+--------------------------------------------------------------------------+
| start + 12:                                                              |
+--------------------------------------------------------------------------+
| start + 16:                                                              |
+--------------------------------------------------------------------------+
| start + 18:                                                              |
+--------------------------------------------------------------------------+
| start + 20:                                                              |
+--------------------------------------------------------------------------+
| start + 22:                                                              |
+--------------------------------------------------------------------------+

**Color Sample Location..**

+-----+-----+-----+-----+----+-----+-----+-----+
|     | 0   |     | 1   |    | 2   |     | 3   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 0   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
|     |     | C   |     |    |     | C   |     |
+-----+-----+-----+-----+----+-----+-----+-----+
| 1   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
+-----+-----+-----+-----+----+-----+-----+-----+
| 2   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
|     |     | C   |     |    |     | C   |     |
+-----+-----+-----+-----+----+-----+-----+-----+
| 3   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
