ioctl VIDIOC\_QUERYBUF
MANVOL
VIDIOC\_QUERYBUF
Query the status of a buffer
int
ioctl
int
fd
int
request
struct v4l2\_buffer \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_QUERYBUF

``argp``

Description
===========

This ioctl is part of the `streaming <#mmap>`__ I/O method. It can be
used to query the status of a buffer at any time after buffers have been
allocated with the VIDIOC-REQBUFS ioctl.

Applications set the type field of a V4L2-BUFFER to the same buffer type
as was previously used with V4L2-FORMAT type and V4L2-REQUESTBUFFERS
type, and the index field. Valid index numbers range from zero to the
number of buffers allocated with VIDIOC-REQBUFS (V4L2-REQUESTBUFFERS
count) minus one. The reserved and reserved2 fields must be set to 0.
When using the `multi-planar API <#planar-apis>`__, the m.planes field
must contain a userspace pointer to an array of V4L2-PLANE and the
length field has to be set to the number of elements in that array.
After calling ``VIDIOC_QUERYBUF`` with a pointer to this structure
drivers return an error code or fill the rest of the structure.

In the flags field the ``V4L2_BUF_FLAG_MAPPED``,
``V4L2_BUF_FLAG_PREPARED``, ``V4L2_BUF_FLAG_QUEUED`` and
``V4L2_BUF_FLAG_DONE`` flags will be valid. The memory field will be set
to the current I/O method. For the single-planar API, the m.offset
contains the offset of the buffer from the start of the device memory,
the length field its size. For the multi-planar API, fields
m.mem\_offset and length in the m.planes array elements will be used
instead and the length field of V4L2-BUFFER is set to the number of
filled-in array elements. The driver may or may not set the remaining
fields and flags, they are meaningless in this context.

The v4l2\_buffer structure is specified in ?.

RETURN-VALUE

EINVAL
    The buffer type is not supported, or the index is out of bounds.
