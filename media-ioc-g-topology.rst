ioctl MEDIA\_IOC\_G\_TOPOLOGY
MANVOL
MEDIA\_IOC\_G\_TOPOLOGY
Enumerate the graph topology and graph element properties
int
ioctl
int
fd
int
request
struct media\_v2\_topology \*
argp
Arguments
=========

``fd``
    File descriptor returned by ```open()`` <#media-func-open>`__.

``request``
    MEDIA\_IOC\_G\_TOPOLOGY

``argp``

Description
===========

The typical usage of this ioctl is to call it twice. On the first call,
the structure defined at MEDIA-V2-TOPOLOGY should be zeroed. At return,
if no errors happen, this ioctl will return the ``topology_version`` and
the total number of entities, interfaces, pads and links.

Before the second call, the userspace should allocate arrays to store
the graph elements that are desired, putting the pointers to them at the
ptr\_entities, ptr\_interfaces, ptr\_links and/or ptr\_pads, keeping the
other values untouched.

If the ``topology_version`` remains the same, the ioctl should fill the
desired arrays with the media graph elements.

+-----------+---------------------+----+----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u64   | topology\_version   |    |    | Version of the media graph topology. When the graph is created, this field starts with zero. Every time a graph element is added or removed, this field is incremented.                                       |
+-----------+---------------------+----+----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u64   | num\_entities       |    |    | Number of entities in the graph                                                                                                                                                                               |
+-----------+---------------------+----+----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u64   | ptr\_entities       |    |    | A pointer to a memory area where the entities array will be stored, converted to a 64-bits integer. It can be zero. if zero, the ioctl won't store the entities. It will just update ``num_entities``         |
+-----------+---------------------+----+----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u64   | num\_interfaces     |    |    | Number of interfaces in the graph                                                                                                                                                                             |
+-----------+---------------------+----+----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u64   | ptr\_interfaces     |    |    | A pointer to a memory area where the interfaces array will be stored, converted to a 64-bits integer. It can be zero. if zero, the ioctl won't store the interfaces. It will just update ``num_interfaces``   |
+-----------+---------------------+----+----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u64   | num\_pads           |    |    | Total number of pads in the graph                                                                                                                                                                             |
+-----------+---------------------+----+----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u64   | ptr\_pads           |    |    | A pointer to a memory area where the pads array will be stored, converted to a 64-bits integer. It can be zero. if zero, the ioctl won't store the pads. It will just update ``num_pads``                     |
+-----------+---------------------+----+----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u64   | num\_links          |    |    | Total number of data and interface links in the graph                                                                                                                                                         |
+-----------+---------------------+----+----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u64   | ptr\_links          |    |    | A pointer to a memory area where the links array will be stored, converted to a 64-bits integer. It can be zero. if zero, the ioctl won't store the links. It will just update ``num_links``                  |
+-----------+---------------------+----+----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct media\_v2\_topology

+-----------+----------------+-----------------------------------------------------------------------------------------+----+---------------------------------------------------+
| \_\_u32   | id             |                                                                                         |    | Unique ID for the entity.                         |
+-----------+----------------+-----------------------------------------------------------------------------------------+----+---------------------------------------------------+
| char      | name[64]       |                                                                                         |    | Entity name as an UTF-8 NULL-terminated string.   |
+-----------+----------------+-----------------------------------------------------------------------------------------+----+---------------------------------------------------+
| \_\_u32   | function       |                                                                                         |    | Entity main function, see ? for details.          |
+-----------+----------------+-----------------------------------------------------------------------------------------+----+---------------------------------------------------+
| \_\_u32   | reserved[12]   | Reserved for future extensions. Drivers and applications must set this array to zero.   |
+-----------+----------------+-----------------------------------------------------------------------------------------+----+---------------------------------------------------+

Table: struct media\_v2\_entity

+-----------------------------------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32                           | id            |    |    | Unique ID for the interface.                                                            |
+-----------------------------------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32                           | intf\_type    |    |    | Interface type, see ? for details.                                                      |
+-----------------------------------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32                           | flags         |    |    | Interface flags. Currently unused.                                                      |
+-----------------------------------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32                           | reserved[9]   |    |    | Reserved for future extensions. Drivers and applications must set this array to zero.   |
+-----------------------------------+---------------+----+----+-----------------------------------------------------------------------------------------+
| struct media\_v2\_intf\_devnode   | devnode       |    |    | Used only for device node interfaces. See ? for details..                               |
+-----------------------------------+---------------+----+----+-----------------------------------------------------------------------------------------+

Table: struct media\_v2\_interface

+-----------+---------+----+----+-----------------------------+
| \_\_u32   | major   |    |    | Device node major number.   |
+-----------+---------+----+----+-----------------------------+
| \_\_u32   | minor   |    |    | Device node minor number.   |
+-----------+---------+----+----+-----------------------------+

Table: struct media\_v2\_interface

+-----------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32   | id            |    |    | Unique ID for the pad.                                                                  |
+-----------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32   | entity\_id    |    |    | Unique ID for the entity where this pad belongs.                                        |
+-----------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32   | flags         |    |    | Pad flags, see ? for more details.                                                      |
+-----------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32   | reserved[9]   |    |    | Reserved for future extensions. Drivers and applications must set this array to zero.   |
+-----------+---------------+----+----+-----------------------------------------------------------------------------------------+

Table: struct media\_v2\_pad

+-----------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32   | id            |    |    | Unique ID for the pad.                                                                  |
+-----------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32   | source\_id    |    |    | On pad to pad links: unique ID for the source pad.                                      |
|           |               |    |    |                                                                                         |
|           |               |    |    | On interface to entity links: unique ID for the interface.                              |
+-----------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32   | sink\_id      |    |    | On pad to pad links: unique ID for the sink pad.                                        |
|           |               |    |    |                                                                                         |
|           |               |    |    | On interface to entity links: unique ID for the entity.                                 |
+-----------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32   | flags         |    |    | Link flags, see ? for more details.                                                     |
+-----------+---------------+----+----+-----------------------------------------------------------------------------------------+
| \_\_u32   | reserved[5]   |    |    | Reserved for future extensions. Drivers and applications must set this array to zero.   |
+-----------+---------------+----+----+-----------------------------------------------------------------------------------------+

Table: struct media\_v2\_pad

RETURN-VALUE

ENOSPC
    This is returned when either one or more of the num\_entities,
    num\_interfaces, num\_links or num\_pads are non-zero and are
    smaller than the actual number of elements inside the graph. This
    may happen if the ``topology_version`` changed when compared to the
    last time this ioctl was called. Userspace should usually free the
    area for the pointers, zero the struct elements and call this ioctl
    again.
