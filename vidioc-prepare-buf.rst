ioctl VIDIOC\_PREPARE\_BUF
MANVOL
VIDIOC\_PREPARE\_BUF
Prepare a buffer for I/O
int
ioctl
int
fd
int
request
struct v4l2\_buffer \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_PREPARE\_BUF

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

Applications can optionally call the ``VIDIOC_PREPARE_BUF`` ioctl to
pass ownership of the buffer to the driver before actually enqueuing it,
using the ``VIDIOC_QBUF`` ioctl, and to prepare it for future I/O. Such
preparations may include cache invalidation or cleaning. Performing them
in advance saves time during the actual I/O. In case such cache
operations are not required, the application can use one of
``V4L2_BUF_FLAG_NO_CACHE_INVALIDATE`` and
``V4L2_BUF_FLAG_NO_CACHE_CLEAN`` flags to skip the respective step.

The v4l2\_buffer structure is specified in ?.

RETURN-VALUE

EBUSY
    File I/O is in progress.

EINVAL
    The buffer type is not supported, or the index is out of bounds, or
    no buffers have been allocated yet, or the userptr or length are
    invalid.
