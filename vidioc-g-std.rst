ioctl VIDIOC\_G\_STD, VIDIOC\_S\_STD
MANVOL
VIDIOC\_G\_STD
VIDIOC\_S\_STD
Query or select the video standard of the current input
int
ioctl
int
fd
int
request
v4l2\_std\_id \*
argp
int
ioctl
int
fd
int
request
const v4l2\_std\_id \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_STD, VIDIOC\_S\_STD

``argp``

Description
===========

To query and select the current video standard applications use the
``VIDIOC_G_STD`` and ``VIDIOC_S_STD`` ioctls which take a pointer to a
V4L2-STD-ID type as argument. ``VIDIOC_G_STD`` can return a single flag
or a set of flags as in V4L2-STANDARD field id. The flags must be
unambiguous such that they appear in only one enumerated v4l2\_standard
structure.

``VIDIOC_S_STD`` accepts one or more flags, being a write-only ioctl it
does not return the actual new standard as ``VIDIOC_G_STD`` does. When
no flags are given or the current input does not support the requested
standard the driver returns an EINVAL. When the standard set is
ambiguous drivers may return EINVAL or choose any of the requested
standards. If the current input or output does not support standard
video timings (e.g. if VIDIOC-ENUMINPUT does not set the
``V4L2_IN_CAP_STD`` flag), then ENODATA is returned.

RETURN-VALUE

EINVAL
    The ``VIDIOC_S_STD`` parameter was unsuitable.

ENODATA
    Standard video timings are not supported for this input or output.
