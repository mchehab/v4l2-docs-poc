V4L2\_PIX\_FMT\_Y41P ('Y41P')
MANVOL
V4L2\_PIX\_FMT\_Y41P
Format with ¼ horizontal chroma resolution, also known as YUV 4:1:1
Description
===========

In this format each 12 bytes is eight pixels. In the twelve bytes are
two CbCr pairs and eight Y's. The first CbCr pair goes with the first
four Y's, and the second CbCr pair goes with the other four Y's. The Cb
and Cr components have one fourth the horizontal resolution of the Y
component.

Do not confuse this format with
```V4L2_PIX_FMT_YUV411P`` <#V4L2-PIX-FMT-YUV411P>`__. Y41P is derived
from "YUV 4:1:1 *packed*", while YUV411P stands for "YUV 4:1:1
*planar*".

**Byte Order.**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 12:                                                              |
+--------------------------------------------------------------------------+
| start + 24:                                                              |
+--------------------------------------------------------------------------+
| start + 36:                                                              |
+--------------------------------------------------------------------------+

**Color Sample Location..**

+-----+-----+----+-----+-----+-----+----+-----+----+-----+----+-----+-----+-----+----+-----+
|     | 0   |    | 1   |     | 2   |    | 3   |    | 4   |    | 5   |     | 6   |    | 7   |
+-----+-----+----+-----+-----+-----+----+-----+----+-----+----+-----+-----+-----+----+-----+
| 0   | Y   |    | Y   | C   | Y   |    | Y   |    | Y   |    | Y   | C   | Y   |    | Y   |
+-----+-----+----+-----+-----+-----+----+-----+----+-----+----+-----+-----+-----+----+-----+
| 1   | Y   |    | Y   | C   | Y   |    | Y   |    | Y   |    | Y   | C   | Y   |    | Y   |
+-----+-----+----+-----+-----+-----+----+-----+----+-----+----+-----+-----+-----+----+-----+
| 2   | Y   |    | Y   | C   | Y   |    | Y   |    | Y   |    | Y   | C   | Y   |    | Y   |
+-----+-----+----+-----+-----+-----+----+-----+----+-----+----+-----+-----+-----+----+-----+
| 3   | Y   |    | Y   | C   | Y   |    | Y   |    | Y   |    | Y   | C   | Y   |    | Y   |
+-----+-----+----+-----+-----+-----+----+-----+----+-----+----+-----+-----+-----+----+-----+
