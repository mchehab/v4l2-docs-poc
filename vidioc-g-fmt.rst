ioctl VIDIOC\_G\_FMT, VIDIOC\_S\_FMT, VIDIOC\_TRY\_FMT
MANVOL
VIDIOC\_G\_FMT
VIDIOC\_S\_FMT
VIDIOC\_TRY\_FMT
Get or set the data format, try a format
int
ioctl
int
fd
int
request
struct v4l2\_format \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_FMT, VIDIOC\_S\_FMT, VIDIOC\_TRY\_FMT

``argp``

Description
===========

These ioctls are used to negotiate the format of data (typically image
format) exchanged between driver and application.

To query the current parameters applications set the type field of a
struct v4l2\_format to the respective buffer (stream) type. For example
video capture devices use ``V4L2_BUF_TYPE_VIDEO_CAPTURE`` or
``V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE``. When the application calls the
``VIDIOC_G_FMT`` ioctl with a pointer to this structure the driver fills
the respective member of the fmt union. In case of video capture devices
that is either the V4L2-PIX-FORMAT pix or the V4L2-PIX-FORMAT-MPLANE
pix\_mp member. When the requested buffer type is not supported drivers
return an EINVAL.

To change the current format parameters applications initialize the type
field and all fields of the respective fmt union member. For details see
the documentation of the various devices types in ?. Good practice is to
query the current parameters first, and to modify only those parameters
not suitable for the application. When the application calls the
``VIDIOC_S_FMT`` ioctl with a pointer to a v4l2\_format structure the
driver checks and adjusts the parameters against hardware abilities.
Drivers should not return an error code unless the type field is
invalid, this is a mechanism to fathom device capabilities and to
approach parameters acceptable for both the application and driver. On
success the driver may program the hardware, allocate resources and
generally prepare for data exchange. Finally the ``VIDIOC_S_FMT`` ioctl
returns the current format parameters as ``VIDIOC_G_FMT`` does. Very
simple, inflexible devices may even ignore all input and always return
the default parameters. However all V4L2 devices exchanging data with
the application must implement the ``VIDIOC_G_FMT`` and ``VIDIOC_S_FMT``
ioctl. When the requested buffer type is not supported drivers return an
EINVAL on a ``VIDIOC_S_FMT`` attempt. When I/O is already in progress or
the resource is not available for other reasons drivers return the
EBUSY.

The ``VIDIOC_TRY_FMT`` ioctl is equivalent to ``VIDIOC_S_FMT`` with one
exception: it does not change driver state. It can also be called at any
time, never returning EBUSY. This function is provided to negotiate
parameters, to learn about hardware limitations, without disabling I/O
or possibly time consuming hardware preparations. Although strongly
recommended drivers are not required to implement this ioctl.

The format as returned by ``VIDIOC_TRY_FMT`` must be identical to what
``VIDIOC_S_FMT`` returns for the same input or output.

+-----------+--------------------------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | type                     |                  | Type of the data stream, see ?.                                                                                                                       |
+-----------+--------------------------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
| union     | fmt                      |
+-----------+--------------------------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
|           | V4L2-PIX-FORMAT          | pix              | Definition of an image format, see ?, used by video capture and output devices.                                                                       |
+-----------+--------------------------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
|           | V4L2-PIX-FORMAT-MPLANE   | pix\_mp          | Definition of an image format, see ?, used by video capture and output devices that support the `multi-planar version of the API <#planar-apis>`__.   |
+-----------+--------------------------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
|           | V4L2-WINDOW              | win              | Definition of an overlaid image, see ?, used by video overlay devices.                                                                                |
+-----------+--------------------------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
|           | V4L2-VBI-FORMAT          | vbi              | Raw VBI capture or output parameters. This is discussed in more detail in ?. Used by raw VBI capture and output devices.                              |
+-----------+--------------------------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
|           | V4L2-SLICED-VBI-FORMAT   | sliced           | Sliced VBI capture or output parameters. See ? for details. Used by sliced VBI capture and output devices.                                            |
+-----------+--------------------------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
|           | V4L2-SDR-FORMAT          | sdr              | Definition of a data format, see ?, used by SDR capture and output devices.                                                                           |
+-----------+--------------------------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+
|           | \_\_u8                   | raw\_data[200]   | Place holder for future extensions.                                                                                                                   |
+-----------+--------------------------+------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_format

RETURN-VALUE

EINVAL
    The V4L2-FORMAT type field is invalid or the requested buffer type
    not supported.
