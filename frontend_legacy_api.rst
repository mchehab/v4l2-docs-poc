Frontend Legacy Data Types
==========================

Frontend type
-------------

For historical reasons, frontend types are named by the type of
modulation used in transmission. The fontend types are given by
fe\_type\_t type, defined as:

+---------------+----------------------------------------------------------------------------+--------------------------------------------------------------------+
| fe\_type      | Description                                                                | `DTV\_DELIVERY\_SYSTEM <#DTV-DELIVERY-SYSTEM>`__ equivalent type   |
+===============+============================================================================+====================================================================+
| ``FE_QPSK``   | For DVB-S standard                                                         | ``SYS_DVBS``                                                       |
+---------------+----------------------------------------------------------------------------+--------------------------------------------------------------------+
| ``FE_QAM``    | For DVB-C annex A standard                                                 | ``SYS_DVBC_ANNEX_A``                                               |
+---------------+----------------------------------------------------------------------------+--------------------------------------------------------------------+
| ``FE_OFDM``   | For DVB-T standard                                                         | ``SYS_DVBT``                                                       |
+---------------+----------------------------------------------------------------------------+--------------------------------------------------------------------+
| ``FE_ATSC``   | For ATSC standard (terrestrial) or for DVB-C Annex B (cable) used in US.   | ``SYS_ATSC`` (terrestrial) or ``SYS_DVBC_ANNEX_B`` (cable)         |
+---------------+----------------------------------------------------------------------------+--------------------------------------------------------------------+

Table: Frontend types

Newer formats like DVB-S2, ISDB-T, ISDB-S and DVB-T2 are not described
at the above, as they're supported via the new
`FE\_GET\_PROPERTY/FE\_GET\_SET\_PROPERTY <#FE_GET_PROPERTY>`__ ioctl's,
using the `DTV\_DELIVERY\_SYSTEM <#DTV-DELIVERY-SYSTEM>`__ parameter.

In the old days, DVB-FRONTEND-INFO used to contain ``fe_type_t`` field
to indicate the delivery systems, filled with either FE\_QPSK, FE\_QAM,
FE\_OFDM or FE\_ATSC. While this is still filled to keep backward
compatibility, the usage of this field is deprecated, as it can report
just one delivery system, but some devices support multiple delivery
systems. Please use `DTV\_ENUM\_DELSYS <#DTV-ENUM-DELSYS>`__ instead.

On devices that support multiple delivery systems,
DVB-FRONTEND-INFO::\ ``fe_type_t`` is filled with the currently
standard, as selected by the last call to
`FE\_SET\_PROPERTY <#FE_GET_PROPERTY>`__ using the DTV-DELIVERY-SYSTEM
property.

Frontend bandwidth
------------------

+---------------------------+---------------------------------------+
| ID                        | Description                           |
+===========================+=======================================+
| ``BANDWIDTH_AUTO``        | Autodetect bandwidth (if supported)   |
+---------------------------+---------------------------------------+
| ``BANDWIDTH_1_712_MHZ``   | 1.712 MHz                             |
+---------------------------+---------------------------------------+
| ``BANDWIDTH_5_MHZ``       | 5 MHz                                 |
+---------------------------+---------------------------------------+
| ``BANDWIDTH_6_MHZ``       | 6 MHz                                 |
+---------------------------+---------------------------------------+
| ``BANDWIDTH_7_MHZ``       | 7 MHz                                 |
+---------------------------+---------------------------------------+
| ``BANDWIDTH_8_MHZ``       | 8 MHz                                 |
+---------------------------+---------------------------------------+
| ``BANDWIDTH_10_MHZ``      | 10 MHz                                |
+---------------------------+---------------------------------------+

Table: enum fe\_bandwidth

frontend parameters
-------------------

The kind of parameters passed to the frontend device for tuning depend
on the kind of hardware you are using.

The struct ``dvb_frontend_parameters`` uses an union with specific
per-system parameters. However, as newer delivery systems required more
data, the structure size weren't enough to fit, and just extending its
size would break the existing applications. So, those parameters were
replaced by the usage of
```FE_GET_PROPERTY/FE_SET_PROPERTY`` <#FE_GET_PROPERTY>`__ ioctl's. The
new API is flexible enough to add new parameters to existing delivery
systems, and to add newer delivery systems.

So, newer applications should use
```FE_GET_PROPERTY/FE_SET_PROPERTY`` <#FE_GET_PROPERTY>`__ instead, in
order to be able to support the newer System Delivery like DVB-S2,
DVB-T2, DVB-C2, ISDB, etc.

All kinds of parameters are combined as an union in the
FrontendParameters structure:

::

    struct dvb_frontend_parameters {
        uint32_t frequency;     /⋆ (absolute) frequency in Hz for QAM/OFDM ⋆/
                    /⋆ intermediate frequency in kHz for QPSK ⋆/
        FE-SPECTRAL-INVERSION-T inversion;
        union {
            struct dvb_qpsk_parameters qpsk;
            struct dvb_qam_parameters  qam;
            struct dvb_ofdm_parameters ofdm;
            struct dvb_vsb_parameters  vsb;
        } u;
    };

In the case of QPSK frontends the ``frequency`` field specifies the
intermediate frequency, i.e. the offset which is effectively added to
the local oscillator frequency (LOF) of the LNB. The intermediate
frequency has to be specified in units of kHz. For QAM and OFDM
frontends the ``frequency`` specifies the absolute frequency and is
given in Hz.

QPSK parameters
~~~~~~~~~~~~~~~

For satellite QPSK frontends you have to use the ``dvb_qpsk_parameters``
structure:

::

     struct dvb_qpsk_parameters {
         uint32_t        symbol_rate;  /⋆ symbol rate in Symbols per second ⋆/
         FE-CODE-RATE-T  fec_inner;    /⋆ forward error correction (see above) ⋆/
     };

QAM parameters
~~~~~~~~~~~~~~

for cable QAM frontend you use the ``dvb_qam_parameters`` structure:

::

     struct dvb_qam_parameters {
         uint32_t         symbol_rate; /⋆ symbol rate in Symbols per second ⋆/
         FE-CODE-RATE-T   fec_inner;   /⋆ forward error correction (see above) ⋆/
         FE-MODULATION-T  modulation;  /⋆ modulation type (see above) ⋆/
     };

VSB parameters
~~~~~~~~~~~~~~

ATSC frontends are supported by the ``dvb_vsb_parameters`` structure:

::

    struct dvb_vsb_parameters {
        FE-MODULATION-T modulation;   /⋆ modulation type (see above) ⋆/
    };

OFDM parameters
~~~~~~~~~~~~~~~

DVB-T frontends are supported by the ``dvb_ofdm_parameters`` structure:

::

     struct dvb_ofdm_parameters {
         FE-BANDWIDTH-T      bandwidth;
         FE-CODE-RATE-T      code_rate_HP;  /⋆ high priority stream code rate ⋆/
         FE-CODE-RATE-T      code_rate_LP;  /⋆ low priority stream code rate ⋆/
         FE-MODULATION-T     constellation; /⋆ modulation type (see above) ⋆/
         FE-TRANSMIT-MODE-T  transmission_mode;
         FE-GUARD-INTERVAL-T guard_interval;
         FE-HIERARCHY-T      hierarchy_information;
     };

frontend events
---------------

::

     struct dvb_frontend_event {
         fe_status_t status;
         struct dvb_frontend_parameters parameters;
     };

Frontend Legacy Function Calls
==============================

Those functions are defined at DVB version 3. The support is kept in the
kernel due to compatibility issues only. Their usage is strongly not
recommended

FE\_READ\_BER
-------------

DESCRIPTION

+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call returns the bit error rate for the signal currently received/demodulated by the front-end. For this command, read-only access to the device is sufficient.   |
+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+--------------------------------------------------------------------------------------+
| int ioctl(int fd, int request = `FE\_READ\_BER <#FE_READ_BER>`__, uint32\_t ⋆ber);   |
+--------------------------------------------------------------------------------------+

PARAMETERS

+-------------------+-------------------------------------------------------------+
| int fd            | File descriptor returned by a previous call to open().      |
+-------------------+-------------------------------------------------------------+
| int request       | Equals `FE\_READ\_BER <#FE_READ_BER>`__ for this command.   |
+-------------------+-------------------------------------------------------------+
| uint32\_t \*ber   | The bit error rate is stored into \*ber.                    |
+-------------------+-------------------------------------------------------------+

RETURN-VALUE-DVB
FE\_READ\_SNR
-------------

DESCRIPTION

+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call returns the signal-to-noise ratio for the signal currently received by the front-end. For this command, read-only access to the device is sufficient.   |
+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+--------------------------------------------------------------------------------------+
| int ioctl(int fd, int request = `FE\_READ\_SNR <#FE_READ_SNR>`__, uint16\_t ⋆snr);   |
+--------------------------------------------------------------------------------------+

PARAMETERS

+-------------------+-------------------------------------------------------------+
| int fd            | File descriptor returned by a previous call to open().      |
+-------------------+-------------------------------------------------------------+
| int request       | Equals `FE\_READ\_SNR <#FE_READ_SNR>`__ for this command.   |
+-------------------+-------------------------------------------------------------+
| uint16\_t \*snr   | The signal-to-noise ratio is stored into \*snr.             |
+-------------------+-------------------------------------------------------------+

RETURN-VALUE-DVB
FE\_READ\_SIGNAL\_STRENGTH
--------------------------

DESCRIPTION

+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call returns the signal strength value for the signal currently received by the front-end. For this command, read-only access to the device is sufficient.   |
+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+---------------------------------------------------------------------------------------------------------------------+
| int ioctl( int fd, int request = `FE\_READ\_SIGNAL\_STRENGTH <#FE_READ_SIGNAL_STRENGTH>`__, uint16\_t ⋆strength);   |
+---------------------------------------------------------------------------------------------------------------------+

PARAMETERS

+------------------------+--------------------------------------------------------------------------------------+
| int fd                 | File descriptor returned by a previous call to open().                               |
+------------------------+--------------------------------------------------------------------------------------+
| int request            | Equals `FE\_READ\_SIGNAL\_STRENGTH <#FE_READ_SIGNAL_STRENGTH>`__ for this command.   |
+------------------------+--------------------------------------------------------------------------------------+
| uint16\_t \*strength   | The signal strength value is stored into \*strength.                                 |
+------------------------+--------------------------------------------------------------------------------------+

RETURN-VALUE-DVB
FE\_READ\_UNCORRECTED\_BLOCKS
-----------------------------

DESCRIPTION

+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call returns the number of uncorrected blocks detected by the device driver during its lifetime. For meaningful measurements, the increment in block count during a specific time interval should be calculated. For this command, read-only access to the device is sufficient.   |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Note that the counter will wrap to zero after its maximum count has been reached.                                                                                                                                                                                                             |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+--------------------------------------------------------------------------------------------------------------------------+
| int ioctl( int fd, int request = `FE\_READ\_UNCORRECTED\_BLOCKS <#FE_READ_UNCORRECTED_BLOCKS>`__, uint32\_t ⋆ublocks);   |
+--------------------------------------------------------------------------------------------------------------------------+

PARAMETERS

+-----------------------+--------------------------------------------------------------------------------------------+
| int fd                | File descriptor returned by a previous call to open().                                     |
+-----------------------+--------------------------------------------------------------------------------------------+
| int request           | Equals `FE\_READ\_UNCORRECTED\_BLOCKS <#FE_READ_UNCORRECTED_BLOCKS>`__ for this command.   |
+-----------------------+--------------------------------------------------------------------------------------------+
| uint32\_t \*ublocks   | The total number of uncorrected blocks seen by the driver so far.                          |
+-----------------------+--------------------------------------------------------------------------------------------+

RETURN-VALUE-DVB
FE\_SET\_FRONTEND
-----------------

DESCRIPTION

+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call starts a tuning operation using specified parameters. The result of this call will be successful if the parameters were valid and the tuning could be initiated. The result of the tuning operation in itself, however, will arrive asynchronously as an event (see documentation for `FE\_GET\_EVENT <#FE_GET_EVENT>`__ and FrontendEvent.) If a new `FE\_SET\_FRONTEND <#FE_SET_FRONTEND>`__ operation is initiated before the previous one was completed, the previous operation will be aborted in favor of the new one. This command requires read/write access to the device.   |
+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+-------------------------------------------------------------------------------------------------------------------+
| int ioctl(int fd, int request = `FE\_SET\_FRONTEND <#FE_SET_FRONTEND>`__, struct dvb\_frontend\_parameters ⋆p);   |
+-------------------------------------------------------------------------------------------------------------------+

PARAMETERS

+----------------------------------------+---------------------------------------------------------------------+
| int fd                                 | File descriptor returned by a previous call to open().              |
+----------------------------------------+---------------------------------------------------------------------+
| int request                            | Equals `FE\_SET\_FRONTEND <#FE_SET_FRONTEND>`__ for this command.   |
+----------------------------------------+---------------------------------------------------------------------+
| struct dvb\_frontend\_parameters \*p   | Points to parameters for tuning operation.                          |
+----------------------------------------+---------------------------------------------------------------------+

RETURN-VALUE-DVB
+----------+------------------------------------------+
| EINVAL   | Maximum supported symbol rate reached.   |
+----------+------------------------------------------+

FE\_GET\_FRONTEND
-----------------

DESCRIPTION

+----------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call queries the currently effective frontend parameters. For this command, read-only access to the device is sufficient.   |
+----------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+-------------------------------------------------------------------------------------------------------------------+
| int ioctl(int fd, int request = `FE\_GET\_FRONTEND <#FE_GET_FRONTEND>`__, struct dvb\_frontend\_parameters ⋆p);   |
+-------------------------------------------------------------------------------------------------------------------+

PARAMETERS

+----------------------------------------+---------------------------------------------------------------------+
| int fd                                 | File descriptor returned by a previous call to open().              |
+----------------------------------------+---------------------------------------------------------------------+
| int request                            | Equals `FE\_SET\_FRONTEND <#FE_SET_FRONTEND>`__ for this command.   |
+----------------------------------------+---------------------------------------------------------------------+
| struct dvb\_frontend\_parameters \*p   | Points to parameters for tuning operation.                          |
+----------------------------------------+---------------------------------------------------------------------+

RETURN-VALUE-DVB
+----------+------------------------------------------+
| EINVAL   | Maximum supported symbol rate reached.   |
+----------+------------------------------------------+

FE\_GET\_EVENT
--------------

DESCRIPTION

+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call returns a frontend event if available. If an event is not available, the behavior depends on whether the device is in blocking or non-blocking mode. In the latter case, the call fails immediately with errno set to EWOULDBLOCK. In the former case, the call blocks until an event becomes available.                                                                                                                                                                                                                                                                                                                                                                                     |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| The standard Linux poll() and/or select() system calls can be used with the device file descriptor to watch for new events. For select(), the file descriptor should be included in the exceptfds argument, and for poll(), POLLPRI should be specified as the wake-up condition. Since the event queue allocated is rather small (room for 8 events), the queue must be serviced regularly to avoid overflow. If an overflow happens, the oldest event is discarded from the queue, and an error (EOVERFLOW) occurs the next time the queue is read. After reporting the error condition in this fashion, subsequent `FE\_GET\_EVENT <#FE_GET_EVENT>`__ calls will return events from the queue as usual.   |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| For the sake of implementation simplicity, this command requires read/write access to the device.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+---------------------------------------------------------------------------------------+
| int ioctl(int fd, int request = QPSK\_GET\_EVENT, struct dvb\_frontend\_event ⋆ev);   |
+---------------------------------------------------------------------------------------+

PARAMETERS

+------------------------------------+---------------------------------------------------------------+
| int fd                             | File descriptor returned by a previous call to open().        |
+------------------------------------+---------------------------------------------------------------+
| int request                        | Equals `FE\_GET\_EVENT <#FE_GET_EVENT>`__ for this command.   |
+------------------------------------+---------------------------------------------------------------+
| struct dvb\_frontend\_event \*ev   | Points to the location where the event,                       |
+------------------------------------+---------------------------------------------------------------+
|                                    | if any, is to be stored.                                      |
+------------------------------------+---------------------------------------------------------------+

RETURN-VALUE-DVB
+---------------+----------------------------------------------------------------------+
| EWOULDBLOCK   | There is no event pending, and the device is in non-blocking mode.   |
+---------------+----------------------------------------------------------------------+
| EOVERFLOW     | Overflow in event queue - one or more events were lost.              |
+---------------+----------------------------------------------------------------------+

FE\_DISHNETWORK\_SEND\_LEGACY\_CMD
----------------------------------

DESCRIPTION

+---------------------------------------------------------------------------------------------------------------------+
| WARNING: This is a very obscure legacy command, used only at stv0299 driver. Should not be used on newer drivers.   |
|                                                                                                                     |
| It provides a non-standard method for selecting Diseqc voltage on the frontend, for Dish Network legacy switches.   |
|                                                                                                                     |
| As support for this ioctl were added in 2004, this means that such dishes were already legacy in 2004.              |
+---------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+---------------------------------------------------------------------------------------------------------------------------------+
| int ioctl(int fd, int request = `FE\_DISHNETWORK\_SEND\_LEGACY\_CMD <#FE_DISHNETWORK_SEND_LEGACY_CMD>`__, unsigned long cmd);   |
+---------------------------------------------------------------------------------------------------------------------------------+

PARAMETERS

+---------------------+-------------------------------------------------------+
| unsigned long cmd   | sends the specified raw cmd to the dish via DISEqC.   |
+---------------------+-------------------------------------------------------+

RETURN-VALUE-DVB
