V4L2\_PIX\_FMT\_SRGGB10P ('pRAA'), V4L2\_PIX\_FMT\_SGRBG10P ('pgAA'),
V4L2\_PIX\_FMT\_SGBRG10P ('pGAA'), V4L2\_PIX\_FMT\_SBGGR10P ('pBAA'),
MANVOL
V4L2\_PIX\_FMT\_SRGGB10P
V4L2\_PIX\_FMT\_SGRBG10P
V4L2\_PIX\_FMT\_SGBRG10P
V4L2\_PIX\_FMT\_SBGGR10P
10-bit packed Bayer formats
Description
===========

These four pixel formats are packed raw sRGB / Bayer formats with 10
bits per colour. Every four consecutive colour components are packed
into 5 bytes. Each of the first 4 bytes contain the 8 high order bits of
the pixels, and the fifth byte contains the two least significants bits
of each pixel, in the same order.

Each n-pixel row contains n/2 green samples and n/2 blue or red samples,
with alternating green-red and green-blue rows. They are conventionally
described as GRGR... BGBG..., RGRG... GBGB..., etc. Below is an example
of one of these formats:

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 5:                                                               |
+--------------------------------------------------------------------------+
| start + 10:                                                              |
+--------------------------------------------------------------------------+
| start + 15:                                                              |
+--------------------------------------------------------------------------+
