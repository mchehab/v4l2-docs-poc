The Radio Data System transmits supplementary information in binary
format, for example the station name or travel information, on an
inaudible audio subcarrier of a radio program. This interface is aimed
at devices capable of receiving and/or transmitting RDS information.

For more information see the core RDS standard ? and the RBDS standard
?.

Note that the RBDS standard as is used in the USA is almost identical to
the RDS standard. Any RDS decoder/encoder can also handle RBDS. Only
some of the fields have slightly different meanings. See the RBDS
standard for more information.

The RBDS standard also specifies support for MMBS (Modified Mobile
Search). This is a proprietary format which seems to be discontinued.
The RDS interface does not support this format. Should support for MMBS
(or the so-called 'E blocks' in general) be needed, then please contact
the linux-media mailing list: V4L-ML.

Querying Capabilities
=====================

Devices supporting the RDS capturing API set the
``V4L2_CAP_RDS_CAPTURE`` flag in the capabilities field of
V4L2-CAPABILITY returned by the VIDIOC-QUERYCAP ioctl. Any tuner that
supports RDS will set the ``V4L2_TUNER_CAP_RDS`` flag in the capability
field of V4L2-TUNER. If the driver only passes RDS blocks without
interpreting the data the ``V4L2_TUNER_CAP_RDS_BLOCK_IO`` flag has to be
set, see `Reading RDS data <#reading-rds-data>`__. For future use the
flag ``V4L2_TUNER_CAP_RDS_CONTROLS`` has also been defined. However, a
driver for a radio tuner with this capability does not yet exist, so if
you are planning to write such a driver you should discuss this on the
linux-media mailing list: V4L-ML.

Whether an RDS signal is present can be detected by looking at the
rxsubchans field of V4L2-TUNER: the ``V4L2_TUNER_SUB_RDS`` will be set
if RDS data was detected.

Devices supporting the RDS output API set the ``V4L2_CAP_RDS_OUTPUT``
flag in the capabilities field of V4L2-CAPABILITY returned by the
VIDIOC-QUERYCAP ioctl. Any modulator that supports RDS will set the
``V4L2_TUNER_CAP_RDS`` flag in the capability field of V4L2-MODULATOR.
In order to enable the RDS transmission one must set the
``V4L2_TUNER_SUB_RDS`` bit in the txsubchans field of V4L2-MODULATOR. If
the driver only passes RDS blocks without interpreting the data the
``V4L2_TUNER_CAP_RDS_BLOCK_IO`` flag has to be set. If the tuner is
capable of handling RDS entities like program identification codes and
radio text, the flag ``V4L2_TUNER_CAP_RDS_CONTROLS`` should be set, see
`Writing RDS data <#writing-rds-data>`__ and `FM Transmitter Control
Reference <#fm-tx-controls>`__.

Reading RDS data
================

RDS data can be read from the radio device with the FUNC-READ function.
The data is packed in groups of three bytes.

Writing RDS data
================

RDS data can be written to the radio device with the FUNC-WRITE
function. The data is packed in groups of three bytes, as follows:

RDS datastructures
==================

+------------+------------+-----------------------------------------------------+
| \_\_u8     | lsb        | Least Significant Byte of RDS Block                 |
+------------+------------+-----------------------------------------------------+
| \_\_u8     | msb        | Most Significant Byte of RDS Block                  |
+------------+------------+-----------------------------------------------------+
| \_\_u8     | block      | Block description                                   |
+------------+------------+-----------------------------------------------------+

Table: struct v4l2\_rds\_data

+--------------+--------------------------------------------------------------+
| Bits 0-2     | Block (aka offset) of the received data.                     |
+--------------+--------------------------------------------------------------+
| Bits 3-5     | Deprecated. Currently identical to bits 0-2. Do not use      |
|              | these bits.                                                  |
+--------------+--------------------------------------------------------------+
| Bit 6        | Corrected bit. Indicates that an error was corrected for     |
|              | this data block.                                             |
+--------------+--------------------------------------------------------------+
| Bit 7        | Error bit. Indicates that an uncorrectable error occurred    |
|              | during reception of this block.                              |
+--------------+--------------------------------------------------------------+

Table: Block description

+-----------+-----------+-----------+-----------------------------------------------+
| V4L2\_RDS |           | 7         | Mask for bits 0-2 to get the block ID.        |
| \_BLOCK\_ |           |           |                                               |
| MSK       |           |           |                                               |
+-----------+-----------+-----------+-----------------------------------------------+
| V4L2\_RDS |           | 0         | Block A.                                      |
| \_BLOCK\_ |           |           |                                               |
| A         |           |           |                                               |
+-----------+-----------+-----------+-----------------------------------------------+
| V4L2\_RDS |           | 1         | Block B.                                      |
| \_BLOCK\_ |           |           |                                               |
| B         |           |           |                                               |
+-----------+-----------+-----------+-----------------------------------------------+
| V4L2\_RDS |           | 2         | Block C.                                      |
| \_BLOCK\_ |           |           |                                               |
| C         |           |           |                                               |
+-----------+-----------+-----------+-----------------------------------------------+
| V4L2\_RDS |           | 3         | Block D.                                      |
| \_BLOCK\_ |           |           |                                               |
| D         |           |           |                                               |
+-----------+-----------+-----------+-----------------------------------------------+
| V4L2\_RDS |           | 4         | Block C'.                                     |
| \_BLOCK\_ |           |           |                                               |
| C\_ALT    |           |           |                                               |
+-----------+-----------+-----------+-----------------------------------------------+
| V4L2\_RDS | read-only | 7         | An invalid block.                             |
| \_BLOCK\_ |           |           |                                               |
| INVALID   |           |           |                                               |
+-----------+-----------+-----------+-----------------------------------------------+
| V4L2\_RDS | read-only | 0x40      | A bit error was detected but corrected.       |
| \_BLOCK\_ |           |           |                                               |
| CORRECTED |           |           |                                               |
+-----------+-----------+-----------+-----------------------------------------------+
| V4L2\_RDS | read-only | 0x80      | An uncorrectable error occurred.              |
| \_BLOCK\_ |           |           |                                               |
| ERROR     |           |           |                                               |
+-----------+-----------+-----------+-----------------------------------------------+

Table: Block defines
