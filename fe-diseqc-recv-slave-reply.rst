ioctl FE\_DISEQC\_RECV\_SLAVE\_REPLY
MANVOL
FE\_DISEQC\_RECV\_SLAVE\_REPLY
Receives reply from a DiSEqC 2.0 command
int
ioctl
int
fd
int
request
struct dvb\_diseqc\_slave\_reply \*
argp
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_DISEQC\_RECV\_SLAVE\_REPLY

``argp``
    pointer to DVB-DISEQC-SLAVE-REPLY

Description
===========

Receives reply from a DiSEqC 2.0 command.

RETURN-VALUE-DVB
+------------+------------+----------------------------------------------------------------------------------+
| uint8\_t   | msg[4]     | DiSEqC message (framing, data[3])                                                |
+------------+------------+----------------------------------------------------------------------------------+
| uint8\_t   | msg\_len   | Length of the DiSEqC message. Valid values are 0 to 4, where 0 means no msg      |
+------------+------------+----------------------------------------------------------------------------------+
| int        | timeout    | Return from ioctl after timeout ms with errorcode when no message was received   |
+------------+------------+----------------------------------------------------------------------------------+

Table: struct dvb\_diseqc\_slave\_reply
