ioctl VIDIOC\_REQBUFS
MANVOL
VIDIOC\_REQBUFS
Initiate Memory Mapping or User Pointer I/O
int
ioctl
int
fd
int
request
struct v4l2\_requestbuffers \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_REQBUFS

``argp``

Description
===========

This ioctl is used to initiate `memory mapped <#mmap>`__, `user
pointer <#userp>`__ or `DMABUF <#dmabuf>`__ based I/O. Memory mapped
buffers are located in device memory and must be allocated with this
ioctl before they can be mapped into the application's address space.
User buffers are allocated by applications themselves, and this ioctl is
merely used to switch the driver into user pointer I/O mode and to setup
some internal structures. Similarly, DMABUF buffers are allocated by
applications through a device driver, and this ioctl only configures the
driver into DMABUF I/O mode without performing any direct allocation.

To allocate device buffers applications initialize all fields of the
v4l2\_requestbuffers structure. They set the type field to the
respective stream or buffer type, the count field to the desired number
of buffers, memory must be set to the requested I/O method and the
reserved array must be zeroed. When the ioctl is called with a pointer
to this structure the driver will attempt to allocate the requested
number of buffers and it stores the actual number allocated in the count
field. It can be smaller than the number requested, even zero, when the
driver runs out of free memory. A larger number is also possible when
the driver requires more buffers to function correctly. For example
video output requires at least two buffers, one displayed and one filled
by the application.

When the I/O method is not supported the ioctl returns an EINVAL.

Applications can call ``VIDIOC_REQBUFS`` again to change the number of
buffers, however this cannot succeed when any buffers are still mapped.
A count value of zero frees all buffers, after aborting or finishing any
DMA in progress, an implicit VIDIOC-STREAMOFF.

+-----------+---------------+------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | count         | The number of buffers requested or granted.                                                                      |
+-----------+---------------+------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | type          | Type of the stream or buffers, this is the same as the V4L2-FORMAT type field. See ? for valid values.           |
+-----------+---------------+------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | memory        | Applications set this field to ``V4L2_MEMORY_MMAP``, ``V4L2_MEMORY_DMABUF`` or ``V4L2_MEMORY_USERPTR``. See ?.   |
+-----------+---------------+------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | reserved[2]   | A place holder for future extensions. Drivers and applications must set the array to zero.                       |
+-----------+---------------+------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_requestbuffers

RETURN-VALUE

EINVAL
    The buffer type (type field) or the requested I/O method (memory) is
    not supported.
