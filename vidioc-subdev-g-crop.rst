ioctl VIDIOC\_SUBDEV\_G\_CROP, VIDIOC\_SUBDEV\_S\_CROP
MANVOL
VIDIOC\_SUBDEV\_G\_CROP
VIDIOC\_SUBDEV\_S\_CROP
Get or set the crop rectangle on a subdev pad
int
ioctl
int
fd
int
request
struct v4l2\_subdev\_crop \*
argp
int
ioctl
int
fd
int
request
const struct v4l2\_subdev\_crop \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_SUBDEV\_G\_CROP, VIDIOC\_SUBDEV\_S\_CROP

``argp``

Description
===========

    **Note**

    This is an `obsolete <#obsolete>`__ interface and may be removed in
    the future. It is superseded by `the selection
    API <#vidioc-subdev-g-selection>`__.

To retrieve the current crop rectangle applications set the pad field of
a V4L2-SUBDEV-CROP to the desired pad number as reported by the media
API and the which field to ``V4L2_SUBDEV_FORMAT_ACTIVE``. They then call
the ``VIDIOC_SUBDEV_G_CROP`` ioctl with a pointer to this structure. The
driver fills the members of the rect field or returns EINVAL if the
input arguments are invalid, or if cropping is not supported on the
given pad.

To change the current crop rectangle applications set both the pad and
which fields and all members of the rect field. They then call the
``VIDIOC_SUBDEV_S_CROP`` ioctl with a pointer to this structure. The
driver verifies the requested crop rectangle, adjusts it based on the
hardware capabilities and configures the device. Upon return the
V4L2-SUBDEV-CROP contains the current format as would be returned by a
``VIDIOC_SUBDEV_G_CROP`` call.

Applications can query the device capabilities by setting the which to
``V4L2_SUBDEV_FORMAT_TRY``. When set, 'try' crop rectangles are not
applied to the device by the driver, but are mangled exactly as active
crop rectangles and stored in the sub-device file handle. Two
applications querying the same sub-device would thus not interact with
each other.

Drivers must not return an error solely because the requested crop
rectangle doesn't match the device capabilities. They must instead
modify the rectangle to match what the hardware can provide. The
modified format should be as close as possible to the original request.

+-------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32     | pad           | Pad number as reported by the media framework.                                         |
+-------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32     | which         | Crop rectangle to get or set, from V4L2-SUBDEV-FORMAT-WHENCE.                          |
+-------------+---------------+----------------------------------------------------------------------------------------+
| V4L2-RECT   | rect          | Crop rectangle boundaries, in pixels.                                                  |
+-------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32     | reserved[8]   | Reserved for future extensions. Applications and drivers must set the array to zero.   |
+-------------+---------------+----------------------------------------------------------------------------------------+

Table: struct v4l2\_subdev\_crop

RETURN-VALUE

EBUSY
    The crop rectangle can't be changed because the pad is currently
    busy. This can be caused, for instance, by an active video stream on
    the pad. The ioctl must not be retried without performing another
    action to fix the problem first. Only returned by
    ``VIDIOC_SUBDEV_S_CROP``

EINVAL
    The V4L2-SUBDEV-CROP pad references a non-existing pad, the which
    field references a non-existing format, or cropping is not supported
    on the given subdev pad.
