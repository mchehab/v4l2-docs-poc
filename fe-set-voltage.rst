ioctl FE\_SET\_VOLTAGE
MANVOL
FE\_SET\_VOLTAGE
Allow setting the DC level sent to the antenna subsystem.
int
ioctl
int
fd
int
request
enum fe\_sec\_voltage \*
voltage
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_SET\_VOLTAGE

``voltage``
    pointer to FE-SEC-VOLTAGE

    Valid values are described at FE-SEC-VOLTAGE.

Description
===========

This ioctl allows to set the DC voltage level sent through the antenna
cable to 13V, 18V or off.

Usually, a satellite antenna subsystems require that the digital TV
device to send a DC voltage to feed power to the LNBf. Depending on the
LNBf type, the polarization or the intermediate frequency (IF) of the
LNBf can controlled by the voltage level. Other devices (for example,
the ones that implement DISEqC and multipoint LNBf's don't need to
control the voltage level, provided that either 13V or 18V is sent to
power up the LNBf.

NOTE: if more than one device is connected to the same antenna, setting
a voltage level may interfere on other devices, as they may lose the
capability of setting polarization or IF. So, on those cases, setting
the voltage to SEC\_VOLTAGE\_OFF while the device is not is used is
recommended.

RETURN-VALUE-DVB
