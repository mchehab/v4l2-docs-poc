V4L2\_PIX\_FMT\_YUV411P ('411P')
MANVOL
V4L2\_PIX\_FMT\_YUV411P
Format with ¼ horizontal chroma resolution, also known as YUV 4:1:1.
Planar layout as opposed to
V4L2\_PIX\_FMT\_Y41P
Description
===========

This format is not commonly used. This is a planar format similar to the
4:2:2 planar format except with half as many chroma. The three
components are separated into three sub-images or planes. The Y plane is
first. The Y plane has one byte per pixel. The Cb plane immediately
follows the Y plane in memory. The Cb plane is ¼ the width of the Y
plane (and of the image). Each Cb belongs to 4 pixels all on the same
row. For example, Cb\ :sub:`0` belongs to Y'\ :sub:`00`, Y'\ :sub:`01`,
Y'\ :sub:`02` and Y'\ :sub:`03`. Following the Cb plane is the Cr plane,
just like the Cb plane.

If the Y plane has pad bytes after each row, then the Cr and Cb planes
have ¼ as many pad bytes after their rows. In other words, four C x rows
(including padding) is exactly as long as one Y row (including padding).

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 4:                                                               |
+--------------------------------------------------------------------------+
| start + 8:                                                               |
+--------------------------------------------------------------------------+
| start + 12:                                                              |
+--------------------------------------------------------------------------+
| start + 16:                                                              |
+--------------------------------------------------------------------------+
| start + 17:                                                              |
+--------------------------------------------------------------------------+
| start + 18:                                                              |
+--------------------------------------------------------------------------+
| start + 19:                                                              |
+--------------------------------------------------------------------------+
| start + 20:                                                              |
+--------------------------------------------------------------------------+
| start + 21:                                                              |
+--------------------------------------------------------------------------+
| start + 22:                                                              |
+--------------------------------------------------------------------------+
| start + 23:                                                              |
+--------------------------------------------------------------------------+

**Color Sample Location..**

+-----+-----+----+-----+-----+-----+----+-----+
|     | 0   |    | 1   |     | 2   |    | 3   |
+-----+-----+----+-----+-----+-----+----+-----+
| 0   | Y   |    | Y   | C   | Y   |    | Y   |
+-----+-----+----+-----+-----+-----+----+-----+
| 1   | Y   |    | Y   | C   | Y   |    | Y   |
+-----+-----+----+-----+-----+-----+----+-----+
| 2   | Y   |    | Y   | C   | Y   |    | Y   |
+-----+-----+----+-----+-----+-----+----+-----+
| 3   | Y   |    | Y   | C   | Y   |    | Y   |
+-----+-----+----+-----+-----+-----+----+-----+
