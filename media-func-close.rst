media close()
MANVOL
media-close
Close a media device
#include <unistd.h>
int
close
int
fd
Arguments
=========

``fd``
    FD

Description
===========

Closes the media device. Resources associated with the file descriptor
are freed. The device configuration remain unchanged.

Return Value
============

``close`` returns 0 on success. On error, -1 is returned, and ``errno``
is set appropriately. Possible error codes are:

EBADF
    ``fd`` is not a valid open file descriptor.
