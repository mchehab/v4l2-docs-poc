ioctl VIDIOC\_SUBDEV\_G\_SELECTION, VIDIOC\_SUBDEV\_S\_SELECTION
MANVOL
VIDIOC\_SUBDEV\_G\_SELECTION
VIDIOC\_SUBDEV\_S\_SELECTION
Get or set selection rectangles on a subdev pad
int
ioctl
int
fd
int
request
struct v4l2\_subdev\_selection \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_SUBDEV\_G\_SELECTION, VIDIOC\_SUBDEV\_S\_SELECTION

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

The selections are used to configure various image processing
functionality performed by the subdevs which affect the image size. This
currently includes cropping, scaling and composition.

The selection API replaces `the old subdev crop
API <#vidioc-subdev-g-crop>`__. All the function of the crop API, and
more, are supported by the selections API.

See ? for more information on how each selection target affects the
image processing pipeline inside the subdevice.

Types of selection targets
--------------------------

There are two types of selection targets: actual and bounds. The actual
targets are the targets which configure the hardware. The BOUNDS target
will return a rectangle that contain all possible actual rectangles.

Discovering supported features
------------------------------

To discover which targets are supported, the user can perform
``VIDIOC_SUBDEV_G_SELECTION`` on them. Any unsupported target will
return ``EINVAL``.

Selection targets and flags are documented in ?.

+-------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32     | which         | Active or try selection, from V4L2-SUBDEV-FORMAT-WHENCE.                               |
+-------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32     | pad           | Pad number as reported by the media framework.                                         |
+-------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32     | target        | Target selection rectangle. See ?.                                                     |
+-------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32     | flags         | Flags. See ?.                                                                          |
+-------------+---------------+----------------------------------------------------------------------------------------+
| V4L2-RECT   | r             | Selection rectangle, in pixels.                                                        |
+-------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32     | reserved[8]   | Reserved for future extensions. Applications and drivers must set the array to zero.   |
+-------------+---------------+----------------------------------------------------------------------------------------+

Table: struct v4l2\_subdev\_selection

RETURN-VALUE

EBUSY
    The selection rectangle can't be changed because the pad is
    currently busy. This can be caused, for instance, by an active video
    stream on the pad. The ioctl must not be retried without performing
    another action to fix the problem first. Only returned by
    ``VIDIOC_SUBDEV_S_SELECTION``

EINVAL
    The V4L2-SUBDEV-SELECTION pad references a non-existing pad, the
    which field references a non-existing format, or the selection
    target is not supported on the given subdev pad.
