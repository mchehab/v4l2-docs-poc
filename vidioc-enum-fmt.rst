ioctl VIDIOC\_ENUM\_FMT
MANVOL
VIDIOC\_ENUM\_FMT
Enumerate image formats
int
ioctl
int
fd
int
request
struct v4l2\_fmtdesc \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_ENUM\_FMT

``argp``

Description
===========

To enumerate image formats applications initialize the type and index
field of V4L2-FMTDESC and call the ``VIDIOC_ENUM_FMT`` ioctl with a
pointer to this structure. Drivers fill the rest of the structure or
return an EINVAL. All formats are enumerable by beginning at index zero
and incrementing by one until EINVAL is returned.

Note that after switching input or output the list of enumerated image
formats may be different.

+-----------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32                                                                                                                                       | index             | Number of the format in the enumeration, set by the application. This is in no way related to the pixelformat field.                                                                                                                                                           |
+-----------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32                                                                                                                                       | type              | Type of the data stream, set by the application. Only these types are valid here: ``V4L2_BUF_TYPE_VIDEO_CAPTURE``, ``V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE``, ``V4L2_BUF_TYPE_VIDEO_OUTPUT``, ``V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE`` and ``V4L2_BUF_TYPE_VIDEO_OVERLAY``. See ?.   |
+-----------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32                                                                                                                                       | flags             | See ?                                                                                                                                                                                                                                                                          |
+-----------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u8                                                                                                                                        | description[32]   | Description of the format, a NUL-terminated ASCII string. This information is intended for the user, for example: "YUV 4:2:2".                                                                                                                                                 |
+-----------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32                                                                                                                                       | pixelformat       | The image format identifier. This is a four character code as computed by the v4l2\_fourcc() macro:                                                                                                                                                                            |
+-----------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ::                                                                                                                                            |
|                                                                                                                                               |
|     #define v4l2_fourcc(a,b,c,d) (((__u32)(a)<<0)|((__u32)(b)<<8)|((__u32)(c)<<16)|((__u32)(d)<<24))                                          |
|                                                                                                                                               |
| Several image formats are already defined by this specification in ?. Note these codes are not the same as those used in the Windows world.   |
+-----------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32                                                                                                                                       | reserved[4]       | Reserved for future extensions. Drivers must set the array to zero.                                                                                                                                                                                                            |
+-----------------------------------------------------------------------------------------------------------------------------------------------+-------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_fmtdesc

+--------------------------------+----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_FMT_FLAG_COMPRESSED``   | 0x0001   | This is a compressed format.                                                                                                                                         |
+--------------------------------+----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_FMT_FLAG_EMULATED``     | 0x0002   | This format is not native to the device but emulated through software (usually libv4l2), where possible try to use a native format instead for better performance.   |
+--------------------------------+----------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: Image Format Description Flags

RETURN-VALUE

EINVAL
    The V4L2-FMTDESC type is not supported or the index is out of
    bounds.
