ioctl VIDIOC\_G\_AUDOUT, VIDIOC\_S\_AUDOUT
MANVOL
VIDIOC\_G\_AUDOUT
VIDIOC\_S\_AUDOUT
Query or select the current audio output
int
ioctl
int
fd
int
request
struct v4l2\_audioout \*
argp
int
ioctl
int
fd
int
request
const struct v4l2\_audioout \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_AUDOUT, VIDIOC\_S\_AUDOUT

``argp``

Description
===========

To query the current audio output applications zero out the reserved
array of a V4L2-AUDIOOUT and call the ``VIDIOC_G_AUDOUT`` ioctl with a
pointer to this structure. Drivers fill the rest of the structure or
return an EINVAL when the device has no audio inputs, or none which
combine with the current video output.

Audio outputs have no writable properties. Nevertheless, to select the
current audio output applications can initialize the index field and
reserved array (which in the future may contain writable properties) of
a v4l2\_audioout structure and call the ``VIDIOC_S_AUDOUT`` ioctl.
Drivers switch to the requested output or return the EINVAL when the
index is out of bounds. This is a write-only ioctl, it does not return
the current audio output attributes as ``VIDIOC_G_AUDOUT`` does.

Note connectors on a TV card to loop back the received audio signal to a
sound card are not audio outputs in this sense.

+-----------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | index         | Identifies the audio output, set by the driver or application.                                                                                                                      |
+-----------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u8    | name[32]      | Name of the audio output, a NUL-terminated ASCII string, for example: "Line Out". This information is intended for the user, preferably the connector label on the device itself.   |
+-----------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | capability    | Audio capability flags, none defined yet. Drivers must set this field to zero.                                                                                                      |
+-----------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | mode          | Audio mode, none defined yet. Drivers and applications (on ``VIDIOC_S_AUDOUT``) must set this field to zero.                                                                        |
+-----------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | reserved[2]   | Reserved for future extensions. Drivers and applications must set the array to zero.                                                                                                |
+-----------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_audioout

RETURN-VALUE

EINVAL
    No audio outputs combine with the current video output, or the
    number of the selected audio output is out of bounds or it does not
    combine.
