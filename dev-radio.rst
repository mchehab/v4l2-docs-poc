This interface is intended for AM and FM (analog) radio receivers and
transmitters.

Conventionally V4L2 radio devices are accessed through character device
special files named ``/dev/radio`` and ``/dev/radio0`` to
``/dev/radio63`` with major number 81 and minor numbers 64 to 127.

Querying Capabilities
=====================

Devices supporting the radio interface set the ``V4L2_CAP_RADIO`` and
``V4L2_CAP_TUNER`` or ``V4L2_CAP_MODULATOR`` flag in the capabilities
field of V4L2-CAPABILITY returned by the VIDIOC-QUERYCAP ioctl. Other
combinations of capability flags are reserved for future extensions.

Supplemental Functions
======================

Radio devices can support `controls <#control>`__, and must support the
`tuner or modulator <#tuner>`__ ioctls.

They do not support the video input or output, audio input or output,
video standard, cropping and scaling, compression and streaming
parameter, or overlay ioctls. All other ioctls and I/O methods are
reserved for future extensions.

Programming
===========

Radio devices may have a couple audio controls (as discussed in ?) such
as a volume control, possibly custom controls. Further all radio devices
have one tuner or modulator (these are discussed in ?) with index number
zero to select the radio frequency and to determine if a monaural or FM
stereo program is received/emitted. Drivers switch automatically between
AM and FM depending on the selected frequency. The VIDIOC-G-TUNER or
VIDIOC-G-MODULATOR ioctl reports the supported frequency range.
