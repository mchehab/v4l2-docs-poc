ioctl VIDIOC\_SUBDEV\_ENUM\_FRAME\_INTERVAL
MANVOL
VIDIOC\_SUBDEV\_ENUM\_FRAME\_INTERVAL
Enumerate frame intervals
int
ioctl
int
fd
int
request
struct v4l2\_subdev\_frame\_interval\_enum \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_SUBDEV\_ENUM\_FRAME\_INTERVAL

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

This ioctl lets applications enumerate available frame intervals on a
given sub-device pad. Frame intervals only makes sense for sub-devices
that can control the frame period on their own. This includes, for
instance, image sensors and TV tuners.

For the common use case of image sensors, the frame intervals available
on the sub-device output pad depend on the frame format and size on the
same pad. Applications must thus specify the desired format and size
when enumerating frame intervals.

To enumerate frame intervals applications initialize the index, pad,
which, code, width and height fields of V4L2-SUBDEV-FRAME-INTERVAL-ENUM
and call the ``VIDIOC_SUBDEV_ENUM_FRAME_INTERVAL`` ioctl with a pointer
to this structure. Drivers fill the rest of the structure or return an
EINVAL if one of the input fields is invalid. All frame intervals are
enumerable by beginning at index zero and incrementing by one until
EINVAL is returned.

Available frame intervals may depend on the current 'try' formats at
other pads of the sub-device, as well as on the current active links.
See VIDIOC-SUBDEV-G-FMT for more information about the try formats.

Sub-devices that support the frame interval enumeration ioctl should
implemented it on a single pad only. Its behaviour when supported on
multiple pads of the same sub-device is not defined.

+--------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32      | index         | Number of the format in the enumeration, set by the application.                       |
+--------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32      | pad           | Pad number as reported by the media controller API.                                    |
+--------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32      | code          | The media bus format code, as defined in ?.                                            |
+--------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32      | width         | Frame width, in pixels.                                                                |
+--------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32      | height        | Frame height, in pixels.                                                               |
+--------------+---------------+----------------------------------------------------------------------------------------+
| V4L2-FRACT   | interval      | Period, in seconds, between consecutive video frames.                                  |
+--------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32      | which         | Frame intervals to be enumerated, from V4L2-SUBDEV-FORMAT-WHENCE.                      |
+--------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32      | reserved[8]   | Reserved for future extensions. Applications and drivers must set the array to zero.   |
+--------------+---------------+----------------------------------------------------------------------------------------+

Table: struct v4l2\_subdev\_frame\_interval\_enum

RETURN-VALUE

EINVAL
    The V4L2-SUBDEV-FRAME-INTERVAL-ENUM pad references a non-existing
    pad, one of the code, width or height fields are invalid for the
    given pad or the index field is out of bounds.
