V4L2\_SDR\_FMT\_RU12LE ('RU12')
MANVOL
V4L2\_SDR\_FMT\_RU12LE
Real unsigned 12-bit little endian sample
Description
===========

This format contains sequence of real number samples. Each sample is
represented as a 12 bit unsigned little endian number. Sample is stored
in 16 bit space with unused high bits padded with 0.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
