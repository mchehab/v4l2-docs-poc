The DVB net device controls the mapping of data packages that are part
of a transport stream to be mapped into a virtual network interface,
visible through the standard Linux network protocol stack.

Currently, two encapsulations are supported:

-  `Multi Protocol Encapsulation
   (MPE) <http://en.wikipedia.org/wiki/Multiprotocol_Encapsulation>`__

-  `Ultra Lightweight Encapsulation
   (ULE) <http://en.wikipedia.org/wiki/Unidirectional_Lightweight_Encapsulation>`__

In order to create the Linux virtual network interfaces, an application
needs to tell to the Kernel what are the PIDs and the encapsulation
types that are present on the transport stream. This is done through
``/dev/dvb/adapter?/net?`` device node. The data will be available via
virtual ``dvb?_?`` network interfaces, and will be controled/routed via
the standard ip tools (like ip, route, netstat, ifconfig, etc).

Data types and and ioctl definitions are defined via ``linux/dvb/net.h``
header.

DVB net Function Calls
======================

ioctl NET\_ADD\_IF
MANVOL
NET\_ADD\_IF
Creates a new network interface for a given Packet ID.
int
ioctl
int
fd
int
request
struct dvb\_net\_if \*
net\_if
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_SET\_TONE

``net_if``
    pointer to DVB-NET-IF

Description
===========

The NET\_ADD\_IF ioctl system call selects the Packet ID (PID) that
contains a TCP/IP traffic, the type of encapsulation to be used (MPE or
ULE) and the interface number for the new interface to be created. When
the system call successfully returns, a new virtual network interface is
created.

The DVB-NET-IF::ifnum field will be filled with the number of the
created interface.

RETURN-VALUE-DVB
struct dvb\_net\_if description
===============================

+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| ID         | Description                                                                                                                          |
+============+======================================================================================================================================+
| pid        | Packet ID (PID) of the MPEG-TS that contains data                                                                                    |
+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| ifnum      | number of the DVB interface.                                                                                                         |
+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| feedtype   | Encapsulation type of the feed. It can be: ``DVB_NET_FEEDTYPE_MPE`` for MPE encoding or ``DVB_NET_FEEDTYPE_ULE`` for ULE encoding.   |
+------------+--------------------------------------------------------------------------------------------------------------------------------------+

Table: struct dvb\_net\_if

ioctl NET\_REMOVE\_IF
MANVOL
NET\_REMOVE\_IF
Removes a network interface.
int
ioctl
int
fd
int
request
int
ifnum
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_SET\_TONE

``net_if``
    number of the interface to be removed

Description
===========

The NET\_REMOVE\_IF ioctl deletes an interface previously created via
NET-ADD-IF.

RETURN-VALUE-DVB
ioctl NET\_GET\_IF
MANVOL
NET\_GET\_IF
Read the configuration data of an interface created via NET-ADD-IF.
int
ioctl
int
fd
int
request
struct dvb\_net\_if \*
net\_if
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_SET\_TONE

``net_if``
    pointer to DVB-NET-IF

Description
===========

The NET\_GET\_IF ioctl uses the interface number given by the
DVB-NET-IF::ifnum field and fills the content of DVB-NET-IF with the
packet ID and encapsulation type used on such interface. If the
interface was not created yet with NET-ADD-IF, it will return -1 and
fill the ``errno`` with ``EINVAL`` error code.

RETURN-VALUE-DVB
