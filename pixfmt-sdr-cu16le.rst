V4L2\_SDR\_FMT\_CU16LE ('CU16')
MANVOL
V4L2\_SDR\_FMT\_CU16LE
Complex unsigned 16-bit little endian IQ sample
Description
===========

This format contains sequence of complex number samples. Each complex
number consist two parts, called In-phase and Quadrature (IQ). Both I
and Q are represented as a 16 bit unsigned little endian number. I value
comes first and Q value after that.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 2:                                                               |
+--------------------------------------------------------------------------+
