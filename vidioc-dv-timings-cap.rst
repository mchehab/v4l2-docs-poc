ioctl VIDIOC\_DV\_TIMINGS\_CAP, VIDIOC\_SUBDEV\_DV\_TIMINGS\_CAP
MANVOL
VIDIOC\_DV\_TIMINGS\_CAP
VIDIOC\_SUBDEV\_DV\_TIMINGS\_CAP
The capabilities of the Digital Video receiver/transmitter
int
ioctl
int
fd
int
request
struct v4l2\_dv\_timings\_cap \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_DV\_TIMINGS\_CAP, VIDIOC\_SUBDEV\_DV\_TIMINGS\_CAP

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

To query the capabilities of the DV receiver/transmitter applications
can call the ``VIDIOC_DV_TIMINGS_CAP`` ioctl on a video node and the
driver will fill in the structure. Note that drivers may return
different values after switching the video input or output.

When implemented by the driver DV capabilities of subdevices can be
queried by calling the ``VIDIOC_SUBDEV_DV_TIMINGS_CAP`` ioctl directly
on a subdevice node. The capabilities are specific to inputs (for DV
receivers) or outputs (for DV transmitters), applications must specify
the desired pad number in the V4L2-DV-TIMINGS-CAP pad field. Attempts to
query capabilities on a pad that doesn't support them will return an
EINVAL.

+-----------+-------------------+-------------------------------------------------------------------------------------------------------+
| \_\_u32   | min\_width        | Minimum width of the active video in pixels.                                                          |
+-----------+-------------------+-------------------------------------------------------------------------------------------------------+
| \_\_u32   | max\_width        | Maximum width of the active video in pixels.                                                          |
+-----------+-------------------+-------------------------------------------------------------------------------------------------------+
| \_\_u32   | min\_height       | Minimum height of the active video in lines.                                                          |
+-----------+-------------------+-------------------------------------------------------------------------------------------------------+
| \_\_u32   | max\_height       | Maximum height of the active video in lines.                                                          |
+-----------+-------------------+-------------------------------------------------------------------------------------------------------+
| \_\_u64   | min\_pixelclock   | Minimum pixelclock frequency in Hz.                                                                   |
+-----------+-------------------+-------------------------------------------------------------------------------------------------------+
| \_\_u64   | max\_pixelclock   | Maximum pixelclock frequency in Hz.                                                                   |
+-----------+-------------------+-------------------------------------------------------------------------------------------------------+
| \_\_u32   | standards         | The video standard(s) supported by the hardware. See ? for a list of standards.                       |
+-----------+-------------------+-------------------------------------------------------------------------------------------------------+
| \_\_u32   | capabilities      | Several flags giving more information about the capabilities. See ? for a description of the flags.   |
+-----------+-------------------+-------------------------------------------------------------------------------------------------------+
| \_\_u32   | reserved[16]      | Reserved for future extensions. Drivers must set the array to zero.                                   |
+-----------+-------------------+-------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_bt\_timings\_cap

+-----------+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------+
| \_\_u32   | type                  | Type of DV timings as listed in ?.                                                                                                                                                         |
+-----------+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------+
| \_\_u32   | pad                   | Pad number as reported by the media controller API. This field is only used when operating on a subdevice node. When operating on a video node applications must set this field to zero.   |
+-----------+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------+
| \_\_u32   | reserved[2]           | Reserved for future extensions. Drivers must set the array to zero.                                                                                                                        |
+-----------+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------+
| union     |                       |                                                                                                                                                                                            |
+-----------+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------+
|           | V4L2-BT-TIMINGS-CAP   | bt                                                                                                                                                                                         | BT.656/1120 timings capabilities of the hardware.   |
+-----------+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------+
|           | \_\_u32               | raw\_data[32]                                                                                                                                                                              |                                                     |
+-----------+-----------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------+

Table: struct v4l2\_dv\_timings\_cap

+----------------------------------------+-------------------------------------------------------------------------------------------------------------+
| Flag                                   | Description                                                                                                 |
+----------------------------------------+-------------------------------------------------------------------------------------------------------------+
+----------------------------------------+-------------------------------------------------------------------------------------------------------------+
| V4L2\_DV\_BT\_CAP\_INTERLACED          | Interlaced formats are supported.                                                                           |
+----------------------------------------+-------------------------------------------------------------------------------------------------------------+
| V4L2\_DV\_BT\_CAP\_PROGRESSIVE         | Progressive formats are supported.                                                                          |
+----------------------------------------+-------------------------------------------------------------------------------------------------------------+
| V4L2\_DV\_BT\_CAP\_REDUCED\_BLANKING   | CVT/GTF specific: the timings can make use of reduced blanking (CVT) or the 'Secondary GTF' curve (GTF).    |
+----------------------------------------+-------------------------------------------------------------------------------------------------------------+
| V4L2\_DV\_BT\_CAP\_CUSTOM              | Can support non-standard timings, i.e. timings not belonging to the standards set in the standards field.   |
+----------------------------------------+-------------------------------------------------------------------------------------------------------------+

Table: DV BT Timing capabilities

RETURN-VALUE
