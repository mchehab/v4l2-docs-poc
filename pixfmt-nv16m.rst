V4L2\_PIX\_FMT\_NV16M ('NM16'), V4L2\_PIX\_FMT\_NV61M ('NM61')
MANVOL
V4L2\_PIX\_FMT\_NV16M
V4L2\_PIX\_FMT\_NV61M
Variation of
V4L2\_PIX\_FMT\_NV16
and
V4L2\_PIX\_FMT\_NV61
with planes non contiguous in memory.
Description
===========

This is a multi-planar, two-plane version of the YUV 4:2:2 format. The
three components are separated into two sub-images or planes.
``V4L2_PIX_FMT_NV16M`` differs from ``V4L2_PIX_FMT_NV16
`` in that the two planes are non-contiguous in memory, i.e. the chroma
plane does not necessarily immediately follow the luma plane. The
luminance data occupies the first plane. The Y plane has one byte per
pixel. In the second plane there is chrominance data with alternating
chroma samples. The CbCr plane is the same width and height, in bytes,
as the Y plane. Each CbCr pair belongs to two pixels. For example,
Cb\ :sub:`0`/Cr:sub:`0` belongs to Y'\ :sub:`00`, Y'\ :sub:`01`.
``V4L2_PIX_FMT_NV61M`` is the same as ``V4L2_PIX_FMT_NV16M`` except the
Cb and Cr bytes are swapped, the CrCb plane starts with a Cr byte.

``V4L2_PIX_FMT_NV16M`` and ``V4L2_PIX_FMT_NV61M`` are intended to be
used only in drivers and applications that support the multi-planar API,
described in ?.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start0 + 0:                                                              |
+--------------------------------------------------------------------------+
| start0 + 4:                                                              |
+--------------------------------------------------------------------------+
| start0 + 8:                                                              |
+--------------------------------------------------------------------------+
| start0 + 12:                                                             |
+--------------------------------------------------------------------------+
+--------------------------------------------------------------------------+
| start1 + 0:                                                              |
+--------------------------------------------------------------------------+
| start1 + 4:                                                              |
+--------------------------------------------------------------------------+
| start1 + 8:                                                              |
+--------------------------------------------------------------------------+
| start1 + 12:                                                             |
+--------------------------------------------------------------------------+

**Color Sample Location..**

+-----+-----+-----+-----+----+-----+-----+-----+
|     | 0   |     | 1   |    | 2   |     | 3   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 0   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
|     |     | C   |     |    |     | C   |     |
+-----+-----+-----+-----+----+-----+-----+-----+
| 1   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
|     |     | C   |     |    |     | C   |     |
+-----+-----+-----+-----+----+-----+-----+-----+
+-----+-----+-----+-----+----+-----+-----+-----+
| 2   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
|     |     | C   |     |    |     | C   |     |
+-----+-----+-----+-----+----+-----+-----+-----+
| 3   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
|     |     | C   |     |    |     | C   |     |
+-----+-----+-----+-----+----+-----+-----+-----+
