V4L2\_PIX\_FMT\_Y16\_BE ('Y16 ' \| (1 << 31))
MANVOL
V4L2\_PIX\_FMT\_Y16\_BE
Grey-scale image
Description
===========

This is a grey-scale image with a depth of 16 bits per pixel. The most
significant byte is stored at lower memory addresses (big-endian). Note
the actual sampling precision may be lower than 16 bits, for example 10
bits per pixel with values in range 0 to 1023.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 8:                                                               |
+--------------------------------------------------------------------------+
| start + 16:                                                              |
+--------------------------------------------------------------------------+
| start + 24:                                                              |
+--------------------------------------------------------------------------+
