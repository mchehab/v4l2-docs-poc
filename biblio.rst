References
==========

CEA 608-E Consumer Electronics Association (http://www.ce.org)

EN 300 294 European Telecommunication Standards Institute
(http://www.etsi.org)

ETS 300 231 European Telecommunication Standards Institute
(http://www.etsi.org)

ETS 300 706 European Telecommunication Standards Institute
(http://www.etsi.org)

ISO 13818-1 International Telecommunication Union (http://www.itu.ch),
International Organisation for Standardisation (http://www.iso.ch)

ISO 13818-2 International Telecommunication Union (http://www.itu.ch),
International Organisation for Standardisation (http://www.iso.ch)

ITU BT.470 International Telecommunication Union (http://www.itu.ch)

ITU BT.601 International Telecommunication Union (http://www.itu.ch)

ITU BT.653 International Telecommunication Union (http://www.itu.ch)

ITU BT.709 International Telecommunication Union (http://www.itu.ch)

ITU BT.1119 International Telecommunication Union (http://www.itu.ch)

JFIF Independent JPEG Group (http://www.ijg.org) Version 1.02

ITU-T.81 International Telecommunication Union (http://www.itu.int)

W3C JPEG JFIF The World Wide Web Consortium
(`http://www.w3.org <http://www.w3.org/Graphics/JPEG>`__)

SMPTE 12M Society of Motion Picture and Television Engineers
(http://www.smpte.org)

SMPTE 170M Society of Motion Picture and Television Engineers
(http://www.smpte.org)

SMPTE 240M Society of Motion Picture and Television Engineers
(http://www.smpte.org)

SMPTE RP 431-2 Society of Motion Picture and Television Engineers
(http://www.smpte.org)

SMPTE ST 2084 Society of Motion Picture and Television Engineers
(http://www.smpte.org)

sRGB International Electrotechnical Commission (http://www.iec.ch)

sYCC International Electrotechnical Commission (http://www.iec.ch)

xvYCC International Electrotechnical Commission (http://www.iec.ch)

AdobeRGB Adobe Systems Incorporated (http://www.adobe.com)

opRGB International Electrotechnical Commission (http://www.iec.ch)

ITU BT.2020 International Telecommunication Union (http://www.itu.ch)

EBU Tech 3213 European Broadcast Union (http://www.ebu.ch)

IEC 62106 International Electrotechnical Commission (http://www.iec.ch)

NRSC-4-B National Radio Systems Committee (http://www.nrscstandards.org)

ISO 12232:2006 International Organization for Standardization
(http://www.iso.org)

CEA-861-E Consumer Electronics Association (http://www.ce.org)

VESA DMT Video Electronics Standards Association (http://www.vesa.org)

EDID Video Electronics Standards Association (http://www.vesa.org)
Release A, Revision 2

HDCP Digital Content Protection LLC (http://www.digital-cp.com) Revision
1.3

HDMI HDMI Licensing LLC (http://www.hdmi.org) Specification Version 1.4a

DP Video Electronics Standards Association (http://www.vesa.org) Version
1, Revision 2

poynton Charles Poynton

colimg Erik Reinhard et al.
