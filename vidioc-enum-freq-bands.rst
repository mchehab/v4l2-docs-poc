ioctl VIDIOC\_ENUM\_FREQ\_BANDS
MANVOL
VIDIOC\_ENUM\_FREQ\_BANDS
Enumerate supported frequency bands
int
ioctl
int
fd
int
request
struct v4l2\_frequency\_band \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_ENUM\_FREQ\_BANDS

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

Enumerates the frequency bands that a tuner or modulator supports. To do
this applications initialize the tuner, type and index fields, and zero
out the reserved array of a V4L2-FREQUENCY-BAND and call the
``VIDIOC_ENUM_FREQ_BANDS`` ioctl with a pointer to this structure.

This ioctl is supported if the ``V4L2_TUNER_CAP_FREQ_BANDS`` capability
of the corresponding tuner/modulator is set.

+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | tuner         | The tuner or modulator index number. This is the same value as in the V4L2-INPUT tuner field and the V4L2-TUNER index field, or the V4L2-OUTPUT modulator field and the V4L2-MODULATOR index field.                                                                                                                   |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | type          | The tuner type. This is the same value as in the V4L2-TUNER type field. The type must be set to ``V4L2_TUNER_RADIO`` for ``/dev/radioX`` device nodes, and to ``V4L2_TUNER_ANALOG_TV`` for all others. Set this field to ``V4L2_TUNER_RADIO`` for modulators (currently only radio modulators are supported). See ?   |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | index         | Identifies the frequency band, set by the application.                                                                                                                                                                                                                                                                |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | capability    | The tuner/modulator capability flags for this frequency band, see ?. The ``V4L2_TUNER_CAP_LOW`` or ``V4L2_TUNER_CAP_1HZ`` capability must be the same for all frequency bands of the selected tuner/modulator. So either all bands have that capability set, or none of them have that capability.                    |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | rangelow      | The lowest tunable frequency in units of 62.5 kHz, or if the capability flag ``V4L2_TUNER_CAP_LOW`` is set, in units of 62.5 Hz, for this frequency band. A 1 Hz unit is used when the capability flag ``V4L2_TUNER_CAP_1HZ`` is set.                                                                                 |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | rangehigh     | The highest tunable frequency in units of 62.5 kHz, or if the capability flag ``V4L2_TUNER_CAP_LOW`` is set, in units of 62.5 Hz, for this frequency band. A 1 Hz unit is used when the capability flag ``V4L2_TUNER_CAP_1HZ`` is set.                                                                                |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | modulation    | The supported modulation systems of this frequency band. See ?. Note that currently only one modulation system per frequency band is supported. More work will need to be done if multiple modulation systems are possible. Contact the linux-media mailing list (V4L-ML) if you need that functionality.             |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | reserved[9]   | Reserved for future extensions. Applications and drivers must set the array to zero.                                                                                                                                                                                                                                  |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_frequency\_band

+--------------------------------+--------+---------------------------------------------------------+
| ``V4L2_BAND_MODULATION_VSB``   | 0x02   | Vestigial Sideband modulation, used for analog TV.      |
+--------------------------------+--------+---------------------------------------------------------+
| ``V4L2_BAND_MODULATION_FM``    | 0x04   | Frequency Modulation, commonly used for analog radio.   |
+--------------------------------+--------+---------------------------------------------------------+
| ``V4L2_BAND_MODULATION_AM``    | 0x08   | Amplitude Modulation, commonly used for analog radio.   |
+--------------------------------+--------+---------------------------------------------------------+

Table: Band Modulation Systems

RETURN-VALUE

EINVAL
    The tuner or index is out of bounds or the type field is wrong.
