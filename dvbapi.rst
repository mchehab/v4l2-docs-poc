2002
2003
Convergence GmbH
2009-2015
Mauro Carvalho Chehab
2.1.0
DocBook improvements and cleanups, in order to document the system calls
on a more standard way and provide more description about the current
DVB API.
2.0.4
Add more information about DVB APIv5, better describing the frontend
GET/SET props ioctl's.
2.0.3
Add some frontend capabilities flags, present on kernel, but missing at
the specs.
2.0.2
documents FE\_SET\_FRONTEND\_TUNE\_MODE and
FE\_DISHETWORK\_SEND\_LEGACY\_CMD ioctls.
2.0.1
Added ISDB-T test originally written by Patrick Boettcher
2.0.0
Conversion from LaTex to DocBook XML. The contents is the same as the
original LaTex version.
1.0.0
Initial revision on LaTEX.
Version 5.10

.. toctree::
   :maxdepth: 2

   intro
   frontend
   debux
   ca
   net

DVB Deprecated APIs
===================

The APIs described here are kept only for historical reasons. There's
just one driver for a very legacy hardware that uses this API. No modern
drivers should use it. Instead, audio and video should be using the V4L2
and ALSA APIs, and the pipelines should be set using the Media
Controller API

.. toctree::
   :maxdepth: 2

   video
   audio
   examples

DVB Audio Header File
=====================

.. toctree::
   :maxdepth: 2

   audio-h

DVB Conditional Access Header File
==================================

.. toctree::
   :maxdepth: 2

   ca-h

DVB Demux Header File
=====================

.. toctree::
   :maxdepth: 2

   dmx-h


DVB Frontend Header File
========================

.. toctree::
   :maxdepth: 2

   frontend-h

DVB Network Header File
=======================

.. toctree::
   :maxdepth: 2

   net-h

DVB Video Header File
=====================

.. toctree::
   :maxdepth: 2

   video-h
