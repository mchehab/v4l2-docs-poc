V4L2\_PIX\_FMT\_YUV422M ('YM16'), V4L2\_PIX\_FMT\_YVU422M ('YM61')
MANVOL
V4L2\_PIX\_FMT\_YUV422M
V4L2\_PIX\_FMT\_YVU422M
Planar formats with ½ horizontal resolution, also known as YUV and YVU
4:2:2
Description
===========

This is a multi-planar format, as opposed to a packed format. The three
components are separated into three sub-images or planes.

The Y plane is first. The Y plane has one byte per pixel. For
``V4L2_PIX_FMT_YUV422M`` the Cb data constitutes the second plane which
is half the width of the Y plane (and of the image). Each Cb belongs to
two pixels. For example, Cb\ :sub:`0` belongs to Y'\ :sub:`00`,
Y'\ :sub:`01`. The Cr data, just like the Cb plane, is in the third
plane.

``V4L2_PIX_FMT_YVU422M`` is the same except the Cr data is stored in the
second plane and the Cb data in the third plane.

If the Y plane has pad bytes after each row, then the Cb and Cr planes
have half as many pad bytes after their rows. In other words, two Cx
rows (including padding) is exactly as long as one Y row (including
padding).

``V4L2_PIX_FMT_YUV422M`` and ``V4L2_PIX_FMT_YVU422M`` are intended to be
used only in drivers and applications that support the multi-planar API,
described in ?.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start0 + 0:                                                              |
+--------------------------------------------------------------------------+
| start0 + 4:                                                              |
+--------------------------------------------------------------------------+
| start0 + 8:                                                              |
+--------------------------------------------------------------------------+
| start0 + 12:                                                             |
+--------------------------------------------------------------------------+
+--------------------------------------------------------------------------+
| start1 + 0:                                                              |
+--------------------------------------------------------------------------+
| start1 + 2:                                                              |
+--------------------------------------------------------------------------+
| start1 + 4:                                                              |
+--------------------------------------------------------------------------+
| start1 + 6:                                                              |
+--------------------------------------------------------------------------+
+--------------------------------------------------------------------------+
| start2 + 0:                                                              |
+--------------------------------------------------------------------------+
| start2 + 2:                                                              |
+--------------------------------------------------------------------------+
| start2 + 4:                                                              |
+--------------------------------------------------------------------------+
| start2 + 6:                                                              |
+--------------------------------------------------------------------------+

**Color Sample Location..**

+-----+-----+-----+-----+----+-----+-----+-----+
|     | 0   |     | 1   |    | 2   |     | 3   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 0   | Y   | C   | Y   |    | Y   | C   | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 1   | Y   | C   | Y   |    | Y   | C   | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 2   | Y   | C   | Y   |    | Y   | C   | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 3   | Y   | C   | Y   |    | Y   | C   | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
