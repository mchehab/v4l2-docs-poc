ioctl VIDIOC\_G\_SELECTION, VIDIOC\_S\_SELECTION
MANVOL
VIDIOC\_G\_SELECTION
VIDIOC\_S\_SELECTION
Get or set one of the selection rectangles
int
ioctl
int
fd
int
request
struct v4l2\_selection \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_SELECTION, VIDIOC\_S\_SELECTION

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

The ioctls are used to query and configure selection rectangles.

To query the cropping (composing) rectangle set V4L2-SELECTION type
field to the respective buffer type. Do not use the multiplanar buffer
types. Use ``V4L2_BUF_TYPE_VIDEO_CAPTURE`` instead of
``V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE`` and use
``V4L2_BUF_TYPE_VIDEO_OUTPUT`` instead of
``V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE``. The next step is setting the
value of V4L2-SELECTION target field to ``V4L2_SEL_TGT_CROP``
(``V4L2_SEL_TGT_COMPOSE``). Please refer to table ? or ? for additional
targets. The flags and reserved fields of V4L2-SELECTION are ignored and
they must be filled with zeros. The driver fills the rest of the
structure or returns EINVAL if incorrect buffer type or target was used.
If cropping (composing) is not supported then the active rectangle is
not mutable and it is always equal to the bounds rectangle. Finally, the
V4L2-RECT r rectangle is filled with the current cropping (composing)
coordinates. The coordinates are expressed in driver-dependent units.
The only exception are rectangles for images in raw formats, whose
coordinates are always expressed in pixels.

To change the cropping (composing) rectangle set the V4L2-SELECTION type
field to the respective buffer type. Do not use multiplanar buffers. Use
``V4L2_BUF_TYPE_VIDEO_CAPTURE`` instead of
``V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE``. Use
``V4L2_BUF_TYPE_VIDEO_OUTPUT`` instead of
``V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE``. The next step is setting the
value of V4L2-SELECTION target to ``V4L2_SEL_TGT_CROP``
(``V4L2_SEL_TGT_COMPOSE``). Please refer to table ? or ? for additional
targets. The V4L2-RECT r rectangle need to be set to the desired active
area. Field V4L2-SELECTION reserved is ignored and must be filled with
zeros. The driver may adjust coordinates of the requested rectangle. An
application may introduce constraints to control rounding behaviour. The
V4L2-SELECTION flags field must be set to one of the following:

-  ``0`` - The driver can adjust the rectangle size freely and shall
   choose a crop/compose rectangle as close as possible to the requested
   one.

-  ``V4L2_SEL_FLAG_GE`` - The driver is not allowed to shrink the
   rectangle. The original rectangle must lay inside the adjusted one.

-  ``V4L2_SEL_FLAG_LE`` - The driver is not allowed to enlarge the
   rectangle. The adjusted rectangle must lay inside the original one.

-  ``V4L2_SEL_FLAG_GE | V4L2_SEL_FLAG_LE`` - The driver must choose the
   size exactly the same as in the requested rectangle.

Please refer to ?.

The driver may have to adjusts the requested dimensions against hardware
limits and other parts as the pipeline, i.e. the bounds given by the
capture/output window or TV display. The closest possible values of
horizontal and vertical offset and sizes are chosen according to
following priority:

1. Satisfy constraints from V4L2-SELECTION flags.

2. Adjust width, height, left, and top to hardware limits and
   alignments.

3. Keep center of adjusted rectangle as close as possible to the
   original one.

4. Keep width and height as close as possible to original ones.

5. Keep horizontal and vertical offset as close as possible to original
   ones.

On success the V4L2-RECT r field contains the adjusted rectangle. When
the parameters are unsuitable the application may modify the cropping
(composing) or image parameters and repeat the cycle until satisfactory
parameters have been negotiated. If constraints flags have to be
violated at then ERANGE is returned. The error indicates that *there
exist no rectangle* that satisfies the constraints.

Selection targets and flags are documented in ?.

Behaviour of rectangle adjustment for different constraint flags.

+-------------+---------------+----------------------------------------------------------------------------------------------------------------+
| \_\_u32     | type          | Type of the buffer (from V4L2-BUF-TYPE).                                                                       |
+-------------+---------------+----------------------------------------------------------------------------------------------------------------+
| \_\_u32     | target        | Used to select between `cropping and composing rectangles <#v4l2-selections-common>`__.                        |
+-------------+---------------+----------------------------------------------------------------------------------------------------------------+
| \_\_u32     | flags         | Flags controlling the selection rectangle adjustments, refer to `selection flags <#v4l2-selection-flags>`__.   |
+-------------+---------------+----------------------------------------------------------------------------------------------------------------+
| V4L2-RECT   | r             | The selection rectangle.                                                                                       |
+-------------+---------------+----------------------------------------------------------------------------------------------------------------+
| \_\_u32     | reserved[9]   | Reserved fields for future use. Drivers and applications must zero this array.                                 |
+-------------+---------------+----------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_selection

RETURN-VALUE

EINVAL
    Given buffer type type or the selection target target is not
    supported, or the flags argument is not valid.

ERANGE
    It is not possible to adjust V4L2-RECT r rectangle to satisfy all
    contraints given in the flags argument.

EBUSY
    It is not possible to apply change of the selection rectangle at the
    moment. Usually because streaming is in progress.
