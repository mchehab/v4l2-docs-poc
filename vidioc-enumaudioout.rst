ioctl VIDIOC\_ENUMAUDOUT
MANVOL
VIDIOC\_ENUMAUDOUT
Enumerate audio outputs
int
ioctl
int
fd
int
request
struct v4l2\_audioout \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_ENUMAUDOUT

``argp``

Description
===========

To query the attributes of an audio output applications initialize the
index field and zero out the reserved array of a V4L2-AUDIOOUT and call
the ``VIDIOC_G_AUDOUT`` ioctl with a pointer to this structure. Drivers
fill the rest of the structure or return an EINVAL when the index is out
of bounds. To enumerate all audio outputs applications shall begin at
index zero, incrementing by one until the driver returns EINVAL.

Note connectors on a TV card to loop back the received audio signal to a
sound card are not audio outputs in this sense.

See ? for a description of V4L2-AUDIOOUT.

RETURN-VALUE

EINVAL
    The number of the audio output is out of bounds.
