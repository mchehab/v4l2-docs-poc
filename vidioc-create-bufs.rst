ioctl VIDIOC\_CREATE\_BUFS
MANVOL
VIDIOC\_CREATE\_BUFS
Create buffers for Memory Mapped or User Pointer or DMA Buffer I/O
int
ioctl
int
fd
int
request
struct v4l2\_create\_buffers \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_CREATE\_BUFS

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

This ioctl is used to create buffers for `memory mapped <#mmap>`__ or
`user pointer <#userp>`__ or `DMA buffer <#dmabuf>`__ I/O. It can be
used as an alternative or in addition to the VIDIOC-REQBUFS ioctl, when
a tighter control over buffers is required. This ioctl can be called
multiple times to create buffers of different sizes.

To allocate the device buffers applications must initialize the relevant
fields of the v4l2\_create\_buffers structure. The count field must be
set to the number of requested buffers, the memory field specifies the
requested I/O method and the reserved array must be zeroed.

The format field specifies the image format that the buffers must be
able to handle. The application has to fill in this V4L2-FORMAT. Usually
this will be done using the VIDIOC-TRY-FMT or VIDIOC-G-FMT ioctls to
ensure that the requested format is supported by the driver. Based on
the format's type field the requested buffer size (for single-planar) or
plane sizes (for multi-planar formats) will be used for the allocated
buffers. The driver may return an error if the size(s) are not supported
by the hardware (usually because they are too small).

The buffers created by this ioctl will have as minimum size the size
defined by the format.pix.sizeimage field (or the corresponding fields
for other format types). Usually if the format.pix.sizeimage field is
less than the minimum required for the given format, then an error will
be returned since drivers will typically not allow this. If it is
larger, then the value will be used as-is. In other words, the driver
may reject the requested size, but if it is accepted the driver will use
it unchanged.

When the ioctl is called with a pointer to this structure the driver
will attempt to allocate up to the requested number of buffers and store
the actual number allocated and the starting index in the count and the
index fields respectively. On return count can be smaller than the
number requested.

+---------------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32       | index         | The starting buffer index, returned by the driver.                                                                                                                                                                                                                                                                                                                                |
+---------------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32       | count         | The number of buffers requested or granted. If count == 0, then ``VIDIOC_CREATE_BUFS`` will set index to the current number of created buffers, and it will check the validity of memory and format.type. If those are invalid -1 is returned and errno is set to EINVAL, otherwise ``VIDIOC_CREATE_BUFS`` returns 0. It will never set errno to EBUSY in this particular case.   |
+---------------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32       | memory        | Applications set this field to ``V4L2_MEMORY_MMAP``, ``V4L2_MEMORY_DMABUF`` or ``V4L2_MEMORY_USERPTR``. See ?                                                                                                                                                                                                                                                                     |
+---------------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| V4L2-FORMAT   | format        | Filled in by the application, preserved by the driver.                                                                                                                                                                                                                                                                                                                            |
+---------------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32       | reserved[8]   | A place holder for future extensions. Drivers and applications must set the array to zero.                                                                                                                                                                                                                                                                                        |
+---------------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_create\_buffers

RETURN-VALUE

ENOMEM
    No memory to allocate buffers for `memory mapped <#mmap>`__ I/O.

EINVAL
    The buffer type (format.type field), requested I/O method (memory)
    or format (format field) is not valid.
