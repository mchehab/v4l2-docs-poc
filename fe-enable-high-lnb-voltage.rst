ioctl FE\_ENABLE\_HIGH\_LNB\_VOLTAGE
MANVOL
FE\_ENABLE\_HIGH\_LNB\_VOLTAGE
Select output DC level between normal LNBf voltages or higher LNBf
voltages.
int
ioctl
int
fd
int
request
unsigned int
high
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_ENABLE\_HIGH\_LNB\_VOLTAGE

``high``
    Valid flags:

    -  0 - normal 13V and 18V.

    -  >0 - enables slightly higher voltages instead of 13/18V, in order
       to compensate for long antenna cables.

Description
===========

Select output DC level between normal LNBf voltages or higher LNBf
voltages between 0 (normal) or a value grater than 0 for higher
voltages.

RETURN-VALUE-DVB
