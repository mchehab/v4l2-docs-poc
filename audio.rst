The DVB audio device controls the MPEG2 audio decoder of the DVB
hardware. It can be accessed through ``/dev/dvb/adapter?/audio?``. Data
types and and ioctl definitions can be accessed by including
``linux/dvb/audio.h`` in your application.

Please note that some DVB cards don’t have their own MPEG decoder, which
results in the omission of the audio and video device.

These ioctls were also used by V4L2 to control MPEG decoders implemented
in V4L2. The use of these ioctls for that purpose has been made obsolete
and proper V4L2 ioctls or controls have been created to replace that
functionality.

Audio Data Types
================

This section describes the structures, data types and defines used when
talking to the audio device.

audio\_stream\_source\_t
------------------------

The audio stream source is set through the AUDIO\_SELECT\_SOURCE call
and can take the following values, depending on whether we are replaying
from an internal (demux) or external (user write) source.

::

    typedef enum {
        AUDIO_SOURCE_DEMUX,
        AUDIO_SOURCE_MEMORY
    } audio_stream_source_t;

AUDIO\_SOURCE\_DEMUX selects the demultiplexer (fed either by the
frontend or the DVR device) as the source of the video stream. If
AUDIO\_SOURCE\_MEMORY is selected the stream comes from the application
through the ``write()`` system call.

audio\_play\_state\_t
---------------------

The following values can be returned by the AUDIO\_GET\_STATUS call
representing the state of audio playback.

::

    typedef enum {
        AUDIO_STOPPED,
        AUDIO_PLAYING,
        AUDIO_PAUSED
    } audio_play_state_t;

audio\_channel\_select\_t
-------------------------

The audio channel selected via AUDIO\_CHANNEL\_SELECT is determined by
the following values.

::

    typedef enum {
        AUDIO_STEREO,
        AUDIO_MONO_LEFT,
        AUDIO_MONO_RIGHT,
        AUDIO_MONO,
        AUDIO_STEREO_SWAPPED
    } audio_channel_select_t;

struct audio\_status
--------------------

The AUDIO\_GET\_STATUS call returns the following structure informing
about various states of the playback operation.

::

    typedef struct audio_status {
        boolean AV_sync_state;
        boolean mute_state;
        audio_play_state_t play_state;
        audio_stream_source_t stream_source;
        audio_channel_select_t channel_select;
        boolean bypass_mode;
        audio_mixer_t mixer_state;
    } audio_status_t;

struct audio\_mixer
-------------------

The following structure is used by the AUDIO\_SET\_MIXER call to set the
audio volume.

::

    typedef struct audio_mixer {
        unsigned int volume_left;
        unsigned int volume_right;
    } audio_mixer_t;

audio encodings
---------------

A call to AUDIO\_GET\_CAPABILITIES returns an unsigned integer with the
following bits set according to the hardwares capabilities.

::

     #define AUDIO_CAP_DTS    1
     #define AUDIO_CAP_LPCM   2
     #define AUDIO_CAP_MP1    4
     #define AUDIO_CAP_MP2    8
     #define AUDIO_CAP_MP3   16
     #define AUDIO_CAP_AAC   32
     #define AUDIO_CAP_OGG   64
     #define AUDIO_CAP_SDDS 128
     #define AUDIO_CAP_AC3  256

struct audio\_karaoke
---------------------

The ioctl AUDIO\_SET\_KARAOKE uses the following format:

::

    typedef
    struct audio_karaoke {
        int vocal1;
        int vocal2;
        int melody;
    } audio_karaoke_t;

If Vocal1 or Vocal2 are non-zero, they get mixed into left and right t
at 70% each. If both, Vocal1 and Vocal2 are non-zero, Vocal1 gets mixed
into the left channel and Vocal2 into the right channel at 100% each. Ff
Melody is non-zero, the melody channel gets mixed into left and right.

audio attributes
----------------

The following attributes can be set by a call to AUDIO\_SET\_ATTRIBUTES:

::

     typedef uint16_t audio_attributes_t;
     /⋆   bits: descr. ⋆/
     /⋆   15-13 audio coding mode (0=ac3, 2=mpeg1, 3=mpeg2ext, 4=LPCM, 6=DTS, ⋆/
     /⋆   12    multichannel extension ⋆/
     /⋆   11-10 audio type (0=not spec, 1=language included) ⋆/
     /⋆    9- 8 audio application mode (0=not spec, 1=karaoke, 2=surround) ⋆/
     /⋆    7- 6 Quantization / DRC (mpeg audio: 1=DRC exists)(lpcm: 0=16bit,  ⋆/
     /⋆    5- 4 Sample frequency fs (0=48kHz, 1=96kHz) ⋆/
     /⋆    2- 0 number of audio channels (n+1 channels) ⋆/

Audio Function Calls
====================

open()
------

DESCRIPTION

+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This system call opens a named audio device (e.g. /dev/dvb/adapter0/audio0) for subsequent use. When an open() call has succeeded, the device will be ready for use. The significance of blocking or non-blocking mode is described in the documentation for functions where there is a difference. It does not affect the semantics of the open() call itself. A device opened in blocking mode can later be put into non-blocking mode (and vice versa) using the F\_SETFL command of the fcntl system call. This is a standard system call, documented in the Linux manual page for fcntl. Only one user can open the Audio Device in O\_RDWR mode. All other attempts to open the device in this mode will fail, and an error code will be returned. If the Audio Device is opened in O\_RDONLY mode, the only ioctl call that can be used is AUDIO\_GET\_STATUS. All other call will return with an error code.   |
+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+------------------------------------------------+
| int open(const char ⋆deviceName, int flags);   |
+------------------------------------------------+

PARAMETERS

+---------------------------+-----------------------------------------+
| const char \*deviceName   | Name of specific audio device.          |
+---------------------------+-----------------------------------------+
| int flags                 | A bit-wise OR of the following flags:   |
+---------------------------+-----------------------------------------+
|                           | O\_RDONLY read-only access              |
+---------------------------+-----------------------------------------+
|                           | O\_RDWR read/write access               |
+---------------------------+-----------------------------------------+
|                           | O\_NONBLOCK open in non-blocking mode   |
+---------------------------+-----------------------------------------+
|                           | (blocking mode is the default)          |
+---------------------------+-----------------------------------------+

RETURN VALUE

+----------+---------------------------------------+
| ENODEV   | Device driver not loaded/available.   |
+----------+---------------------------------------+
| EBUSY    | Device or resource busy.              |
+----------+---------------------------------------+
| EINVAL   | Invalid argument.                     |
+----------+---------------------------------------+

close()
-------

DESCRIPTION

+-------------------------------------------------------------+
| This system call closes a previously opened audio device.   |
+-------------------------------------------------------------+

SYNOPSIS

+----------------------+
| int close(int fd);   |
+----------------------+

PARAMETERS

+----------+----------------------------------------------------------+
| int fd   | File descriptor returned by a previous call to open().   |
+----------+----------------------------------------------------------+

RETURN VALUE

+---------+-------------------------------------------+
| EBADF   | fd is not a valid open file descriptor.   |
+---------+-------------------------------------------+

write()
-------

DESCRIPTION

+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This system call can only be used if AUDIO\_SOURCE\_MEMORY is selected in the ioctl call AUDIO\_SELECT\_SOURCE. The data provided shall be in PES format. If O\_NONBLOCK is not specified the function will block until buffer space is available. The amount of data to be transferred is implied by count.   |
+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+----------------------------------------------------------+
| size\_t write(int fd, const void ⋆buf, size\_t count);   |
+----------------------------------------------------------+

PARAMETERS

+-----------------+----------------------------------------------------------+
| int fd          | File descriptor returned by a previous call to open().   |
+-----------------+----------------------------------------------------------+
| void \*buf      | Pointer to the buffer containing the PES data.           |
+-----------------+----------------------------------------------------------+
| size\_t count   | Size of buf.                                             |
+-----------------+----------------------------------------------------------+

RETURN VALUE

+----------+-------------------------------------------------------------------+
| EPERM    | Mode AUDIO\_SOURCE\_MEMORY not selected.                          |
+----------+-------------------------------------------------------------------+
| ENOMEM   | Attempted to write more data than the internal buffer can hold.   |
+----------+-------------------------------------------------------------------+
| EBADF    | fd is not a valid open file descriptor.                           |
+----------+-------------------------------------------------------------------+

AUDIO\_STOP
-----------

DESCRIPTION

+-----------------------------------------------------------------------------+
| This ioctl call asks the Audio Device to stop playing the current stream.   |
+-----------------------------------------------------------------------------+

SYNOPSIS

+-------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_STOP);   |
+-------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals AUDIO\_STOP for this command.                     |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_PLAY
-----------

DESCRIPTION

+----------------------------------------------------------------------------------------------------+
| This ioctl call asks the Audio Device to start playing an audio stream from the selected source.   |
+----------------------------------------------------------------------------------------------------+

SYNOPSIS

+-------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_PLAY);   |
+-------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals AUDIO\_PLAY for this command.                     |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_PAUSE
------------

DESCRIPTION

+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call suspends the audio stream being played. Decoding and playing are paused. It is then possible to restart again decoding and playing process of the audio stream using AUDIO\_CONTINUE command.   |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| If AUDIO\_SOURCE\_MEMORY is selected in the ioctl call AUDIO\_SELECT\_SOURCE, the DVB-subsystem will not decode (consume) any more data until the ioctl call AUDIO\_CONTINUE or AUDIO\_PLAY is performed.       |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+--------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_PAUSE);   |
+--------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals AUDIO\_PAUSE for this command.                    |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_CONTINUE
---------------

DESCRIPTION

+-----------------------------------------------------------------------------------------------------+
| This ioctl restarts the decoding and playing process previously paused with AUDIO\_PAUSE command.   |
+-----------------------------------------------------------------------------------------------------+
| It only works if the stream were previously stopped with AUDIO\_PAUSE                               |
+-----------------------------------------------------------------------------------------------------+

SYNOPSIS

+-----------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_CONTINUE);   |
+-----------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals AUDIO\_CONTINUE for this command.                 |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_SELECT\_SOURCE
---------------------

DESCRIPTION

+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call informs the audio device which source shall be used for the input data. The possible sources are demux or memory. If AUDIO\_SOURCE\_MEMORY is selected, the data is fed to the Audio Device through the write command.   |
+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+--------------------------------------------------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_SELECT\_SOURCE, audio\_stream\_source\_t source);   |
+--------------------------------------------------------------------------------------------+

PARAMETERS

+-----------------------------------+-----------------------------------------------------------------+
| int fd                            | File descriptor returned by a previous call to open().          |
+-----------------------------------+-----------------------------------------------------------------+
| int request                       | Equals AUDIO\_SELECT\_SOURCE for this command.                  |
+-----------------------------------+-----------------------------------------------------------------+
| audio\_stream\_source\_t source   | Indicates the source that shall be used for the Audio stream.   |
+-----------------------------------+-----------------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_SET\_MUTE
----------------

DESCRIPTION

This ioctl is for DVB devices only. To control a V4L2 decoder use the
V4L2 VIDIOC-DECODER-CMD with the ``V4L2_DEC_CMD_START_MUTE_AUDIO`` flag
instead.

+--------------------------------------------------------------------------------------------+
| This ioctl call asks the audio device to mute the stream that is currently being played.   |
+--------------------------------------------------------------------------------------------+

SYNOPSIS

+---------------------------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_SET\_MUTE, boolean state);   |
+---------------------------------------------------------------------+

PARAMETERS

+-----------------+----------------------------------------------------------+
| int fd          | File descriptor returned by a previous call to open().   |
+-----------------+----------------------------------------------------------+
| int request     | Equals AUDIO\_SET\_MUTE for this command.                |
+-----------------+----------------------------------------------------------+
| boolean state   | Indicates if audio device shall mute or not.             |
+-----------------+----------------------------------------------------------+
|                 | TRUE Audio Mute                                          |
+-----------------+----------------------------------------------------------+
|                 | FALSE Audio Un-mute                                      |
+-----------------+----------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_SET\_AV\_SYNC
--------------------

DESCRIPTION

+--------------------------------------------------------------------------------+
| This ioctl call asks the Audio Device to turn ON or OFF A/V synchronization.   |
+--------------------------------------------------------------------------------+

SYNOPSIS

+-------------------------------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_SET\_AV\_SYNC, boolean state);   |
+-------------------------------------------------------------------------+

PARAMETERS

+-----------------+----------------------------------------------------------------------+
| int fd          | File descriptor returned by a previous call to open().               |
+-----------------+----------------------------------------------------------------------+
| int request     | Equals AUDIO\_AV\_SYNC for this command.                             |
+-----------------+----------------------------------------------------------------------+
| boolean state   | Tells the DVB subsystem if A/V synchronization shall be ON or OFF.   |
+-----------------+----------------------------------------------------------------------+
|                 | TRUE AV-sync ON                                                      |
+-----------------+----------------------------------------------------------------------+
|                 | FALSE AV-sync OFF                                                    |
+-----------------+----------------------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_SET\_BYPASS\_MODE
------------------------

DESCRIPTION

+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call asks the Audio Device to bypass the Audio decoder and forward the stream without decoding. This mode shall be used if streams that can’t be handled by the DVB system shall be decoded. Dolby DigitalTM streams are automatically forwarded by the DVB subsystem if the hardware can handle it.   |
+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+----------------------------------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_SET\_BYPASS\_MODE, boolean mode);   |
+----------------------------------------------------------------------------+

PARAMETERS

+----------------+--------------------------------------------------------------------------------------+
| int fd         | File descriptor returned by a previous call to open().                               |
+----------------+--------------------------------------------------------------------------------------+
| int request    | Equals AUDIO\_SET\_BYPASS\_MODE for this command.                                    |
+----------------+--------------------------------------------------------------------------------------+
| boolean mode   | Enables or disables the decoding of the current Audio stream in the DVB subsystem.   |
+----------------+--------------------------------------------------------------------------------------+
|                | TRUE Bypass is disabled                                                              |
+----------------+--------------------------------------------------------------------------------------+
|                | FALSE Bypass is enabled                                                              |
+----------------+--------------------------------------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_CHANNEL\_SELECT
----------------------

DESCRIPTION

This ioctl is for DVB devices only. To control a V4L2 decoder use the
V4L2 ``V4L2_CID_MPEG_AUDIO_DEC_PLAYBACK`` control instead.

+--------------------------------------------------------------------------------------+
| This ioctl call asks the Audio Device to select the requested channel if possible.   |
+--------------------------------------------------------------------------------------+

SYNOPSIS

+---------------------------------------------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_CHANNEL\_SELECT, audio\_channel\_select\_t);   |
+---------------------------------------------------------------------------------------+

PARAMETERS

+--------------------------------+--------------------------------------------------------------------+
| int fd                         | File descriptor returned by a previous call to open().             |
+--------------------------------+--------------------------------------------------------------------+
| int request                    | Equals AUDIO\_CHANNEL\_SELECT for this command.                    |
+--------------------------------+--------------------------------------------------------------------+
| audio\_channel\_select\_t ch   | Select the output format of the audio (mono left/right, stereo).   |
+--------------------------------+--------------------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_BILINGUAL\_CHANNEL\_SELECT
---------------------------------

DESCRIPTION

This ioctl is obsolete. Do not use in new drivers. It has been replaced
by the V4L2 ``V4L2_CID_MPEG_AUDIO_DEC_MULTILINGUAL_PLAYBACK`` control
for MPEG decoders controlled through V4L2.

+------------------------------------------------------------------------------------------------------------+
| This ioctl call asks the Audio Device to select the requested channel for bilingual streams if possible.   |
+------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+--------------------------------------------------------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_BILINGUAL\_CHANNEL\_SELECT, audio\_channel\_select\_t);   |
+--------------------------------------------------------------------------------------------------+

PARAMETERS

+--------------------------------+--------------------------------------------------------------------+
| int fd                         | File descriptor returned by a previous call to open().             |
+--------------------------------+--------------------------------------------------------------------+
| int request                    | Equals AUDIO\_BILINGUAL\_CHANNEL\_SELECT for this command.         |
+--------------------------------+--------------------------------------------------------------------+
| audio\_channel\_select\_t ch   | Select the output format of the audio (mono left/right, stereo).   |
+--------------------------------+--------------------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_GET\_PTS
---------------

DESCRIPTION

This ioctl is obsolete. Do not use in new drivers. If you need this
functionality, then please contact the linux-media mailing list
(V4L-ML).

+------------------------------------------------------------------------------+
| This ioctl call asks the Audio Device to return the current PTS timestamp.   |
+------------------------------------------------------------------------------+

SYNOPSIS

+--------------------------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_GET\_PTS, \_\_u64 \*pts);   |
+--------------------------------------------------------------------+

PARAMETERS

+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| int fd          | File descriptor returned by a previous call to open().                                                                                                                                     |
+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| int request     | Equals AUDIO\_GET\_PTS for this command.                                                                                                                                                   |
+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u64 \*pts   | Returns the 33-bit timestamp as defined in ITU T-REC-H.222.0 / ISO/IEC 13818-1.                                                                                                            |
|                 |                                                                                                                                                                                            |
|                 | The PTS should belong to the currently played frame if possible, but may also be a value close to it like the PTS of the last decoded frame or the last PTS extracted by the PES parser.   |
+-----------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_GET\_STATUS
------------------

DESCRIPTION

+------------------------------------------------------------------------------------------+
| This ioctl call asks the Audio Device to return the current state of the Audio Device.   |
+------------------------------------------------------------------------------------------+

SYNOPSIS

+--------------------------------------------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_GET\_STATUS, struct audio\_status ⋆status);   |
+--------------------------------------------------------------------------------------+

PARAMETERS

+---------------------------------+----------------------------------------------------------+
| int fd                          | File descriptor returned by a previous call to open().   |
+---------------------------------+----------------------------------------------------------+
| int request                     | Equals AUDIO\_GET\_STATUS for this command.              |
+---------------------------------+----------------------------------------------------------+
| struct audio\_status \*status   | Returns the current state of Audio Device.               |
+---------------------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_GET\_CAPABILITIES
------------------------

DESCRIPTION

+-----------------------------------------------------------------------------------------------------------+
| This ioctl call asks the Audio Device to tell us about the decoding capabilities of the audio hardware.   |
+-----------------------------------------------------------------------------------------------------------+

SYNOPSIS

+---------------------------------------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_GET\_CAPABILITIES, unsigned int ⋆cap);   |
+---------------------------------------------------------------------------------+

PARAMETERS

+----------------------+----------------------------------------------------------+
| int fd               | File descriptor returned by a previous call to open().   |
+----------------------+----------------------------------------------------------+
| int request          | Equals AUDIO\_GET\_CAPABILITIES for this command.        |
+----------------------+----------------------------------------------------------+
| unsigned int \*cap   | Returns a bit array of supported sound formats.          |
+----------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_CLEAR\_BUFFER
--------------------

DESCRIPTION

+-----------------------------------------------------------------------------------------------------------------+
| This ioctl call asks the Audio Device to clear all software and hardware buffers of the audio decoder device.   |
+-----------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+----------------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_CLEAR\_BUFFER);   |
+----------------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals AUDIO\_CLEAR\_BUFFER for this command.            |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_SET\_ID
--------------

DESCRIPTION

+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl selects which sub-stream is to be decoded if a program or system stream is sent to the video device. If no audio stream type is set the id has to be in [0xC0,0xDF] for MPEG sound, in [0x80,0x87] for AC3 and in [0xA0,0xA7] for LPCM. More specifications may follow for other stream types. If the stream type is set the id just specifies the substream id of the audio stream and only the first 5 bits are recognized.   |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+------------------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_SET\_ID, int id);   |
+------------------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals AUDIO\_SET\_ID for this command.                  |
+---------------+----------------------------------------------------------+
| int id        | audio sub-stream id                                      |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_SET\_MIXER
-----------------

DESCRIPTION

+-----------------------------------------------------------------------+
| This ioctl lets you adjust the mixer settings of the audio decoder.   |
+-----------------------------------------------------------------------+

SYNOPSIS

+-----------------------------------------------------------------------------+
| int ioctl(int fd, int request = AUDIO\_SET\_MIXER, audio\_mixer\_t ⋆mix);   |
+-----------------------------------------------------------------------------+

PARAMETERS

+-------------------------+----------------------------------------------------------+
| int fd                  | File descriptor returned by a previous call to open().   |
+-------------------------+----------------------------------------------------------+
| int request             | Equals AUDIO\_SET\_ID for this command.                  |
+-------------------------+----------------------------------------------------------+
| audio\_mixer\_t \*mix   | mixer settings.                                          |
+-------------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
AUDIO\_SET\_STREAMTYPE
----------------------

DESCRIPTION

+------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl tells the driver which kind of audio stream to expect. This is useful if the stream offers several audio sub-streams like LPCM and AC3.   |
+------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+------------------------------------------------------------------+
| int ioctl(fd, int request = AUDIO\_SET\_STREAMTYPE, int type);   |
+------------------------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals AUDIO\_SET\_STREAMTYPE for this command.          |
+---------------+----------------------------------------------------------+
| int type      | stream type                                              |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
+----------+-------------------------------------------------+
| EINVAL   | type is not a valid or supported stream type.   |
+----------+-------------------------------------------------+

AUDIO\_SET\_EXT\_ID
-------------------

DESCRIPTION

+--------------------------------------------------------------------------------------------------------------------------+
| This ioctl can be used to set the extension id for MPEG streams in DVD playback. Only the first 3 bits are recognized.   |
+--------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+-------------------------------------------------------------+
| int ioctl(fd, int request = AUDIO\_SET\_EXT\_ID, int id);   |
+-------------------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals AUDIO\_SET\_EXT\_ID for this command.             |
+---------------+----------------------------------------------------------+
| int id        | audio sub\_stream\_id                                    |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
+----------+-------------------------+
| EINVAL   | id is not a valid id.   |
+----------+-------------------------+

AUDIO\_SET\_ATTRIBUTES
----------------------

DESCRIPTION

+-------------------------------------------------------------------------------------------------------------+
| This ioctl is intended for DVD playback and allows you to set certain information about the audio stream.   |
+-------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+------------------------------------------------------------------------------------+
| int ioctl(fd, int request = AUDIO\_SET\_ATTRIBUTES, audio\_attributes\_t attr );   |
+------------------------------------------------------------------------------------+

PARAMETERS

+-----------------------------+----------------------------------------------------------+
| int fd                      | File descriptor returned by a previous call to open().   |
+-----------------------------+----------------------------------------------------------+
| int request                 | Equals AUDIO\_SET\_ATTRIBUTES for this command.          |
+-----------------------------+----------------------------------------------------------+
| audio\_attributes\_t attr   | audio attributes according to section ??                 |
+-----------------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
+----------+-------------------------------------------------------+
| EINVAL   | attr is not a valid or supported attribute setting.   |
+----------+-------------------------------------------------------+

AUDIO\_SET\_KARAOKE
-------------------

DESCRIPTION

+----------------------------------------------------------------------+
| This ioctl allows one to set the mixer settings for a karaoke DVD.   |
+----------------------------------------------------------------------+

SYNOPSIS

+---------------------------------------------------------------------------------+
| int ioctl(fd, int request = AUDIO\_SET\_KARAOKE, audio\_karaoke\_t ⋆karaoke);   |
+---------------------------------------------------------------------------------+

PARAMETERS

+-------------------------------+----------------------------------------------------------+
| int fd                        | File descriptor returned by a previous call to open().   |
+-------------------------------+----------------------------------------------------------+
| int request                   | Equals AUDIO\_SET\_KARAOKE for this command.             |
+-------------------------------+----------------------------------------------------------+
| audio\_karaoke\_t \*karaoke   | karaoke settings according to section ??.                |
+-------------------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
+----------+--------------------------------------------------------+
| EINVAL   | karaoke is not a valid or supported karaoke setting.   |
+----------+--------------------------------------------------------+
