ioctl VIDIOC\_SUBDEV\_G\_FMT, VIDIOC\_SUBDEV\_S\_FMT
MANVOL
VIDIOC\_SUBDEV\_G\_FMT
VIDIOC\_SUBDEV\_S\_FMT
Get or set the data format on a subdev pad
int
ioctl
int
fd
int
request
struct v4l2\_subdev\_format \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_SUBDEV\_G\_FMT, VIDIOC\_SUBDEV\_S\_FMT

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

These ioctls are used to negotiate the frame format at specific subdev
pads in the image pipeline.

To retrieve the current format applications set the pad field of a
V4L2-SUBDEV-FORMAT to the desired pad number as reported by the media
API and the which field to ``V4L2_SUBDEV_FORMAT_ACTIVE``. When they call
the ``VIDIOC_SUBDEV_G_FMT`` ioctl with a pointer to this structure the
driver fills the members of the format field.

To change the current format applications set both the pad and which
fields and all members of the format field. When they call the
``VIDIOC_SUBDEV_S_FMT`` ioctl with a pointer to this structure the
driver verifies the requested format, adjusts it based on the hardware
capabilities and configures the device. Upon return the
V4L2-SUBDEV-FORMAT contains the current format as would be returned by a
``VIDIOC_SUBDEV_G_FMT`` call.

Applications can query the device capabilities by setting the which to
``V4L2_SUBDEV_FORMAT_TRY``. When set, 'try' formats are not applied to
the device by the driver, but are changed exactly as active formats and
stored in the sub-device file handle. Two applications querying the same
sub-device would thus not interact with each other.

For instance, to try a format at the output pad of a sub-device,
applications would first set the try format at the sub-device input with
the ``VIDIOC_SUBDEV_S_FMT`` ioctl. They would then either retrieve the
default format at the output pad with the ``VIDIOC_SUBDEV_G_FMT`` ioctl,
or set the desired output pad format with the ``VIDIOC_SUBDEV_S_FMT``
ioctl and check the returned value.

Try formats do not depend on active formats, but can depend on the
current links configuration or sub-device controls value. For instance,
a low-pass noise filter might crop pixels at the frame boundaries,
modifying its output frame size.

Drivers must not return an error solely because the requested format
doesn't match the device capabilities. They must instead modify the
format to match what the hardware can provide. The modified format
should be as close as possible to the original request.

+----------------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32              | pad           | Pad number as reported by the media controller API.                                    |
+----------------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32              | which         | Format to modified, from V4L2-SUBDEV-FORMAT-WHENCE.                                    |
+----------------------+---------------+----------------------------------------------------------------------------------------+
| V4L2-MBUS-FRAMEFMT   | format        | Definition of an image format, see ? for details.                                      |
+----------------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32              | reserved[8]   | Reserved for future extensions. Applications and drivers must set the array to zero.   |
+----------------------+---------------+----------------------------------------------------------------------------------------+

Table: struct v4l2\_subdev\_format

+--------------------------------+-----+-------------------------------------------------------+
| V4L2\_SUBDEV\_FORMAT\_TRY      | 0   | Try formats, used for querying device capabilities.   |
+--------------------------------+-----+-------------------------------------------------------+
| V4L2\_SUBDEV\_FORMAT\_ACTIVE   | 1   | Active formats, applied to the hardware.              |
+--------------------------------+-----+-------------------------------------------------------+

Table: enum v4l2\_subdev\_format\_whence

RETURN-VALUE

EBUSY
    The format can't be changed because the pad is currently busy. This
    can be caused, for instance, by an active video stream on the pad.
    The ioctl must not be retried without performing another action to
    fix the problem first. Only returned by ``VIDIOC_SUBDEV_S_FMT``

EINVAL
    The V4L2-SUBDEV-FORMAT pad references a non-existing pad, or the
    which field references a non-existing format.

RETURN-VALUE
