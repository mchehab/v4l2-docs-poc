The DVB demux device controls the filters of the DVB hardware/software.
It can be accessed through ``/dev/adapter?/demux?``. Data types and and
ioctl definitions can be accessed by including ``linux/dvb/dmx.h`` in
your application.

Demux Data Types
================

Output for the demux
--------------------

+--------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ID                       | Description                                                                                                                                                                                                                                                   |
+==========================+===============================================================================================================================================================================================================================================================+
| DMX\_OUT\_DECODER        | Streaming directly to decoder.                                                                                                                                                                                                                                |
+--------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| DMX\_OUT\_TAP            | Output going to a memory buffer (to be retrieved via the read command). Delivers the stream output to the demux device on which the ioctl is called.                                                                                                          |
+--------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| DMX\_OUT\_TS\_TAP        | Output multiplexed into a new TS (to be retrieved by reading from the logical DVR device). Routes output to the logical DVR device ``/dev/dvb/adapter?/dvr?``, which delivers a TS multiplexed from all filters for which ``DMX_OUT_TS_TAP`` was specified.   |
+--------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| DMX\_OUT\_TSDEMUX\_TAP   | Like DMX-OUT-TS-TAP but retrieved from the DMX device.                                                                                                                                                                                                        |
+--------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: enum dmx\_output

dmx\_input\_t
-------------

::

    typedef enum
    {
        DMX_IN_FRONTEND, /⋆ Input from a front-end device.  ⋆/
        DMX_IN_DVR       /⋆ Input from the logical DVR device.  ⋆/
    } dmx_input_t;

dmx\_pes\_type\_t
-----------------

::

    typedef enum
    {
        DMX_PES_AUDIO0,
        DMX_PES_VIDEO0,
        DMX_PES_TELETEXT0,
        DMX_PES_SUBTITLE0,
        DMX_PES_PCR0,

        DMX_PES_AUDIO1,
        DMX_PES_VIDEO1,
        DMX_PES_TELETEXT1,
        DMX_PES_SUBTITLE1,
        DMX_PES_PCR1,

        DMX_PES_AUDIO2,
        DMX_PES_VIDEO2,
        DMX_PES_TELETEXT2,
        DMX_PES_SUBTITLE2,
        DMX_PES_PCR2,

        DMX_PES_AUDIO3,
        DMX_PES_VIDEO3,
        DMX_PES_TELETEXT3,
        DMX_PES_SUBTITLE3,
        DMX_PES_PCR3,

        DMX_PES_OTHER
    } dmx_pes_type_t;

struct dmx\_filter
------------------

::

     typedef struct dmx_filter
    {
        __u8  filter[DMX_FILTER_SIZE];
        __u8  mask[DMX_FILTER_SIZE];
        __u8  mode[DMX_FILTER_SIZE];
    } dmx_filter_t;

struct dmx\_sct\_filter\_params
-------------------------------

::

    struct dmx_sct_filter_params
    {
        __u16          pid;
        dmx_filter_t   filter;
        __u32          timeout;
        __u32          flags;
    #define DMX_CHECK_CRC       1
    #define DMX_ONESHOT         2
    #define DMX_IMMEDIATE_START 4
    #define DMX_KERNEL_CLIENT   0x8000
    };

struct dmx\_pes\_filter\_params
-------------------------------

::

    struct dmx_pes_filter_params
    {
        __u16          pid;
        dmx_input_t    input;
        dmx_output_t   output;
        dmx_pes_type_t pes_type;
        __u32          flags;
    };

struct dmx\_event
-----------------

::

     struct dmx_event
     {
         dmx_event_t          event;
         time_t               timeStamp;
         union
         {
             dmx_scrambling_status_t scrambling;
         } u;
     };

struct dmx\_stc
---------------

::

    struct dmx_stc {
        unsigned int num;   /⋆ input : which STC? 0..N ⋆/
        unsigned int base;  /⋆ output: divisor for stc to get 90 kHz clock ⋆/
        __u64 stc;      /⋆ output: stc in 'base'⋆90 kHz units ⋆/
    };

struct dmx\_caps
----------------

::

     typedef struct dmx_caps {
        __u32 caps;
        int num_decoders;
    } dmx_caps_t;

enum dmx\_source\_t
-------------------

::

    typedef enum {
        DMX_SOURCE_FRONT0 = 0,
        DMX_SOURCE_FRONT1,
        DMX_SOURCE_FRONT2,
        DMX_SOURCE_FRONT3,
        DMX_SOURCE_DVR0   = 16,
        DMX_SOURCE_DVR1,
        DMX_SOURCE_DVR2,
        DMX_SOURCE_DVR3
    } dmx_source_t;

Demux Function Calls
====================

open()
------

DESCRIPTION

+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This system call, used with a device name of /dev/dvb/adapter0/demux0, allocates a new filter and returns a handle which can be used for subsequent control of that filter. This call has to be made for each filter to be used, i.e. every returned file descriptor is a reference to a single filter. /dev/dvb/adapter0/dvr0 is a logical device to be used for retrieving Transport Streams for digital video recording. When reading from this device a transport stream containing the packets from all PES filters set in the corresponding demux device (/dev/dvb/adapter0/demux0) having the output set to DMX\_OUT\_TS\_TAP. A recorded Transport Stream is replayed by writing to this device.   |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| The significance of blocking or non-blocking mode is described in the documentation for functions where there is a difference. It does not affect the semantics of the open() call itself. A device opened in blocking mode can later be put into non-blocking mode (and vice versa) using the F\_SETFL command of the fcntl system call.                                                                                                                                                                                                                                                                                                                                                                  |
+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+------------------------------------------------+
| int open(const char ⋆deviceName, int flags);   |
+------------------------------------------------+

PARAMETERS

+---------------------------+-----------------------------------------+
| const char \*deviceName   | Name of demux device.                   |
+---------------------------+-----------------------------------------+
| int flags                 | A bit-wise OR of the following flags:   |
+---------------------------+-----------------------------------------+
|                           | O\_RDWR read/write access               |
+---------------------------+-----------------------------------------+
|                           | O\_NONBLOCK open in non-blocking mode   |
+---------------------------+-----------------------------------------+
|                           | (blocking mode is the default)          |
+---------------------------+-----------------------------------------+

RETURN VALUE

+----------+----------------------------------------------------------+
| ENODEV   | Device driver not loaded/available.                      |
+----------+----------------------------------------------------------+
| EINVAL   | Invalid argument.                                        |
+----------+----------------------------------------------------------+
| EMFILE   | “Too many open files”, i.e. no more filters available.   |
+----------+----------------------------------------------------------+
| ENOMEM   | The driver failed to allocate enough memory.             |
+----------+----------------------------------------------------------+

close()
-------

DESCRIPTION

+------------------------------------------------------------------------------------------------------------+
| This system call deactivates and deallocates a filter that was previously allocated via the open() call.   |
+------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+----------------------+
| int close(int fd);   |
+----------------------+

PARAMETERS

+----------+----------------------------------------------------------+
| int fd   | File descriptor returned by a previous call to open().   |
+----------+----------------------------------------------------------+

RETURN VALUE

+---------+-------------------------------------------+
| EBADF   | fd is not a valid open file descriptor.   |
+---------+-------------------------------------------+

read()
------

DESCRIPTION

+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This system call returns filtered data, which might be section or PES data. The filtered data is transferred from the driver’s internal circular buffer to buf. The maximum amount of data to be transferred is implied by count.                                                                                                                                                                                                                                                                                                                                               |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| When returning section data the driver always tries to return a complete single section (even though buf would provide buffer space for more data). If the size of the buffer is smaller than the section as much as possible will be returned, and the remaining data will be provided in subsequent calls.                                                                                                                                                                                                                                                                    |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| The size of the internal buffer is 2 \* 4096 bytes (the size of two maximum sized sections) by default. The size of this buffer may be changed by using the DMX\_SET\_BUFFER\_SIZE function. If the buffer is not large enough, or if the read operations are not performed fast enough, this may result in a buffer overflow error. In this case EOVERFLOW will be returned, and the circular buffer will be emptied. This call is blocking if there is no data to return, i.e. the process will be put to sleep waiting for data, unless the O\_NONBLOCK flag is specified.   |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Note that in order to be able to read, the filtering process has to be started by defining either a section or a PES filter by means of the ioctl functions, and then starting the filtering process via the DMX\_START ioctl function or by setting the DMX\_IMMEDIATE\_START flag. If the reading is done from a logical DVR demux device, the data will constitute a Transport Stream including the packets from all PES filters in the corresponding demux device /dev/dvb/adapter0/demux0 having the output set to DMX\_OUT\_TS\_TAP.                                      |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+---------------------------------------------------+
| size\_t read(int fd, void ⋆buf, size\_t count);   |
+---------------------------------------------------+

PARAMETERS

+-----------------+----------------------------------------------------------------+
| int fd          | File descriptor returned by a previous call to open().         |
+-----------------+----------------------------------------------------------------+
| void \*buf      | Pointer to the buffer to be used for returned filtered data.   |
+-----------------+----------------------------------------------------------------+
| size\_t count   | Size of buf.                                                   |
+-----------------+----------------------------------------------------------------+

RETURN VALUE

+---------------+-----------------------------------------------------------------------------------------------------------------------------+
| EWOULDBLOCK   | No data to return and O\_NONBLOCK was specified.                                                                            |
+---------------+-----------------------------------------------------------------------------------------------------------------------------+
| EBADF         | fd is not a valid open file descriptor.                                                                                     |
+---------------+-----------------------------------------------------------------------------------------------------------------------------+
| ECRC          | Last section had a CRC error - no data returned. The buffer is flushed.                                                     |
+---------------+-----------------------------------------------------------------------------------------------------------------------------+
| EOVERFLOW     |                                                                                                                             |
+---------------+-----------------------------------------------------------------------------------------------------------------------------+
|               | The filtered data was not read from the buffer in due time, resulting in non-read data being lost. The buffer is flushed.   |
+---------------+-----------------------------------------------------------------------------------------------------------------------------+
| ETIMEDOUT     | The section was not loaded within the stated timeout period. See ioctl DMX\_SET\_FILTER for how to set a timeout.           |
+---------------+-----------------------------------------------------------------------------------------------------------------------------+
| EFAULT        | The driver failed to write to the callers buffer due to an invalid \*buf pointer.                                           |
+---------------+-----------------------------------------------------------------------------------------------------------------------------+

write()
-------

DESCRIPTION

+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This system call is only provided by the logical device /dev/dvb/adapter0/dvr0, associated with the physical demux device that provides the actual DVR functionality. It is used for replay of a digitally recorded Transport Stream. Matching filters have to be defined in the corresponding physical demux device, /dev/dvb/adapter0/demux0. The amount of data to be transferred is implied by count.   |
+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+-----------------------------------------------------------+
| ssize\_t write(int fd, const void ⋆buf, size\_t count);   |
+-----------------------------------------------------------+

PARAMETERS

+-----------------+----------------------------------------------------------+
| int fd          | File descriptor returned by a previous call to open().   |
+-----------------+----------------------------------------------------------+
| void \*buf      | Pointer to the buffer containing the Transport Stream.   |
+-----------------+----------------------------------------------------------+
| size\_t count   | Size of buf.                                             |
+-----------------+----------------------------------------------------------+

RETURN VALUE

+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| EWOULDBLOCK   | No data was written. This might happen if O\_NONBLOCK was specified and there is no more buffer space available (if O\_NONBLOCK is not specified the function will block until buffer space is available).                                              |
+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| EBUSY         | This error code indicates that there are conflicting requests. The corresponding demux device is setup to receive data from the front- end. Make sure that these filters are stopped and that the filters with input set to DMX\_IN\_DVR are started.   |
+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| EBADF         | fd is not a valid open file descriptor.                                                                                                                                                                                                                 |
+---------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

DMX\_START
----------

DESCRIPTION

+------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call is used to start the actual filtering operation defined via the ioctl calls DMX\_SET\_FILTER or DMX\_SET\_PES\_FILTER.   |
+------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+-------------------------------------------------+
| int ioctl( int fd, int request = DMX\_START);   |
+-------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals DMX\_START for this command.                      |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
+----------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| EINVAL   | Invalid argument, i.e. no filtering parameters provided via the DMX\_SET\_FILTER or DMX\_SET\_PES\_FILTER functions.                                                                                      |
+----------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| EBUSY    | This error code indicates that there are conflicting requests. There are active filters filtering data from another input source. Make sure that these filters are stopped before starting this filter.   |
+----------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

DMX\_STOP
---------

DESCRIPTION

+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call is used to stop the actual filtering operation defined via the ioctl calls DMX\_SET\_FILTER or DMX\_SET\_PES\_FILTER and started via the DMX\_START command.   |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+------------------------------------------------+
| int ioctl( int fd, int request = DMX\_STOP);   |
+------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals DMX\_STOP for this command.                       |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
DMX\_SET\_FILTER
----------------

DESCRIPTION

+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call sets up a filter according to the filter and mask parameters provided. A timeout may be defined stating number of seconds to wait for a section to be loaded. A value of 0 means that no timeout should be applied. Finally there is a flag field where it is possible to state whether a section should be CRC-checked, whether the filter should be a ”one-shot” filter, i.e. if the filtering operation should be stopped after the first section is received, and whether the filtering operation should be started immediately (without waiting for a DMX\_START ioctl call). If a filter was previously set-up, this filter will be canceled, and the receive buffer will be flushed.   |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+------------------------------------------------------------------------------------------------+
| int ioctl( int fd, int request = DMX\_SET\_FILTER, struct dmx\_sct\_filter\_params ⋆params);   |
+------------------------------------------------------------------------------------------------+

PARAMETERS

+--------------------------------------------+----------------------------------------------------------+
| int fd                                     | File descriptor returned by a previous call to open().   |
+--------------------------------------------+----------------------------------------------------------+
| int request                                | Equals DMX\_SET\_FILTER for this command.                |
+--------------------------------------------+----------------------------------------------------------+
| struct dmx\_sct\_filter\_params \*params   | Pointer to structure containing filter parameters.       |
+--------------------------------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
DMX\_SET\_PES\_FILTER
---------------------

DESCRIPTION

+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call sets up a PES filter according to the parameters provided. By a PES filter is meant a filter that is based just on the packet identifier (PID), i.e. no PES header or payload filtering capability is supported.                                                                                                                                                                                                                                           |
+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| The transport stream destination for the filtered output may be set. Also the PES type may be stated in order to be able to e.g. direct a video stream directly to the video decoder. Finally there is a flag field where it is possible to state whether the filtering operation should be started immediately (without waiting for a DMX\_START ioctl call). If a filter was previously set-up, this filter will be cancelled, and the receive buffer will be flushed.   |
+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+-----------------------------------------------------------------------------------------------------+
| int ioctl( int fd, int request = DMX\_SET\_PES\_FILTER, struct dmx\_pes\_filter\_params ⋆params);   |
+-----------------------------------------------------------------------------------------------------+

PARAMETERS

+--------------------------------------------+----------------------------------------------------------+
| int fd                                     | File descriptor returned by a previous call to open().   |
+--------------------------------------------+----------------------------------------------------------+
| int request                                | Equals DMX\_SET\_PES\_FILTER for this command.           |
+--------------------------------------------+----------------------------------------------------------+
| struct dmx\_pes\_filter\_params \*params   | Pointer to structure containing filter parameters.       |
+--------------------------------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
+---------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| EBUSY   | This error code indicates that there are conflicting requests. There are active filters filtering data from another input source. Make sure that these filters are stopped before starting this filter.   |
+---------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

DMX\_SET\_BUFFER\_SIZE
----------------------

DESCRIPTION

+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call is used to set the size of the circular buffer used for filtered data. The default size is two maximum sized sections, i.e. if this function is not called a buffer size of 2 \* 4096 bytes will be used.   |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+---------------------------------------------------------------------------------+
| int ioctl( int fd, int request = DMX\_SET\_BUFFER\_SIZE, unsigned long size);   |
+---------------------------------------------------------------------------------+

PARAMETERS

+----------------------+----------------------------------------------------------+
| int fd               | File descriptor returned by a previous call to open().   |
+----------------------+----------------------------------------------------------+
| int request          | Equals DMX\_SET\_BUFFER\_SIZE for this command.          |
+----------------------+----------------------------------------------------------+
| unsigned long size   | Size of circular buffer.                                 |
+----------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
DMX\_GET\_EVENT
---------------

DESCRIPTION

+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call returns an event if available. If an event is not available, the behavior depends on whether the device is in blocking or non-blocking mode. In the latter case, the call fails immediately with errno set to EWOULDBLOCK. In the former case, the call blocks until an event becomes available.                    |
+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| The standard Linux poll() and/or select() system calls can be used with the device file descriptor to watch for new events. For select(), the file descriptor should be included in the exceptfds argument, and for poll(), POLLPRI should be specified as the wake-up condition. Only the latest event for each filter is saved.   |
+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+-----------------------------------------------------------------------------+
| int ioctl( int fd, int request = DMX\_GET\_EVENT, struct dmx\_event ⋆ev);   |
+-----------------------------------------------------------------------------+

PARAMETERS

+--------------------------+------------------------------------------------------------+
| int fd                   | File descriptor returned by a previous call to open().     |
+--------------------------+------------------------------------------------------------+
| int request              | Equals DMX\_GET\_EVENT for this command.                   |
+--------------------------+------------------------------------------------------------+
| struct dmx\_event \*ev   | Pointer to the location where the event is to be stored.   |
+--------------------------+------------------------------------------------------------+

RETURN-VALUE-DVB
+---------------+----------------------------------------------------------------------+
| EWOULDBLOCK   | There is no event pending, and the device is in non-blocking mode.   |
+---------------+----------------------------------------------------------------------+

DMX\_GET\_STC
-------------

DESCRIPTION

+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call returns the current value of the system time counter (which is driven by a PES filter of type DMX\_PES\_PCR). Some hardware supports more than one STC, so you must specify which one by setting the num field of stc before the ioctl (range 0...n). The result is returned in form of a ratio with a 64 bit numerator and a 32 bit denominator, so the real 90kHz STC value is stc->stc / stc->base .   |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+--------------------------------------------------------------------------+
| int ioctl( int fd, int request = DMX\_GET\_STC, struct dmx\_stc ⋆stc);   |
+--------------------------------------------------------------------------+

PARAMETERS

+-------------------------+----------------------------------------------------------+
| int fd                  | File descriptor returned by a previous call to open().   |
+-------------------------+----------------------------------------------------------+
| int request             | Equals DMX\_GET\_STC for this command.                   |
+-------------------------+----------------------------------------------------------+
| struct dmx\_stc \*stc   | Pointer to the location where the stc is to be stored.   |
+-------------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
+----------+-----------------------+
| EINVAL   | Invalid stc number.   |
+----------+-----------------------+

DMX\_GET\_PES\_PIDS
-------------------

DESCRIPTION

+---------------------------------------------------------+
| This ioctl is undocumented. Documentation is welcome.   |
+---------------------------------------------------------+

SYNOPSIS

+-----------------------------------------------------------------+
| int ioctl(fd, int request = DMX\_GET\_PES\_PIDS, \_\_u16[5]);   |
+-----------------------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals DMX\_GET\_PES\_PIDS for this command.             |
+---------------+----------------------------------------------------------+
| \_\_u16[5]    | Undocumented.                                            |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
DMX\_GET\_CAPS
--------------

DESCRIPTION

+---------------------------------------------------------+
| This ioctl is undocumented. Documentation is welcome.   |
+---------------------------------------------------------+

SYNOPSIS

+-----------------------------------------------------------------+
| int ioctl(fd, int request = DMX\_GET\_CAPS, dmx\_caps\_t \*);   |
+-----------------------------------------------------------------+

PARAMETERS

+-------------------+----------------------------------------------------------+
| int fd            | File descriptor returned by a previous call to open().   |
+-------------------+----------------------------------------------------------+
| int request       | Equals DMX\_GET\_CAPS for this command.                  |
+-------------------+----------------------------------------------------------+
| dmx\_caps\_t \*   | Undocumented.                                            |
+-------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
DMX\_SET\_SOURCE
----------------

DESCRIPTION

+---------------------------------------------------------+
| This ioctl is undocumented. Documentation is welcome.   |
+---------------------------------------------------------+

SYNOPSIS

+---------------------------------------------------------------------+
| int ioctl(fd, int request = DMX\_SET\_SOURCE, dmx\_source\_t \*);   |
+---------------------------------------------------------------------+

PARAMETERS

+---------------------+----------------------------------------------------------+
| int fd              | File descriptor returned by a previous call to open().   |
+---------------------+----------------------------------------------------------+
| int request         | Equals DMX\_SET\_SOURCE for this command.                |
+---------------------+----------------------------------------------------------+
| dmx\_source\_t \*   | Undocumented.                                            |
+---------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
DMX\_ADD\_PID
-------------

DESCRIPTION

+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call allows to add multiple PIDs to a transport stream filter previously set up with DMX\_SET\_PES\_FILTER and output equal to DMX\_OUT\_TSDEMUX\_TAP.                                                              |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| It is used by readers of /dev/dvb/adapterX/demuxY.                                                                                                                                                                             |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| It may be called at any time, i.e. before or after the first filter on the shared file descriptor was started. It makes it possible to record multiple services without the need to de-multiplex or re-multiplex TS packets.   |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+-----------------------------------------------------------+
| int ioctl(fd, int request = DMX\_ADD\_PID, \_\_u16 \*);   |
+-----------------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals DMX\_ADD\_PID for this command.                   |
+---------------+----------------------------------------------------------+
| \_\_u16 \*    | PID number to be filtered.                               |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
DMX\_REMOVE\_PID
----------------

DESCRIPTION

+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This ioctl call allows to remove a PID when multiple PIDs are set on a transport stream filter, e. g. a filter previously set up with output equal to DMX\_OUT\_TSDEMUX\_TAP, created via either DMX\_SET\_PES\_FILTER or DMX\_ADD\_PID.   |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| It is used by readers of /dev/dvb/adapterX/demuxY.                                                                                                                                                                                         |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| It may be called at any time, i.e. before or after the first filter on the shared file descriptor was started. It makes it possible to record multiple services without the need to de-multiplex or re-multiplex TS packets.               |
+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+--------------------------------------------------------------+
| int ioctl(fd, int request = DMX\_REMOVE\_PID, \_\_u16 \*);   |
+--------------------------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals DMX\_REMOVE\_PID for this command.                |
+---------------+----------------------------------------------------------+
| \_\_u16 \*    | PID of the PES filter to be removed.                     |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
