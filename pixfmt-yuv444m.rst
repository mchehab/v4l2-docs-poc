V4L2\_PIX\_FMT\_YUV444M ('YM24'), V4L2\_PIX\_FMT\_YVU444M ('YM42')
MANVOL
V4L2\_PIX\_FMT\_YUV444M
V4L2\_PIX\_FMT\_YVU444M
Planar formats with full horizontal resolution, also known as YUV and
YVU 4:4:4
Description
===========

This is a multi-planar format, as opposed to a packed format. The three
components are separated into three sub-images or planes.

The Y plane is first. The Y plane has one byte per pixel. For
``V4L2_PIX_FMT_YUV444M`` the Cb data constitutes the second plane which
is the same width and height as the Y plane (and as the image). The Cr
data, just like the Cb plane, is in the third plane.

``V4L2_PIX_FMT_YVU444M`` is the same except the Cr data is stored in the
second plane and the Cb data in the third plane.

If the Y plane has pad bytes after each row, then the Cb and Cr planes
have the same number of pad bytes after their rows.

``V4L2_PIX_FMT_YUV444M`` and ``V4L2_PIX_FMT_YUV444M`` are intended to be
used only in drivers and applications that support the multi-planar API,
described in ?.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start0 + 0:                                                              |
+--------------------------------------------------------------------------+
| start0 + 4:                                                              |
+--------------------------------------------------------------------------+
| start0 + 8:                                                              |
+--------------------------------------------------------------------------+
| start0 + 12:                                                             |
+--------------------------------------------------------------------------+
+--------------------------------------------------------------------------+
| start1 + 0:                                                              |
+--------------------------------------------------------------------------+
| start1 + 4:                                                              |
+--------------------------------------------------------------------------+
| start1 + 8:                                                              |
+--------------------------------------------------------------------------+
| start1 + 12:                                                             |
+--------------------------------------------------------------------------+
+--------------------------------------------------------------------------+
| start2 + 0:                                                              |
+--------------------------------------------------------------------------+
| start2 + 4:                                                              |
+--------------------------------------------------------------------------+
| start2 + 8:                                                              |
+--------------------------------------------------------------------------+
| start2 + 12:                                                             |
+--------------------------------------------------------------------------+

**Color Sample Location..**

+-----+------+----+------+----+------+----+------+
|     | 0    |    | 1    |    | 2    |    | 3    |
+-----+------+----+------+----+------+----+------+
| 0   | YC   |    | YC   |    | YC   |    | YC   |
+-----+------+----+------+----+------+----+------+
| 1   | YC   |    | YC   |    | YC   |    | YC   |
+-----+------+----+------+----+------+----+------+
| 2   | YC   |    | YC   |    | YC   |    | YC   |
+-----+------+----+------+----+------+----+------+
| 3   | YC   |    | YC   |    | YC   |    | YC   |
+-----+------+----+------+----+------+----+------+
