ioctl VIDIOC\_G\_AUDIO, VIDIOC\_S\_AUDIO
MANVOL
VIDIOC\_G\_AUDIO
VIDIOC\_S\_AUDIO
Query or select the current audio input and its attributes
int
ioctl
int
fd
int
request
struct v4l2\_audio \*
argp
int
ioctl
int
fd
int
request
const struct v4l2\_audio \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_AUDIO, VIDIOC\_S\_AUDIO

``argp``

Description
===========

To query the current audio input applications zero out the reserved
array of a V4L2-AUDIO and call the ``VIDIOC_G_AUDIO`` ioctl with a
pointer to this structure. Drivers fill the rest of the structure or
return an EINVAL when the device has no audio inputs, or none which
combine with the current video input.

Audio inputs have one writable property, the audio mode. To select the
current audio input *and* change the audio mode, applications initialize
the index and mode fields, and the reserved array of a v4l2\_audio
structure and call the ``VIDIOC_S_AUDIO`` ioctl. Drivers may switch to a
different audio mode if the request cannot be satisfied. However, this
is a write-only ioctl, it does not return the actual new audio mode.

+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | index         | Identifies the audio input, set by the driver or application.                                                                                                                     |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u8    | name[32]      | Name of the audio input, a NUL-terminated ASCII string, for example: "Line In". This information is intended for the user, preferably the connector label on the device itself.   |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | capability    | Audio capability flags, see ?.                                                                                                                                                    |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | mode          | Audio mode flags set by drivers and applications (on ``VIDIOC_S_AUDIO`` ioctl), see ?.                                                                                            |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | reserved[2]   | Reserved for future extensions. Drivers and applications must set the array to zero.                                                                                              |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_audio

+--------------------------+-----------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_AUDCAP_STEREO``   | 0x00001   | This is a stereo input. The flag is intended to automatically disable stereo recording etc. when the signal is always monaural. The API provides no means to detect if stereo is *received*, unless the audio input belongs to a tuner.   |
+--------------------------+-----------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_AUDCAP_AVL``      | 0x00002   | Automatic Volume Level mode is supported.                                                                                                                                                                                                 |
+--------------------------+-----------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: Audio Capability Flags

+------------------------+-----------+-------------------+
| ``V4L2_AUDMODE_AVL``   | 0x00001   | AVL mode is on.   |
+------------------------+-----------+-------------------+

Table: Audio Mode Flags

RETURN-VALUE

EINVAL
    No audio inputs combine with the current video input, or the number
    of the selected audio input is out of bounds or it does not combine.
