ioctl FE\_SET\_TONE
MANVOL
FE\_SET\_TONE
Sets/resets the generation of the continuous 22kHz tone.
int
ioctl
int
fd
int
request
enum fe\_sec\_tone\_mode \*
tone
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_SET\_TONE

``tone``
    pointer to FE-SEC-TONE-MODE

Description
===========

This ioctl is used to set the generation of the continuous 22kHz tone.
This call requires read/write permissions.

Usually, satellite antenna subsystems require that the digital TV device
to send a 22kHz tone in order to select between high/low band on some
dual-band LNBf. It is also used to send signals to DiSEqC equipment, but
this is done using the DiSEqC ioctls.

NOTE: if more than one device is connected to the same antenna, setting
a tone may interfere on other devices, as they may lose the capability
of selecting the band. So, it is recommended that applications would
change to SEC\_TONE\_OFF when the device is not used.

RETURN-VALUE-DVB
enum fe\_sec\_tone\_mode
========================

+--------------------+-------------------------------------------------------------------------------------------+
| ID                 | Description                                                                               |
+====================+===========================================================================================+
| ``SEC_TONE_ON``    | Sends a 22kHz tone burst to the antenna                                                   |
+--------------------+-------------------------------------------------------------------------------------------+
| ``SEC_TONE_OFF``   | Don't send a 22kHz tone to the antenna (except if the FE\_DISEQC\_\* ioctls are called)   |
+--------------------+-------------------------------------------------------------------------------------------+

Table: enum fe\_sec\_tone\_mode
