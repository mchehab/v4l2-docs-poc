ioctl VIDIOC\_G\_OUTPUT, VIDIOC\_S\_OUTPUT
MANVOL
VIDIOC\_G\_OUTPUT
VIDIOC\_S\_OUTPUT
Query or select the current video output
int
ioctl
int
fd
int
request
int \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_OUTPUT, VIDIOC\_S\_OUTPUT

``argp``

Description
===========

To query the current video output applications call the
``VIDIOC_G_OUTPUT`` ioctl with a pointer to an integer where the driver
stores the number of the output, as in the V4L2-OUTPUT index field. This
ioctl will fail only when there are no video outputs, returning the
EINVAL.

To select a video output applications store the number of the desired
output in an integer and call the ``VIDIOC_S_OUTPUT`` ioctl with a
pointer to this integer. Side effects are possible. For example outputs
may support different video standards, so the driver may implicitly
switch the current standard. standard. Because of these possible side
effects applications must select an output before querying or
negotiating any other parameters.

Information about video outputs is available using the VIDIOC-ENUMOUTPUT
ioctl.

RETURN-VALUE

EINVAL
    The number of the video output is out of bounds, or there are no
    video outputs at all.
