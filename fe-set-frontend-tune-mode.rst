ioctl FE\_SET\_FRONTEND\_TUNE\_MODE
MANVOL
FE\_SET\_FRONTEND\_TUNE\_MODE
Allow setting tuner mode flags to the frontend.
int
ioctl
int
fd
int
request
unsigned int
flags
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_SET\_FRONTEND\_TUNE\_MODE

``flags``
    Valid flags:

    -  0 - normal tune mode

    -  FE\_TUNE\_MODE\_ONESHOT - When set, this flag will disable any
       zigzagging or other "normal" tuning behaviour. Additionally,
       there will be no automatic monitoring of the lock status, and
       hence no frontend events will be generated. If a frontend device
       is closed, this flag will be automatically turned off when the
       device is reopened read-write.

Description
===========

Allow setting tuner mode flags to the frontend, between 0 (normal) or
FE\_TUNE\_MODE\_ONESHOT mode

RETURN-VALUE-DVB
