media ioctl()
MANVOL
media-ioctl
Control a media device
#include <sys/ioctl.h>
int
ioctl
int
fd
int
request
void \*
argp
Arguments
=========

``fd``
    FD

``request``
    Media ioctl request code as defined in the media.h header file, for
    example MEDIA\_IOC\_SETUP\_LINK.

``argp``
    Pointer to a request-specific structure.

Description
===========

The ``ioctl()`` function manipulates media device parameters. The
argument ``fd`` must be an open file descriptor.

The ioctl ``request`` code specifies the media function to be called. It
has encoded in it whether the argument is an input, output or read/write
parameter, and the size of the argument ``argp`` in bytes.

Macros and structures definitions specifying media ioctl requests and
their parameters are located in the media.h header file. All media ioctl
requests, their respective function and parameters are specified in ?.

RETURN-VALUE
Request-specific error codes are listed in the individual requests
descriptions.

When an ioctl that takes an output or read/write parameter fails, the
parameter remains unmodified.
