Video capture devices sample an analog video signal and store the
digitized images in memory. Today nearly all devices can capture at full
25 or 30 frames/second. With this interface applications can control the
capture process and move images from the driver into user space.

Conventionally V4L2 video capture devices are accessed through character
device special files named ``/dev/video`` and ``/dev/video0`` to
``/dev/video63`` with major number 81 and minor numbers 0 to 63.
``/dev/video`` is typically a symbolic link to the preferred video
device. Note the same device files are used for video output devices.

Querying Capabilities
=====================

Devices supporting the video capture interface set the
``V4L2_CAP_VIDEO_CAPTURE`` or ``V4L2_CAP_VIDEO_CAPTURE_MPLANE`` flag in
the capabilities field of V4L2-CAPABILITY returned by the
VIDIOC-QUERYCAP ioctl. As secondary device functions they may also
support the `video overlay <#overlay>`__ (``V4L2_CAP_VIDEO_OVERLAY``)
and the `raw VBI capture <#raw-vbi>`__ (``V4L2_CAP_VBI_CAPTURE``)
interface. At least one of the read/write or streaming I/O methods must
be supported. Tuners and audio inputs are optional.

Supplemental Functions
======================

Video capture devices shall support `audio input <#audio>`__,
`tuner <#tuner>`__, `controls <#control>`__, `cropping and
scaling <#crop>`__ and `streaming parameter <#streaming-par>`__ ioctls
as needed. The `video input <#video>`__ and `video
standard <#standard>`__ ioctls must be supported by all video capture
devices.

Image Format Negotiation
========================

The result of a capture operation is determined by cropping and image
format parameters. The former select an area of the video picture to
capture, the latter how images are stored in memory, IE in RGB or YUV
format, the number of bits per pixel or width and height. Together they
also define how images are scaled in the process.

As usual these parameters are *not* reset at FUNC-OPEN time to permit
Unix tool chains, programming a device and then reading from it as if it
was a plain file. Well written V4L2 applications ensure they really get
what they want, including cropping and scaling.

Cropping initialization at minimum requires to reset the parameters to
defaults. An example is given in ?.

To query the current image format applications set the type field of a
V4L2-FORMAT to ``V4L2_BUF_TYPE_VIDEO_CAPTURE`` or
``V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE`` and call the VIDIOC-G-FMT ioctl
with a pointer to this structure. Drivers fill the V4L2-PIX-FORMAT pix
or the V4L2-PIX-FORMAT-MPLANE pix\_mp member of the fmt union.

To request different parameters applications set the type field of a
V4L2-FORMAT as above and initialize all fields of the V4L2-PIX-FORMAT
vbi member of the fmt union, or better just modify the results of
``VIDIOC_G_FMT``, and call the VIDIOC-S-FMT ioctl with a pointer to this
structure. Drivers may adjust the parameters and finally return the
actual parameters as ``VIDIOC_G_FMT`` does.

Like ``VIDIOC_S_FMT`` the VIDIOC-TRY-FMT ioctl can be used to learn
about hardware limitations without disabling I/O or possibly time
consuming hardware preparations.

The contents of V4L2-PIX-FORMAT and V4L2-PIX-FORMAT-MPLANE are discussed
in ?. See also the specification of the ``VIDIOC_G_FMT``,
``VIDIOC_S_FMT`` and ``VIDIOC_TRY_FMT`` ioctls for details. Video
capture devices must implement both the ``VIDIOC_G_FMT`` and
``VIDIOC_S_FMT`` ioctl, even if ``VIDIOC_S_FMT`` ignores all requests
and always returns default parameters as ``VIDIOC_G_FMT`` does.
``VIDIOC_TRY_FMT`` is optional.

Reading Images
==============

A video capture device may support the `read() function <#rw>`__ and/or
streaming (`memory mapping <#mmap>`__ or `user pointer <#userp>`__) I/O.
See ? for details.
