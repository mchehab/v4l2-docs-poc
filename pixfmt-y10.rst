V4L2\_PIX\_FMT\_Y10 ('Y10 ')
MANVOL
V4L2\_PIX\_FMT\_Y10
Grey-scale image
Description
===========

This is a grey-scale image with a depth of 10 bits per pixel. Pixels are
stored in 16-bit words with unused high bits padded with 0. The least
significant byte is stored at lower memory addresses (little-endian).

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 8:                                                               |
+--------------------------------------------------------------------------+
| start + 16:                                                              |
+--------------------------------------------------------------------------+
| start + 24:                                                              |
+--------------------------------------------------------------------------+
