V4L2 munmap()
MANVOL
v4l2-munmap
Unmap device memory
#include <unistd.h> #include <sys/mman.h>
int
munmap
void \*
start
size\_t
length
Arguments
=========

``start``
    Address of the mapped buffer as returned by the FUNC-MMAP function.

``length``
    Length of the mapped buffer. This must be the same value as given to
    ``mmap()`` and returned by the driver in the V4L2-BUFFER length
    field for the single-planar API and in the V4L2-PLANE length field
    for the multi-planar API.

Description
===========

Unmaps a previously with the FUNC-MMAP function mapped buffer and frees
it, if possible.

Return Value
============

On success ``munmap()`` returns 0, on failure -1 and the ``errno``
variable is set appropriately:

EINVAL
    The ``start`` or ``length`` is incorrect, or no buffers have been
    mapped yet.
