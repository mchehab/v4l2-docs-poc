ioctl MEDIA\_IOC\_SETUP\_LINK
MANVOL
MEDIA\_IOC\_SETUP\_LINK
Modify the properties of a link
int
ioctl
int
fd
int
request
struct media\_link\_desc \*
argp
Arguments
=========

``fd``
    File descriptor returned by ```open()`` <#media-func-open>`__.

``request``
    MEDIA\_IOC\_SETUP\_LINK

``argp``

Description
===========

To change link properties applications fill a MEDIA-LINK-DESC with link
identification information (source and sink pad) and the new requested
link flags. They then call the MEDIA\_IOC\_SETUP\_LINK ioctl with a
pointer to that structure.

The only configurable property is the ``ENABLED`` link flag to
enable/disable a link. Links marked with the ``IMMUTABLE`` link flag can
not be enabled or disabled.

Link configuration has no side effect on other links. If an enabled link
at the sink pad prevents the link from being enabled, the driver returns
with an EBUSY.

Only links marked with the ``DYNAMIC`` link flag can be enabled/disabled
while streaming media data. Attempting to enable or disable a streaming
non-dynamic link will return an EBUSY.

If the specified link can't be found the driver returns with an EINVAL.

RETURN-VALUE

EINVAL
    The MEDIA-LINK-DESC references a non-existing link, or the link is
    immutable and an attempt to modify its configuration was made.
