V4L2 ioctl()
MANVOL
v4l2-ioctl
Program a V4L2 device
#include <sys/ioctl.h>
int
ioctl
int
fd
int
request
void \*
argp
Arguments
=========

``fd``
    FD

``request``
    V4L2 ioctl request code as defined in the ``videodev2.h`` header
    file, for example VIDIOC\_QUERYCAP.

``argp``
    Pointer to a function parameter, usually a structure.

Description
===========

The ``ioctl()`` function is used to program V4L2 devices. The argument
``fd`` must be an open file descriptor. An ioctl ``request`` has encoded
in it whether the argument is an input, output or read/write parameter,
and the size of the argument ``argp`` in bytes. Macros and defines
specifying V4L2 ioctl requests are located in the ``videodev2.h`` header
file. Applications should use their own copy, not include the version in
the kernel sources on the system they compile on. All V4L2 ioctl
requests, their respective function and parameters are specified in ?.

RETURN-VALUE
When an ioctl that takes an output or read/write parameter fails, the
parameter remains unmodified.
