ioctl VIDIOC\_OVERLAY
MANVOL
VIDIOC\_OVERLAY
Start or stop video overlay
int
ioctl
int
fd
int
request
const int \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_OVERLAY

``argp``

Description
===========

This ioctl is part of the `video overlay <#overlay>`__ I/O method.
Applications call ``VIDIOC_OVERLAY`` to start or stop the overlay. It
takes a pointer to an integer which must be set to zero by the
application to stop overlay, to one to start.

Drivers do not support VIDIOC-STREAMON or VIDIOC-STREAMOFF with
``V4L2_BUF_TYPE_VIDEO_OVERLAY``.

RETURN-VALUE

EINVAL
    The overlay parameters have not been set up. See ? for the necessary
    steps.
