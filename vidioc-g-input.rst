ioctl VIDIOC\_G\_INPUT, VIDIOC\_S\_INPUT
MANVOL
VIDIOC\_G\_INPUT
VIDIOC\_S\_INPUT
Query or select the current video input
int
ioctl
int
fd
int
request
int \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_INPUT, VIDIOC\_S\_INPUT

``argp``

Description
===========

To query the current video input applications call the
``VIDIOC_G_INPUT`` ioctl with a pointer to an integer where the driver
stores the number of the input, as in the V4L2-INPUT index field. This
ioctl will fail only when there are no video inputs, returning EINVAL.

To select a video input applications store the number of the desired
input in an integer and call the ``VIDIOC_S_INPUT`` ioctl with a pointer
to this integer. Side effects are possible. For example inputs may
support different video standards, so the driver may implicitly switch
the current standard. Because of these possible side effects
applications must select an input before querying or negotiating any
other parameters.

Information about video inputs is available using the VIDIOC-ENUMINPUT
ioctl.

RETURN-VALUE

EINVAL
    The number of the video input is out of bounds.
