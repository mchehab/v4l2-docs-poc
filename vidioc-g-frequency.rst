ioctl VIDIOC\_G\_FREQUENCY, VIDIOC\_S\_FREQUENCY
MANVOL
VIDIOC\_G\_FREQUENCY
VIDIOC\_S\_FREQUENCY
Get or set tuner or modulator radio frequency
int
ioctl
int
fd
int
request
struct v4l2\_frequency \*
argp
int
ioctl
int
fd
int
request
const struct v4l2\_frequency \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_FREQUENCY, VIDIOC\_S\_FREQUENCY

``argp``

Description
===========

To get the current tuner or modulator radio frequency applications set
the tuner field of a V4L2-FREQUENCY to the respective tuner or modulator
number (only input devices have tuners, only output devices have
modulators), zero out the reserved array and call the
``VIDIOC_G_FREQUENCY`` ioctl with a pointer to this structure. The
driver stores the current frequency in the frequency field.

To change the current tuner or modulator radio frequency applications
initialize the tuner, type and frequency fields, and the reserved array
of a V4L2-FREQUENCY and call the ``VIDIOC_S_FREQUENCY`` ioctl with a
pointer to this structure. When the requested frequency is not possible
the driver assumes the closest possible value. However
``VIDIOC_S_FREQUENCY`` is a write-only ioctl, it does not return the
actual new frequency.

+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | tuner         | The tuner or modulator index number. This is the same value as in the V4L2-INPUT tuner field and the V4L2-TUNER index field, or the V4L2-OUTPUT modulator field and the V4L2-MODULATOR index field.                                                                                                                   |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | type          | The tuner type. This is the same value as in the V4L2-TUNER type field. The type must be set to ``V4L2_TUNER_RADIO`` for ``/dev/radioX`` device nodes, and to ``V4L2_TUNER_ANALOG_TV`` for all others. Set this field to ``V4L2_TUNER_RADIO`` for modulators (currently only radio modulators are supported). See ?   |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | frequency     | Tuning frequency in units of 62.5 kHz, or if the V4L2-TUNER or V4L2-MODULATOR capability flag ``V4L2_TUNER_CAP_LOW`` is set, in units of 62.5 Hz. A 1 Hz unit is used when the capability flag ``V4L2_TUNER_CAP_1HZ`` is set.                                                                                         |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | reserved[8]   | Reserved for future extensions. Drivers and applications must set the array to zero.                                                                                                                                                                                                                                  |
+-----------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_frequency

RETURN-VALUE

EINVAL
    The tuner index is out of bounds or the value in the type field is
    wrong.

EBUSY
    A hardware seek is in progress.
