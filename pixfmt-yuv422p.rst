V4L2\_PIX\_FMT\_YUV422P ('422P')
MANVOL
V4L2\_PIX\_FMT\_YUV422P
Format with ½ horizontal chroma resolution, also known as YUV 4:2:2.
Planar layout as opposed to
V4L2\_PIX\_FMT\_YUYV
Description
===========

This format is not commonly used. This is a planar version of the YUYV
format. The three components are separated into three sub-images or
planes. The Y plane is first. The Y plane has one byte per pixel. The Cb
plane immediately follows the Y plane in memory. The Cb plane is half
the width of the Y plane (and of the image). Each Cb belongs to two
pixels. For example, Cb\ :sub:`0` belongs to Y'\ :sub:`00`,
Y'\ :sub:`01`. Following the Cb plane is the Cr plane, just like the Cb
plane.

If the Y plane has pad bytes after each row, then the Cr and Cb planes
have half as many pad bytes after their rows. In other words, two Cx
rows (including padding) is exactly as long as one Y row (including
padding).

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 4:                                                               |
+--------------------------------------------------------------------------+
| start + 8:                                                               |
+--------------------------------------------------------------------------+
| start + 12:                                                              |
+--------------------------------------------------------------------------+
| start + 16:                                                              |
+--------------------------------------------------------------------------+
| start + 18:                                                              |
+--------------------------------------------------------------------------+
| start + 20:                                                              |
+--------------------------------------------------------------------------+
| start + 22:                                                              |
+--------------------------------------------------------------------------+
| start + 24:                                                              |
+--------------------------------------------------------------------------+
| start + 26:                                                              |
+--------------------------------------------------------------------------+
| start + 28:                                                              |
+--------------------------------------------------------------------------+
| start + 30:                                                              |
+--------------------------------------------------------------------------+

**Color Sample Location..**

+-----+-----+-----+-----+----+-----+-----+-----+
|     | 0   |     | 1   |    | 2   |     | 3   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 0   | Y   | C   | Y   |    | Y   | C   | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 1   | Y   | C   | Y   |    | Y   | C   | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 2   | Y   | C   | Y   |    | Y   | C   | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 3   | Y   | C   | Y   |    | Y   | C   | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
