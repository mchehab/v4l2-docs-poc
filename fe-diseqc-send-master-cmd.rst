ioctl FE\_DISEQC\_SEND\_MASTER\_CMD
MANVOL
FE\_DISEQC\_SEND\_MASTER\_CMD
Sends a DiSEqC command
int
ioctl
int
fd
int
request
struct dvb\_diseqc\_master\_cmd \*
argp
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_DISEQC\_SEND\_MASTER\_CMD

``argp``
    pointer to DVB-DISEQC-MASTER-CMD

Description
===========

Sends a DiSEqC command to the antenna subsystem.

RETURN-VALUE-DVB
+------------+------------+---------------------------------------------------------+
| uint8\_t   | msg[6]     | DiSEqC message (framing, address, command, data[3])     |
+------------+------------+---------------------------------------------------------+
| uint8\_t   | msg\_len   | Length of the DiSEqC message. Valid values are 3 to 6   |
+------------+------------+---------------------------------------------------------+

Table: struct dvb\_diseqc\_master\_cmd
