ioctl MEDIA\_IOC\_ENUM\_LINKS
MANVOL
MEDIA\_IOC\_ENUM\_LINKS
Enumerate all pads and links for a given entity
int
ioctl
int
fd
int
request
struct media\_links\_enum \*
argp
Arguments
=========

``fd``
    File descriptor returned by ```open()`` <#media-func-open>`__.

``request``
    MEDIA\_IOC\_ENUM\_LINKS

``argp``

Description
===========

To enumerate pads and/or links for a given entity, applications set the
entity field of a MEDIA-LINKS-ENUM structure and initialize the
MEDIA-PAD-DESC and MEDIA-LINK-DESC structure arrays pointed by the pads
and links fields. They then call the MEDIA\_IOC\_ENUM\_LINKS ioctl with
a pointer to this structure.

If the pads field is not NULL, the driver fills the pads array with
information about the entity's pads. The array must have enough room to
store all the entity's pads. The number of pads can be retrieved with
the MEDIA-IOC-ENUM-ENTITIES ioctl.

If the links field is not NULL, the driver fills the links array with
information about the entity's outbound links. The array must have
enough room to store all the entity's outbound links. The number of
outbound links can be retrieved with the MEDIA-IOC-ENUM-ENTITIES ioctl.

Only forward links that originate at one of the entity's source pads are
returned during the enumeration process.

+-------------------+-----------+---------------------------------------------------------------------------+
| \_\_u32           | entity    | Entity id, set by the application.                                        |
+-------------------+-----------+---------------------------------------------------------------------------+
| MEDIA-PAD-DESC    | \*pads    | Pointer to a pads array allocated by the application. Ignored if NULL.    |
+-------------------+-----------+---------------------------------------------------------------------------+
| MEDIA-LINK-DESC   | \*links   | Pointer to a links array allocated by the application. Ignored if NULL.   |
+-------------------+-----------+---------------------------------------------------------------------------+

Table: struct media\_links\_enum

+-----------+----------+-----------------------------------------+
| \_\_u32   | entity   | ID of the entity this pad belongs to.   |
+-----------+----------+-----------------------------------------+
| \_\_u16   | index    | 0-based pad index.                      |
+-----------+----------+-----------------------------------------+
| \_\_u32   | flags    | Pad flags, see ? for more details.      |
+-----------+----------+-----------------------------------------+

Table: struct media\_pad\_desc

+------------------+----------+---------------------------------------+
| MEDIA-PAD-DESC   | source   | Pad at the origin of this link.       |
+------------------+----------+---------------------------------------+
| MEDIA-PAD-DESC   | sink     | Pad at the target of this link.       |
+------------------+----------+---------------------------------------+
| \_\_u32          | flags    | Link flags, see ? for more details.   |
+------------------+----------+---------------------------------------+

Table: struct media\_link\_desc

RETURN-VALUE

EINVAL
    The MEDIA-LINKS-ENUM id references a non-existing entity.
