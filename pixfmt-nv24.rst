V4L2\_PIX\_FMT\_NV24 ('NV24'), V4L2\_PIX\_FMT\_NV42 ('NV42')
MANVOL
V4L2\_PIX\_FMT\_NV24
V4L2\_PIX\_FMT\_NV42
Formats with full horizontal and vertical chroma resolutions, also known
as YUV 4:4:4. One luminance and one chrominance plane with alternating
chroma samples as opposed to
V4L2\_PIX\_FMT\_YVU420
Description
===========

These are two-plane versions of the YUV 4:4:4 format. The three
components are separated into two sub-images or planes. The Y plane is
first, with each Y sample stored in one byte per pixel. For
``V4L2_PIX_FMT_NV24``, a combined CbCr plane immediately follows the Y
plane in memory. The CbCr plane has the same width and height, in
pixels, as the Y plane (and the image). Each line contains one CbCr pair
per pixel, with each Cb and Cr sample stored in one byte.
``V4L2_PIX_FMT_NV42`` is the same except that the Cb and Cr samples are
swapped, the CrCb plane starts with a Cr sample.

If the Y plane has pad bytes after each row, then the CbCr plane has
twice as many pad bytes after its rows.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 4:                                                               |
+--------------------------------------------------------------------------+
| start + 8:                                                               |
+--------------------------------------------------------------------------+
| start + 12:                                                              |
+--------------------------------------------------------------------------+
| start + 16:                                                              |
+--------------------------------------------------------------------------+
| start + 24:                                                              |
+--------------------------------------------------------------------------+
| start + 32:                                                              |
+--------------------------------------------------------------------------+
| start + 40:                                                              |
+--------------------------------------------------------------------------+
