V4L2\_PIX\_FMT\_SBGGR16 ('BYR2')
MANVOL
V4L2\_PIX\_FMT\_SBGGR16
Bayer RGB format
Description
===========

This format is similar to
```V4L2_PIX_FMT_SBGGR8`` <#V4L2-PIX-FMT-SBGGR8>`__, except each pixel
has a depth of 16 bits. The least significant byte is stored at lower
memory addresses (little-endian). Note the actual sampling precision may
be lower than 16 bits, for example 10 bits per pixel with values in
range 0 to 1023.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 8:                                                               |
+--------------------------------------------------------------------------+
| start + 16:                                                              |
+--------------------------------------------------------------------------+
| start + 24:                                                              |
+--------------------------------------------------------------------------+
