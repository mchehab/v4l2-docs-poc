ioctl VIDIOC\_ENUM\_DV\_TIMINGS, VIDIOC\_SUBDEV\_ENUM\_DV\_TIMINGS
MANVOL
VIDIOC\_ENUM\_DV\_TIMINGS
VIDIOC\_SUBDEV\_ENUM\_DV\_TIMINGS
Enumerate supported Digital Video timings
int
ioctl
int
fd
int
request
struct v4l2\_enum\_dv\_timings \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_ENUM\_DV\_TIMINGS, VIDIOC\_SUBDEV\_ENUM\_DV\_TIMINGS

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

While some DV receivers or transmitters support a wide range of timings,
others support only a limited number of timings. With this ioctl
applications can enumerate a list of known supported timings. Call
VIDIOC-DV-TIMINGS-CAP to check if it also supports other standards or
even custom timings that are not in this list.

To query the available timings, applications initialize the index field
and zero the reserved array of V4L2-ENUM-DV-TIMINGS and call the
``VIDIOC_ENUM_DV_TIMINGS`` ioctl on a video node with a pointer to this
structure. Drivers fill the rest of the structure or return an EINVAL
when the index is out of bounds. To enumerate all supported DV timings,
applications shall begin at index zero, incrementing by one until the
driver returns EINVAL. Note that drivers may enumerate a different set
of DV timings after switching the video input or output.

When implemented by the driver DV timings of subdevices can be queried
by calling the ``VIDIOC_SUBDEV_ENUM_DV_TIMINGS`` ioctl directly on a
subdevice node. The DV timings are specific to inputs (for DV receivers)
or outputs (for DV transmitters), applications must specify the desired
pad number in the V4L2-ENUM-DV-TIMINGS pad field. Attempts to enumerate
timings on a pad that doesn't support them will return an EINVAL.

+-------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32           | index         | Number of the DV timings, set by the application.                                                                                                                                          |
+-------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32           | pad           | Pad number as reported by the media controller API. This field is only used when operating on a subdevice node. When operating on a video node applications must set this field to zero.   |
+-------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32           | reserved[2]   | Reserved for future extensions. Drivers and applications must set the array to zero.                                                                                                       |
+-------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| V4L2-DV-TIMINGS   | timings       | The timings.                                                                                                                                                                               |
+-------------------+---------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_enum\_dv\_timings

RETURN-VALUE

EINVAL
    The V4L2-ENUM-DV-TIMINGS index is out of bounds or the pad number is
    invalid.

ENODATA
    Digital video presets are not supported for this input or output.
