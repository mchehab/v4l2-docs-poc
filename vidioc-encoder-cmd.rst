ioctl VIDIOC\_ENCODER\_CMD, VIDIOC\_TRY\_ENCODER\_CMD
MANVOL
VIDIOC\_ENCODER\_CMD
VIDIOC\_TRY\_ENCODER\_CMD
Execute an encoder command
int
ioctl
int
fd
int
request
struct v4l2\_encoder\_cmd \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_ENCODER\_CMD, VIDIOC\_TRY\_ENCODER\_CMD

``argp``

Description
===========

These ioctls control an audio/video (usually MPEG-) encoder.
``VIDIOC_ENCODER_CMD`` sends a command to the encoder,
``VIDIOC_TRY_ENCODER_CMD`` can be used to try a command without actually
executing it.

To send a command applications must initialize all fields of a
V4L2-ENCODER-CMD and call ``VIDIOC_ENCODER_CMD`` or
``VIDIOC_TRY_ENCODER_CMD`` with a pointer to this structure.

The cmd field must contain the command code. The flags field is
currently only used by the STOP command and contains one bit: If the
``V4L2_ENC_CMD_STOP_AT_GOP_END`` flag is set, encoding will continue
until the end of the current *Group Of Pictures*, otherwise it will stop
immediately.

A ``read``\ () or VIDIOC-STREAMON call sends an implicit START command
to the encoder if it has not been started yet. After a STOP command,
``read``\ () calls will read the remaining data buffered by the driver.
When the buffer is empty, ``read``\ () will return zero and the next
``read``\ () call will restart the encoder.

A ``close``\ () or VIDIOC-STREAMOFF call of a streaming file descriptor
sends an implicit immediate STOP to the encoder, and all buffered data
is discarded.

These ioctls are optional, not all drivers may support them. They were
introduced in Linux 2.6.21.

+-----------+-----------+----------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | cmd       | The encoder command, see ?.                                                                                                            |
+-----------+-----------+----------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | flags     | Flags to go with the command, see ?. If no flags are defined for this command, drivers and applications must set this field to zero.   |
+-----------+-----------+----------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | data[8]   | Reserved for future extensions. Drivers and applications must set the array to zero.                                                   |
+-----------+-----------+----------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_encoder\_cmd

+---------------------------+-----+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_ENC_CMD_START``    | 0   | Start the encoder. When the encoder is already running or paused, this command does nothing. No flags are defined for this command.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
+---------------------------+-----+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_ENC_CMD_STOP``     | 1   | Stop the encoder. When the ``V4L2_ENC_CMD_STOP_AT_GOP_END`` flag is set, encoding will continue until the end of the current *Group Of Pictures*, otherwise encoding will stop immediately. When the encoder is already stopped, this command does nothing. mem2mem encoders will send a ``V4L2_EVENT_EOS`` event when the last frame has been encoded and all frames are ready to be dequeued and will set the ``V4L2_BUF_FLAG_LAST`` buffer flag on the last buffer of the capture queue to indicate there will be no new buffers produced to dequeue. This buffer may be empty, indicated by the driver setting the bytesused field to 0. Once the ``V4L2_BUF_FLAG_LAST`` flag was set, the `VIDIOC\_DQBUF <#vidioc-qbuf>`__ ioctl will not block anymore, but return an EPIPE.   |
+---------------------------+-----+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_ENC_CMD_PAUSE``    | 2   | Pause the encoder. When the encoder has not been started yet, the driver will return an EPERM. When the encoder is already paused, this command does nothing. No flags are defined for this command.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
+---------------------------+-----+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_ENC_CMD_RESUME``   | 3   | Resume encoding after a PAUSE command. When the encoder has not been started yet, the driver will return an EPERM. When the encoder is already running, this command does nothing. No flags are defined for this command.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
+---------------------------+-----+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: Encoder Commands

+------------------------------------+----------+-----------------------------------------------------------------------------------------+
| ``V4L2_ENC_CMD_STOP_AT_GOP_END``   | 0x0001   | Stop encoding at the end of the current *Group Of Pictures*, rather than immediately.   |
+------------------------------------+----------+-----------------------------------------------------------------------------------------+

Table: Encoder Command Flags

RETURN-VALUE

EINVAL
    The cmd field is invalid.

EPERM
    The application sent a PAUSE or RESUME command when the encoder was
    not running.
