ioctl VIDIOC\_S\_HW\_FREQ\_SEEK
MANVOL
VIDIOC\_S\_HW\_FREQ\_SEEK
Perform a hardware frequency seek
int
ioctl
int
fd
int
request
struct v4l2\_hw\_freq\_seek \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_S\_HW\_FREQ\_SEEK

``argp``

Description
===========

Start a hardware frequency seek from the current frequency. To do this
applications initialize the tuner, type, seek\_upward, wrap\_around,
spacing, rangelow and rangehigh fields, and zero out the reserved array
of a V4L2-HW-FREQ-SEEK and call the ``VIDIOC_S_HW_FREQ_SEEK`` ioctl with
a pointer to this structure.

The rangelow and rangehigh fields can be set to a non-zero value to tell
the driver to search a specific band. If the V4L2-TUNER capability field
has the ``V4L2_TUNER_CAP_HWSEEK_PROG_LIM`` flag set, these values must
fall within one of the bands returned by VIDIOC-ENUM-FREQ-BANDS. If the
``V4L2_TUNER_CAP_HWSEEK_PROG_LIM`` flag is not set, then these values
must exactly match those of one of the bands returned by
VIDIOC-ENUM-FREQ-BANDS. If the current frequency of the tuner does not
fall within the selected band it will be clamped to fit in the band
before the seek is started.

If an error is returned, then the original frequency will be restored.

This ioctl is supported if the ``V4L2_CAP_HW_FREQ_SEEK`` capability is
set.

If this ioctl is called from a non-blocking filehandle, then EAGAIN is
returned and no seek takes place.

+-----------+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | tuner          | The tuner index number. This is the same value as in the V4L2-INPUT tuner field and the V4L2-TUNER index field.                                                                                                                                                                                                                                       |
+-----------+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | type           | The tuner type. This is the same value as in the V4L2-TUNER type field. See ?                                                                                                                                                                                                                                                                         |
+-----------+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | seek\_upward   | If non-zero, seek upward from the current frequency, else seek downward.                                                                                                                                                                                                                                                                              |
+-----------+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | wrap\_around   | If non-zero, wrap around when at the end of the frequency range, else stop seeking. The V4L2-TUNER capability field will tell you what the hardware supports.                                                                                                                                                                                         |
+-----------+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | spacing        | If non-zero, defines the hardware seek resolution in Hz. The driver selects the nearest value that is supported by the device. If spacing is zero a reasonable default value is used.                                                                                                                                                                 |
+-----------+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | rangelow       | If non-zero, the lowest tunable frequency of the band to search in units of 62.5 kHz, or if the V4L2-TUNER capability field has the ``V4L2_TUNER_CAP_LOW`` flag set, in units of 62.5 Hz or if the V4L2-TUNER capability field has the ``V4L2_TUNER_CAP_1HZ`` flag set, in units of 1 Hz. If rangelow is zero a reasonable default value is used.     |
+-----------+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | rangehigh      | If non-zero, the highest tunable frequency of the band to search in units of 62.5 kHz, or if the V4L2-TUNER capability field has the ``V4L2_TUNER_CAP_LOW`` flag set, in units of 62.5 Hz or if the V4L2-TUNER capability field has the ``V4L2_TUNER_CAP_1HZ`` flag set, in units of 1 Hz. If rangehigh is zero a reasonable default value is used.   |
+-----------+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | reserved[5]    | Reserved for future extensions. Applications must set the array to zero.                                                                                                                                                                                                                                                                              |
+-----------+----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_hw\_freq\_seek

RETURN-VALUE

EINVAL
    The tuner index is out of bounds, the wrap\_around value is not
    supported or one of the values in the type, rangelow or rangehigh
    fields is wrong.

EAGAIN
    Attempted to call ``VIDIOC_S_HW_FREQ_SEEK`` with the filehandle in
    non-blocking mode.

ENODATA
    The hardware seek found no channels.

EBUSY
    Another hardware seek is already in progress.
