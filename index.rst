Contents:Introduction
=====================

This document covers the Linux Kernel to Userspace API's used by video
and radio streaming devices, including video cameras, analog and digital
TV receiver cards, AM/FM receiver cards, streaming capture and output
devices, codec devices and remote controllers.

A typical media device hardware is shown at the picture below:

image:: typical_media_device.svg

The media infrastructure API was designed to control such devices. It is
divided into four parts.

The first part covers radio, video capture and output, cameras, analog
TV devices and codecs.

The second part covers the API used for digital TV and Internet
reception via one of the several digital tv standards. While it is
called as DVB API, in fact it covers several different video standards
including DVB-T/T2, DVB-S/S2, DVB-C, ATSC, ISDB-T, ISDB-S,etc. The
complete list of supported standards can be found at ?.

The third part covers the Remote Controller API.

The fourth part covers the Media Controller API.

It should also be noted that a media device may also have audio
components, like mixers, PCM capture, PCM playback, etc, which are
controlled via ALSA API.

For additional information and for the latest development code, see:
https://linuxtv.org.

For discussing improvements, reporting troubles, sending new drivers,
etc, please mail to: `Linux Media Mailing List
(LMML). <http://vger.kernel.org/vger-lists.html#linux-media>`__.


.. toctree::
   :maxdepth: 2

   v4l2
   remote_controllers
   dvbapi
   media-controller
   gen-errors
   fdl-appendix

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

