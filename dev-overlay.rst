Also known as Framebuffer Overlay or Previewing
Video overlay devices have the ability to genlock (TV-)video into the
(VGA-)video signal of a graphics card, or to store captured images
directly in video memory of a graphics card, typically with clipping.
This can be considerable more efficient than capturing images and
displaying them by other means. In the old days when only nuclear power
plants needed cooling towers this used to be the only way to put live
video into a window.

Video overlay devices are accessed through the same character special
files as `video capture <#capture>`__ devices. Note the default function
of a ``/dev/video`` device is video capturing. The overlay function is
only available after calling the VIDIOC-S-FMT ioctl.

The driver may support simultaneous overlay and capturing using the
read/write and streaming I/O methods. If so, operation at the nominal
frame rate of the video standard is not guaranteed. Frames may be
directed away from overlay to capture, or one field may be used for
overlay and the other for capture if the capture parameters permit this.

Applications should use different file descriptors for capturing and
overlay. This must be supported by all drivers capable of simultaneous
capturing and overlay. Optionally these drivers may also permit
capturing and overlay with a single file descriptor for compatibility
with V4L and earlier versions of V4L2. [1]_

Querying Capabilities
=====================

Devices supporting the video overlay interface set the
``V4L2_CAP_VIDEO_OVERLAY`` flag in the capabilities field of
V4L2-CAPABILITY returned by the VIDIOC-QUERYCAP ioctl. The overlay I/O
method specified below must be supported. Tuners and audio inputs are
optional.

Supplemental Functions
======================

Video overlay devices shall support `audio input <#audio>`__,
`tuner <#tuner>`__, `controls <#control>`__, `cropping and
scaling <#crop>`__ and `streaming parameter <#streaming-par>`__ ioctls
as needed. The `video input <#video>`__ and `video
standard <#standard>`__ ioctls must be supported by all video overlay
devices.

Setup
=====

Before overlay can commence applications must program the driver with
frame buffer parameters, namely the address and size of the frame buffer
and the image format, for example RGB 5:6:5. The VIDIOC-G-FBUF and
VIDIOC-S-FBUF ioctls are available to get and set these parameters,
respectively. The ``VIDIOC_S_FBUF`` ioctl is privileged because it
allows to set up DMA into physical memory, bypassing the memory
protection mechanisms of the kernel. Only the superuser can change the
frame buffer address and size. Users are not supposed to run TV
applications as root or with SUID bit set. A small helper application
with suitable privileges should query the graphics system and program
the V4L2 driver at the appropriate time.

Some devices add the video overlay to the output signal of the graphics
card. In this case the frame buffer is not modified by the video device,
and the frame buffer address and pixel format are not needed by the
driver. The ``VIDIOC_S_FBUF`` ioctl is not privileged. An application
can check for this type of device by calling the ``VIDIOC_G_FBUF``
ioctl.

A driver may support any (or none) of five clipping/blending methods:

1. Chroma-keying displays the overlaid image only where pixels in the
   primary graphics surface assume a certain color.

2. A bitmap can be specified where each bit corresponds to a pixel in
   the overlaid image. When the bit is set, the corresponding video
   pixel is displayed, otherwise a pixel of the graphics surface.

3. A list of clipping rectangles can be specified. In these regions *no*
   video is displayed, so the graphics surface can be seen here.

4. The framebuffer has an alpha channel that can be used to clip or
   blend the framebuffer with the video.

5. A global alpha value can be specified to blend the framebuffer
   contents with video images.

When simultaneous capturing and overlay is supported and the hardware
prohibits different image and frame buffer formats, the format requested
first takes precedence. The attempt to capture (VIDIOC-S-FMT) or overlay
(VIDIOC-S-FBUF) may fail with an EBUSY or return accordingly modified
parameters..

Overlay Window
==============

The overlaid image is determined by cropping and overlay window
parameters. The former select an area of the video picture to capture,
the latter how images are overlaid and clipped. Cropping initialization
at minimum requires to reset the parameters to defaults. An example is
given in ?.

The overlay window is described by a V4L2-WINDOW. It defines the size of
the image, its position over the graphics surface and the clipping to be
applied. To get the current parameters applications set the type field
of a V4L2-FORMAT to ``V4L2_BUF_TYPE_VIDEO_OVERLAY`` and call the
VIDIOC-G-FMT ioctl. The driver fills the v4l2\_window substructure named
win. It is not possible to retrieve a previously programmed clipping
list or bitmap.

To program the overlay window applications set the type field of a
V4L2-FORMAT to ``V4L2_BUF_TYPE_VIDEO_OVERLAY``, initialize the win
substructure and call the VIDIOC-S-FMT ioctl. The driver adjusts the
parameters against hardware limits and returns the actual parameters as
``VIDIOC_G_FMT`` does. Like ``VIDIOC_S_FMT``, the VIDIOC-TRY-FMT ioctl
can be used to learn about driver capabilities without actually changing
driver state. Unlike ``VIDIOC_S_FMT`` this also works after the overlay
has been enabled.

The scaling factor of the overlaid image is implied by the width and
height given in V4L2-WINDOW and the size of the cropping rectangle. For
more information see ?.

When simultaneous capturing and overlay is supported and the hardware
prohibits different image and window sizes, the size requested first
takes precedence. The attempt to capture or overlay as well
(VIDIOC-S-FMT) may fail with an EBUSY or return accordingly modified
parameters.

+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| V4L2-RECT                                                                                                                                                                                                                                                                                                                   | w               | Size and position of the window relative to the top, left corner of the frame buffer defined with VIDIOC-S-FBUF. The window can extend the frame buffer width and height, the x and y coordinates can be negative, and it can lie completely outside the frame buffer. The driver clips the window accordingly, or if that is not possible, modifies its size and/or position.                                                                                                                             |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| V4L2-FIELD                                                                                                                                                                                                                                                                                                                  | field           | Applications set this field to determine which video field shall be overlaid, typically one of ``V4L2_FIELD_ANY`` (0), ``V4L2_FIELD_TOP``, ``V4L2_FIELD_BOTTOM`` or ``V4L2_FIELD_INTERLACED``. Drivers may have to choose a different field order and return the actual setting here.                                                                                                                                                                                                                      |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32                                                                                                                                                                                                                                                                                                                     | chromakey       | When chroma-keying has been negotiated with VIDIOC-S-FBUF applications set this field to the desired pixel value for the chroma key. The format is the same as the pixel format of the framebuffer (V4L2-FRAMEBUFFER fmt.pixelformat field), with bytes in host order. E. g. for ```V4L2_PIX_FMT_BGR24`` <#V4L2-PIX-FMT-BGR32>`__ the value should be 0xRRGGBB on a little endian, 0xBBGGRR on a big endian host.                                                                                          |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| V4L2-CLIP \*                                                                                                                                                                                                                                                                                                                | clips           | When chroma-keying has *not* been negotiated and VIDIOC-G-FBUF indicated this capability, applications can set this field to point to an array of clipping rectangles.                                                                                                                                                                                                                                                                                                                                     |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|                                                                                                                                                                                                                                                                                                                             |                 | Like the window coordinates w, clipping rectangles are defined relative to the top, left corner of the frame buffer. However clipping rectangles must not extend the frame buffer width and height, and they must not overlap. If possible applications should merge adjacent rectangles. Whether this must create x-y or y-x bands, or the order of rectangles, is not defined. When clip lists are not supported the driver ignores this field. Its contents after calling VIDIOC-S-FMT are undefined.   |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32                                                                                                                                                                                                                                                                                                                     | clipcount       | When the application set the clips field, this field must contain the number of clipping rectangles in the list. When clip lists are not supported the driver ignores this field, its contents after calling ``VIDIOC_S_FMT`` are undefined. When clip lists are supported but no clipping is desired this field must be set to zero.                                                                                                                                                                      |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| void \*                                                                                                                                                                                                                                                                                                                     | bitmap          | When chroma-keying has *not* been negotiated and VIDIOC-G-FBUF indicated this capability, applications can set this field to point to a clipping bit mask.                                                                                                                                                                                                                                                                                                                                                 |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| It must be of the same size as the window, w.width and w.height. Each bit corresponds to a pixel in the overlaid image, which is displayed only when the bit is *set*. Pixel coordinates translate to bits like:                                                                                                            |
|                                                                                                                                                                                                                                                                                                                             |
| ::                                                                                                                                                                                                                                                                                                                          |
|                                                                                                                                                                                                                                                                                                                             |
|     ((__u8 *) bitmap)[w.width * y + x / 8] & (1 << (x & 7))                                                                                                                                                                                                                                                                 |
|                                                                                                                                                                                                                                                                                                                             |
| where 0 ≤ x < w.width and 0 ≤ y <w.height. [2]_                                                                                                                                                                                                                                                                             |
|                                                                                                                                                                                                                                                                                                                             |
| When a clipping bit mask is not supported the driver ignores this field, its contents after calling VIDIOC-S-FMT are undefined. When a bit mask is supported but no clipping is desired this field must be set to ``NULL``.                                                                                                 |
|                                                                                                                                                                                                                                                                                                                             |
| Applications need not create a clip list or bit mask. When they pass both, or despite negotiating chroma-keying, the results are undefined. Regardless of the chosen method, the clipping abilities of the hardware may be limited in quantity or quality. The results when these limits are exceeded are undefined. [3]_   |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u8                                                                                                                                                                                                                                                                                                                      | global\_alpha   | The global alpha value used to blend the framebuffer with video images, if global alpha blending has been negotiated (``V4L2_FBUF_FLAG_GLOBAL_ALPHA``, see VIDIOC-S-FBUF, ?).                                                                                                                                                                                                                                                                                                                              |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|                                                                                                                                                                                                                                                                                                                             |                 | Note this field was added in Linux 2.6.23, extending the structure. However the `VIDIOC\_G/S/TRY\_FMT <#vidioc-g-fmt>`__ ioctls, which take a pointer to a `v4l2\_format <#v4l2-format>`__ parent structure with padding bytes at the end, are not affected.                                                                                                                                                                                                                                               |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-----------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_window

+----------------+--------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| V4L2-RECT      | c      | Coordinates of the clipping rectangle, relative to the top, left corner of the frame buffer. Only window pixels *outside* all clipping rectangles are displayed.           |
+----------------+--------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| V4L2-CLIP \*   | next   | Pointer to the next clipping rectangle, NULL when this is the last rectangle. Drivers ignore this field, it cannot be used to pass a linked list of clipping rectangles.   |
+----------------+--------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_clip [4]_

+-----------+----------+----------------------------------------------------------------------------------------------------------------+
| \_\_s32   | left     | Horizontal offset of the top, left corner of the rectangle, in pixels.                                         |
+-----------+----------+----------------------------------------------------------------------------------------------------------------+
| \_\_s32   | top      | Vertical offset of the top, left corner of the rectangle, in pixels. Offsets increase to the right and down.   |
+-----------+----------+----------------------------------------------------------------------------------------------------------------+
| \_\_u32   | width    | Width of the rectangle, in pixels.                                                                             |
+-----------+----------+----------------------------------------------------------------------------------------------------------------+
| \_\_u32   | height   | Height of the rectangle, in pixels.                                                                            |
+-----------+----------+----------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_rect

Enabling Overlay
================

To start or stop the frame buffer overlay applications call the
VIDIOC-OVERLAY ioctl.

.. [1]
   A common application of two file descriptors is the XFree86
   `Xv/V4L <#xvideo>`__ interface driver and a V4L2 application. While
   the X server controls video overlay, the application can take
   advantage of memory mapping and DMA.

   In the opinion of the designers of this API, no driver writer taking
   the efforts to support simultaneous capturing and overlay will
   restrict this ability by requiring a single file descriptor, as in
   V4L and earlier versions of V4L2. Making this optional means
   applications depending on two file descriptors need backup routines
   to be compatible with all drivers, which is considerable more work
   than using two fds in applications which do not. Also two fd's fit
   the general concept of one file descriptor for each logical stream.
   Hence as a complexity trade-off drivers *must* support two file
   descriptors and *may* support single fd operation.

.. [2]
   Should we require w.width to be a multiple of eight?

.. [3]
   When the image is written into frame buffer memory it will be
   undesirable if the driver clips out less pixels than expected,
   because the application and graphics system are not aware these
   regions need to be refreshed. The driver should clip out more pixels
   or not write the image at all.

.. [4]
   The X Window system defines "regions" which are vectors of struct
   BoxRec { short x1, y1, x2, y2; } with width = x2 - x1 and height = y2
   - y1, so one cannot pass X11 clip lists directly.
