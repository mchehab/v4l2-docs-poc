ioctl VIDIOC\_QUERYSTD
MANVOL
VIDIOC\_QUERYSTD
Sense the video standard received by the current input
int
ioctl
int
fd
int
request
v4l2\_std\_id \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_QUERYSTD

``argp``

Description
===========

The hardware may be able to detect the current video standard
automatically. To do so, applications call ``
VIDIOC_QUERYSTD`` with a pointer to a V4L2-STD-ID type. The driver
stores here a set of candidates, this can be a single flag or a set of
supported standards if for example the hardware can only distinguish
between 50 and 60 Hz systems. If no signal was detected, then the driver
will return V4L2\_STD\_UNKNOWN. When detection is not possible or fails,
the set must contain all standards supported by the current video input
or output.

Please note that drivers shall *not* switch the video standard
automatically if a new video standard is detected. Instead, drivers
should send the ``V4L2_EVENT_SOURCE_CHANGE`` event (if they support
this) and expect that userspace will take action by calling
``VIDIOC_QUERYSTD``. The reason is that a new video standard can mean
different buffer sizes as well, and you cannot change buffer sizes on
the fly. In general, applications that receive the Source Change event
will have to call ``VIDIOC_QUERYSTD``, and if the detected video
standard is valid they will have to stop streaming, set the new
standard, allocate new buffers and start streaming again.

RETURN-VALUE

ENODATA
    Standard video timings are not supported for this input or output.
