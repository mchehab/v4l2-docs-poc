ioctl FE\_READ\_STATUS
MANVOL
FE\_READ\_STATUS
Returns status information about the front-end. This call only requires
read-only access to the device
int
ioctl
int
fd
int
request
unsigned int \*
status
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_READ\_STATUS

``status``
    pointer to a bitmask integer filled with the values defined by
    FE-STATUS.

Description
===========

All DVB frontend devices support the ``FE_READ_STATUS`` ioctl. It is
used to check about the locking status of the frontend after being
tuned. The ioctl takes a pointer to an integer where the status will be
written.

NOTE: the size of status is actually sizeof(enum fe\_status), with
varies according with the architecture. This needs to be fixed in the
future.

RETURN-VALUE-DVB
int fe\_status
==============

The fe\_status parameter is used to indicate the current state and/or
state changes of the frontend hardware. It is produced using the
FE-STATUS values on a bitmask

+----------------------+---------------------------------------------------------------------------------------------------+
| ID                   | Description                                                                                       |
+======================+===================================================================================================+
| ``FE_HAS_SIGNAL``    | The frontend has found something above the noise level                                            |
+----------------------+---------------------------------------------------------------------------------------------------+
| ``FE_HAS_CARRIER``   | The frontend has found a DVB signal                                                               |
+----------------------+---------------------------------------------------------------------------------------------------+
| ``FE_HAS_VITERBI``   | The frontend FEC inner coding (Viterbi, LDPC or other inner code) is stable                       |
+----------------------+---------------------------------------------------------------------------------------------------+
| ``FE_HAS_SYNC``      | Synchronization bytes was found                                                                   |
+----------------------+---------------------------------------------------------------------------------------------------+
| ``FE_HAS_LOCK``      | The DVB were locked and everything is working                                                     |
+----------------------+---------------------------------------------------------------------------------------------------+
| ``FE_TIMEDOUT``      | no lock within the last about 2 seconds                                                           |
+----------------------+---------------------------------------------------------------------------------------------------+
| ``FE_REINIT``        | The frontend was reinitialized, application is recommended to reset DiSEqC, tone and parameters   |
+----------------------+---------------------------------------------------------------------------------------------------+

Table: enum fe\_status
