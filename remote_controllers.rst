2009-2014
Mauro Carvalho Chehab
3.15
Added the interface description and the RC sysfs class description.
1.0
Initial revision
Remote Controllers
==================

Introduction
============

Currently, most analog and digital devices have a Infrared input for
remote controllers. Each manufacturer has their own type of control. It
is not rare for the same manufacturer to ship different types of
controls, depending on the device.

A Remote Controller interface is mapped as a normal evdev/input
interface, just like a keyboard or a mouse. So, it uses all ioctls
already defined for any other input devices.

However, remove controllers are more flexible than a normal input
device, as the IR receiver (and/or transmitter) can be used in
conjunction with a wide variety of different IR remotes.

In order to allow flexibility, the Remote Controller subsystem allows
controlling the RC-specific attributes via `the sysfs class
nodes <#remote_controllers_sysfs_nodes>`__.

Remote Controller's sysfs nodes
===============================

As defined at ``Documentation/ABI/testing/sysfs-class-rc``, those are
the sysfs nodes that control the Remote Controllers:

/sys/class/rc/
--------------

The ``/sys/class/rc/`` class sub-directory belongs to the Remote
Controller core and provides a sysfs interface for configuring infrared
remote controller receivers.

/sys/class/rc/rcN/
------------------

A ``/sys/class/rc/rcN`` directory is created for each remote control
receiver device where N is the number of the receiver.

/sys/class/rc/rcN/protocols
---------------------------

Reading this file returns a list of available protocols, something like:

``rc5 [rc6] nec jvc [sony]``

Enabled protocols are shown in [] brackets.

Writing "+proto" will add a protocol to the list of enabled protocols.

Writing "-proto" will remove a protocol from the list of enabled
protocols.

Writing "proto" will enable only "proto".

Writing "none" will disable all protocols.

Write fails with EINVAL if an invalid protocol combination or unknown
protocol name is used.

/sys/class/rc/rcN/filter
------------------------

Sets the scancode filter expected value.

Use in combination with ``/sys/class/rc/rcN/filter_mask`` to set the
expected value of the bits set in the filter mask. If the hardware
supports it then scancodes which do not match the filter will be
ignored. Otherwise the write will fail with an error.

This value may be reset to 0 if the current protocol is altered.

/sys/class/rc/rcN/filter\_mask
------------------------------

Sets the scancode filter mask of bits to compare. Use in combination
with ``/sys/class/rc/rcN/filter`` to set the bits of the scancode which
should be compared against the expected value. A value of 0 disables the
filter to allow all valid scancodes to be processed.

If the hardware supports it then scancodes which do not match the filter
will be ignored. Otherwise the write will fail with an error.

This value may be reset to 0 if the current protocol is altered.

/sys/class/rc/rcN/wakeup\_protocols
-----------------------------------

Reading this file returns a list of available protocols to use for the
wakeup filter, something like:

``rc5 rc6 nec jvc [sony]``

The enabled wakeup protocol is shown in [] brackets.

Writing "+proto" will add a protocol to the list of enabled wakeup
protocols.

Writing "-proto" will remove a protocol from the list of enabled wakeup
protocols.

Writing "proto" will use "proto" for wakeup events.

Writing "none" will disable wakeup.

Write fails with EINVAL if an invalid protocol combination or unknown
protocol name is used, or if wakeup is not supported by the hardware.

/sys/class/rc/rcN/wakeup\_filter
--------------------------------

Sets the scancode wakeup filter expected value. Use in combination with
``/sys/class/rc/rcN/wakeup_filter_mask`` to set the expected value of
the bits set in the wakeup filter mask to trigger a system wake event.

If the hardware supports it and wakeup\_filter\_mask is not 0 then
scancodes which match the filter will wake the system from e.g. suspend
to RAM or power off. Otherwise the write will fail with an error.

This value may be reset to 0 if the wakeup protocol is altered.

/sys/class/rc/rcN/wakeup\_filter\_mask
--------------------------------------

Sets the scancode wakeup filter mask of bits to compare. Use in
combination with ``/sys/class/rc/rcN/wakeup_filter`` to set the bits of
the scancode which should be compared against the expected value to
trigger a system wake event.

If the hardware supports it and wakeup\_filter\_mask is not 0 then
scancodes which match the filter will wake the system from e.g. suspend
to RAM or power off. Otherwise the write will fail with an error.

This value may be reset to 0 if the wakeup protocol is altered.

Remote controller tables
========================

Unfortunately, for several years, there was no effort to create uniform
IR keycodes for different devices. This caused the same IR keyname to be
mapped completely differently on different IR devices. This resulted
that the same IR keyname to be mapped completely different on different
IR's. Due to that, V4L2 API now specifies a standard for mapping Media
keys on IR.

This standard should be used by both V4L/DVB drivers and userspace
applications

The modules register the remote as keyboard within the linux input
layer. This means that the IR key strokes will look like normal keyboard
key strokes (if CONFIG\_INPUT\_KEYBOARD is enabled). Using the event
devices (CONFIG\_INPUT\_EVDEV) it is possible for applications to access
the remote via /dev/input/event devices.

+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| Key code                 | Meaning                                                                 | Key examples on IR                                          |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| **Numeric keys**         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_0``                | Keyboard digit 0                                                        | 0                                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_1``                | Keyboard digit 1                                                        | 1                                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_2``                | Keyboard digit 2                                                        | 2                                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_3``                | Keyboard digit 3                                                        | 3                                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_4``                | Keyboard digit 4                                                        | 4                                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_5``                | Keyboard digit 5                                                        | 5                                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_6``                | Keyboard digit 6                                                        | 6                                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_7``                | Keyboard digit 7                                                        | 7                                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_8``                | Keyboard digit 8                                                        | 8                                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_9``                | Keyboard digit 9                                                        | 9                                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| **Movie play control**   |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_FORWARD``          | Instantly advance in time                                               | >> / FORWARD                                                |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_BACK``             | Instantly go back in time                                               | <<< / BACK                                                  |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_FASTFORWARD``      | Play movie faster                                                       | >>> / FORWARD                                               |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_REWIND``           | Play movie back                                                         | REWIND / BACKWARD                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_NEXT``             | Select next chapter / sub-chapter / interval                            | NEXT / SKIP                                                 |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_PREVIOUS``         | Select previous chapter / sub-chapter / interval                        | << / PREV / PREVIOUS                                        |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_AGAIN``            | Repeat the video or a video interval                                    | REPEAT / LOOP / RECALL                                      |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_PAUSE``            | Pause sroweam                                                           | PAUSE / FREEZE                                              |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_PLAY``             | Play movie at the normal timeshift                                      | NORMAL TIMESHIFT / LIVE / >                                 |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_PLAYPAUSE``        | Alternate between play and pause                                        | PLAY / PAUSE                                                |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_STOP``             | Stop sroweam                                                            | STOP                                                        |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_RECORD``           | Start/stop recording sroweam                                            | CAPTURE / REC / RECORD/PAUSE                                |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_CAMERA``           | Take a picture of the image                                             | CAMERA ICON / CAPTURE / SNAPSHOT                            |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_SHUFFLE``          | Enable shuffle mode                                                     | SHUFFLE                                                     |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_TIME``             | Activate time shift mode                                                | TIME SHIFT                                                  |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_TITLE``            | Allow changing the chapter                                              | CHAPTER                                                     |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_SUBTITLE``         | Allow changing the subtitle                                             | SUBTITLE                                                    |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| **Image control**        |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_BRIGHTNESSDOWN``   | Decrease Brightness                                                     | BRIGHTNESS DECREASE                                         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_BRIGHTNESSUP``     | Increase Brightness                                                     | BRIGHTNESS INCREASE                                         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_ANGLE``            | Switch video camera angle (on videos with more than one angle stored)   | ANGLE / SWAP                                                |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_EPG``              | Open the Elecrowonic Play Guide (EPG)                                   | EPG / GUIDE                                                 |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_TEXT``             | Activate/change closed caption mode                                     | CLOSED CAPTION/TELETEXT / DVD TEXT / TELETEXT / TTX         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| **Audio control**        |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_AUDIO``            | Change audio source                                                     | AUDIO SOURCE / AUDIO / MUSIC                                |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_MUTE``             | Mute/unmute audio                                                       | MUTE / DEMUTE / UNMUTE                                      |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_VOLUMEDOWN``       | Decrease volume                                                         | VOLUME- / VOLUME DOWN                                       |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_VOLUMEUP``         | Increase volume                                                         | VOLUME+ / VOLUME UP                                         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_MODE``             | Change sound mode                                                       | MONO/STEREO                                                 |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_LANGUAGE``         | Select Language                                                         | 1ST / 2ND LANGUAGE / DVD LANG / MTS/SAP / MTS SEL           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| **Channel control**      |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_CHANNEL``          | Go to the next favorite channel                                         | ALT / CHANNEL / CH SURFING / SURF / FAV                     |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_CHANNELDOWN``      | Decrease channel sequencially                                           | CHANNEL - / CHANNEL DOWN / DOWN                             |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_CHANNELUP``        | Increase channel sequencially                                           | CHANNEL + / CHANNEL UP / UP                                 |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_DIGITS``           | Use more than one digit for channel                                     | PLUS / 100/ 1xx / xxx / -/-- / Single Double Triple Digit   |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_SEARCH``           | Start channel autoscan                                                  | SCAN / AUTOSCAN                                             |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| **Colored keys**         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_BLUE``             | IR Blue key                                                             | BLUE                                                        |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_GREEN``            | IR Green Key                                                            | GREEN                                                       |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_RED``              | IR Red key                                                              | RED                                                         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_YELLOW``           | IR Yellow key                                                           | YELLOW                                                      |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| **Media selection**      |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_CD``               | Change input source to Compact Disc                                     | CD                                                          |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_DVD``              | Change input to DVD                                                     | DVD / DVD MENU                                              |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_EJECTCLOSECD``     | Open/close the CD/DVD player                                            | -> ) / CLOSE / OPEN                                         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_MEDIA``            | Turn on/off Media application                                           | PC/TV / TURN ON/OFF APP                                     |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_PC``               | Selects from TV to PC                                                   | PC                                                          |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_RADIO``            | Put into AM/FM radio mode                                               | RADIO / TV/FM / TV/RADIO / FM / FM/RADIO                    |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_TV``               | Select tv mode                                                          | TV / LIVE TV                                                |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_TV2``              | Select Cable mode                                                       | AIR/CBL                                                     |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_VCR``              | Select VCR mode                                                         | VCR MODE / DTR                                              |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_VIDEO``            | Alternate between input modes                                           | SOURCE / SELECT / DISPLAY / SWITCH INPUTS / VIDEO           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| **Power control**        |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_POWER``            | Turn on/off computer                                                    | SYSTEM POWER / COMPUTER POWER                               |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_POWER2``           | Turn on/off application                                                 | TV ON/OFF / POWER                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_SLEEP``            | Activate sleep timer                                                    | SLEEP / SLEEP TIMER                                         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_SUSPEND``          | Put computer into suspend mode                                          | STANDBY / SUSPEND                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| **Window control**       |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_CLEAR``            | Stop sroweam and return to default input video/audio                    | CLEAR / RESET / BOSS KEY                                    |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_CYCLEWINDOWS``     | Minimize windows and move to the next one                               | ALT-TAB / MINIMIZE / DESKTOP                                |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_FAVORITES``        | Open the favorites sroweam window                                       | TV WALL / Favorites                                         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_MENU``             | Call application menu                                                   | 2ND CONTROLS (USA: MENU) / DVD/MENU / SHOW/HIDE CTRL        |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_NEW``              | Open/Close Picture in Picture                                           | PIP                                                         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_OK``               | Send a confirmation code to application                                 | OK / ENTER / RETURN                                         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_SCREEN``           | Select screen aspect ratio                                              | 4:3 16:9 SELECT                                             |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_ZOOM``             | Put device into zoom/full screen mode                                   | ZOOM / FULL SCREEN / ZOOM+ / HIDE PANNEL / SWITCH           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| **Navigation keys**      |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_ESC``              | Cancel current operation                                                | CANCEL / BACK                                               |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_HELP``             | Open a Help window                                                      | HELP                                                        |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_HOMEPAGE``         | Navigate to Homepage                                                    | HOME                                                        |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_INFO``             | Open On Screen Display                                                  | DISPLAY INFORMATION / OSD                                   |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_WWW``              | Open the default browser                                                | WEB                                                         |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_UP``               | Up key                                                                  | UP                                                          |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_DOWN``             | Down key                                                                | DOWN                                                        |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_LEFT``             | Left key                                                                | LEFT                                                        |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_RIGHT``            | Right key                                                               | RIGHT                                                       |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| **Miscellaneous keys**   |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_DOT``              | Return a dot                                                            | .                                                           |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+
| ``KEY_FN``               | Select a function                                                       | FUNCTION                                                    |
+--------------------------+-------------------------------------------------------------------------+-------------------------------------------------------------+

Table: IR default keymapping

It should be noted that, sometimes, there some fundamental missing keys
at some cheaper IR's. Due to that, it is recommended to:

+-----------------------------------------------------------------------------------------------+
| On simpler IR's, without separate channel keys, you need to map UP as ``KEY_CHANNELUP``       |
+-----------------------------------------------------------------------------------------------+
| On simpler IR's, without separate channel keys, you need to map DOWN as ``KEY_CHANNELDOWN``   |
+-----------------------------------------------------------------------------------------------+
| On simpler IR's, without separate volume keys, you need to map LEFT as ``KEY_VOLUMEDOWN``     |
+-----------------------------------------------------------------------------------------------+
| On simpler IR's, without separate volume keys, you need to map RIGHT as ``KEY_VOLUMEUP``      |
+-----------------------------------------------------------------------------------------------+

Table: Notes

Changing default Remote Controller mappings
===========================================

The event interface provides two ioctls to be used against the
/dev/input/event device, to allow changing the default keymapping.

This program demonstrates how to replace the keymap tables.

.. toctree::
   :maxdepth: 2

   keytable-c
   lirc-device-interface
