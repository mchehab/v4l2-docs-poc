ioctl VIDIOC\_SUBDEV\_ENUM\_FRAME\_SIZE
MANVOL
VIDIOC\_SUBDEV\_ENUM\_FRAME\_SIZE
Enumerate media bus frame sizes
int
ioctl
int
fd
int
request
struct v4l2\_subdev\_frame\_size\_enum \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_SUBDEV\_ENUM\_FRAME\_SIZE

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

This ioctl allows applications to enumerate all frame sizes supported by
a sub-device on the given pad for the given media bus format. Supported
formats can be retrieved with the VIDIOC-SUBDEV-ENUM-MBUS-CODE ioctl.

To enumerate frame sizes applications initialize the pad, which , code
and index fields of the V4L2-SUBDEV-MBUS-CODE-ENUM and call the
``VIDIOC_SUBDEV_ENUM_FRAME_SIZE`` ioctl with a pointer to the structure.
Drivers fill the minimum and maximum frame sizes or return an EINVAL if
one of the input parameters is invalid.

Sub-devices that only support discrete frame sizes (such as most
sensors) will return one or more frame sizes with identical minimum and
maximum values.

Not all possible sizes in given [minimum, maximum] ranges need to be
supported. For instance, a scaler that uses a fixed-point scaling ratio
might not be able to produce every frame size between the minimum and
maximum values. Applications must use the VIDIOC-SUBDEV-S-FMT ioctl to
try the sub-device for an exact supported frame size.

Available frame sizes may depend on the current 'try' formats at other
pads of the sub-device, as well as on the current active links and the
current values of V4L2 controls. See VIDIOC-SUBDEV-G-FMT for more
information about try formats.

+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | index         | Number of the format in the enumeration, set by the application.                       |
+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | pad           | Pad number as reported by the media controller API.                                    |
+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | code          | The media bus format code, as defined in ?.                                            |
+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | min\_width    | Minimum frame width, in pixels.                                                        |
+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | max\_width    | Maximum frame width, in pixels.                                                        |
+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | min\_height   | Minimum frame height, in pixels.                                                       |
+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | max\_height   | Maximum frame height, in pixels.                                                       |
+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | which         | Frame sizes to be enumerated, from V4L2-SUBDEV-FORMAT-WHENCE.                          |
+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | reserved[8]   | Reserved for future extensions. Applications and drivers must set the array to zero.   |
+-----------+---------------+----------------------------------------------------------------------------------------+

Table: struct v4l2\_subdev\_frame\_size\_enum

RETURN-VALUE

EINVAL
    The V4L2-SUBDEV-FRAME-SIZE-ENUM pad references a non-existing pad,
    the code is invalid for the given pad or the index field is out of
    bounds.
