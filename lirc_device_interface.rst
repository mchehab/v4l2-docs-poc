LIRC Device Interface
=====================

Introduction
------------

The LIRC device interface is a bi-directional interface for transporting
raw IR data between userspace and kernelspace. Fundamentally, it is just
a chardev (/dev/lircX, for X = 0, 1, 2, ...), with a number of standard
struct file\_operations defined on it. With respect to transporting raw
IR data to and fro, the essential fops are read, write and ioctl.

Example dmesg output upon a driver registering w/LIRC:

    $ dmesg \|grep lirc\_dev

    lirc\_dev: IR Remote Control driver registered, major 248

    rc rc0: lirc\_dev: driver ir-lirc-codec (mceusb) registered at minor
    = 0

What you should see for a chardev:

    $ ls -l /dev/lirc\*

    crw-rw---- 1 root root 248, 0 Jul 2 22:20 /dev/lirc0

LIRC read fop
-------------

The lircd userspace daemon reads raw IR data from the LIRC chardev. The
exact format of the data depends on what modes a driver supports, and
what mode has been selected. lircd obtains supported modes and sets the
active mode via the ioctl interface, detailed at ?. The generally
preferred mode is LIRC\_MODE\_MODE2, in which packets containing an int
value describing an IR signal are read from the chardev.

See also http://www.lirc.org/html/technical.html for more info.

LIRC write fop
--------------

The data written to the chardev is a pulse/space sequence of integer
values. Pulses and spaces are only marked implicitly by their position.
The data must start and end with a pulse, therefore, the data must
always include an uneven number of samples. The write function must
block until the data has been transmitted by the hardware. If more data
is provided than the hardware can send, the driver returns EINVAL.

LIRC ioctl fop
--------------

The LIRC device's ioctl definition is bound by the ioctl function
definition of struct file\_operations, leaving us with an unsigned int
for the ioctl command and an unsigned long for the arg. For the purposes
of ioctl portability across 32-bit and 64-bit, these values are capped
to their 32-bit sizes.

The following ioctls can be used to change specific hardware settings.
In general each driver should have a default set of settings. The driver
implementation is expected to re-apply the default settings when the
device is closed by user-space, so that every application opening the
device can rely on working with the default settings initially.

LIRC\_GET\_FEATURES
    Obviously, get the underlying hardware device's features. If a
    driver does not announce support of certain features, calling of the
    corresponding ioctls is undefined.

LIRC\_GET\_SEND\_MODE
    Get supported transmit mode. Only LIRC\_MODE\_PULSE is supported by
    lircd.

LIRC\_GET\_REC\_MODE
    Get supported receive modes. Only LIRC\_MODE\_MODE2 and
    LIRC\_MODE\_LIRCCODE are supported by lircd.

LIRC\_GET\_SEND\_CARRIER
    Get carrier frequency (in Hz) currently used for transmit.

LIRC\_GET\_REC\_CARRIER
    Get carrier frequency (in Hz) currently used for IR reception.

LIRC\_{G,S}ET\_{SEND,REC}\_DUTY\_CYCLE
    Get/set the duty cycle (from 0 to 100) of the carrier signal.
    Currently, no special meaning is defined for 0 or 100, but this
    could be used to switch off carrier generation in the future, so
    these values should be reserved.

LIRC\_GET\_REC\_RESOLUTION
    Some receiver have maximum resolution which is defined by internal
    sample rate or data format limitations. E.g. it's common that
    signals can only be reported in 50 microsecond steps. This integer
    value is used by lircd to automatically adjust the aeps tolerance
    value in the lircd config file.

LIRC\_GET\_M{IN,AX}\_TIMEOUT
    Some devices have internal timers that can be used to detect when
    there's no IR activity for a long time. This can help lircd in
    detecting that a IR signal is finished and can speed up the decoding
    process. Returns an integer value with the minimum/maximum timeout
    that can be set. Some devices have a fixed timeout, in that case
    both ioctls will return the same value even though the timeout
    cannot be changed.

LIRC\_GET\_M{IN,AX}\_FILTER\_{PULSE,SPACE}
    Some devices are able to filter out spikes in the incoming signal
    using given filter rules. These ioctls return the hardware
    capabilities that describe the bounds of the possible filters.
    Filter settings depend on the IR protocols that are expected. lircd
    derives the settings from all protocols definitions found in its
    config file.

LIRC\_GET\_LENGTH
    Retrieves the code length in bits (only for LIRC\_MODE\_LIRCCODE).
    Reads on the device must be done in blocks matching the bit count.
    The bit could should be rounded up so that it matches full bytes.

LIRC\_SET\_{SEND,REC}\_MODE
    Set send/receive mode. Largely obsolete for send, as only
    LIRC\_MODE\_PULSE is supported.

LIRC\_SET\_{SEND,REC}\_CARRIER
    Set send/receive carrier (in Hz).

LIRC\_SET\_TRANSMITTER\_MASK
    This enables the given set of transmitters. The first transmitter is
    encoded by the least significant bit, etc. When an invalid bit mask
    is given, i.e. a bit is set, even though the device does not have so
    many transitters, then this ioctl returns the number of available
    transitters and does nothing otherwise.

LIRC\_SET\_REC\_TIMEOUT
    Sets the integer value for IR inactivity timeout (cf.
    LIRC\_GET\_MIN\_TIMEOUT and LIRC\_GET\_MAX\_TIMEOUT). A value of 0
    (if supported by the hardware) disables all hardware timeouts and
    data should be reported as soon as possible. If the exact value
    cannot be set, then the next possible value \_greater\_ than the
    given value should be set.

LIRC\_SET\_REC\_TIMEOUT\_REPORTS
    Enable (1) or disable (0) timeout reports in LIRC\_MODE\_MODE2. By
    default, timeout reports should be turned off.

LIRC\_SET\_REC\_FILTER\_{,PULSE,SPACE}
    Pulses/spaces shorter than this are filtered out by hardware. If
    filters cannot be set independently for pulse/space, the
    corresponding ioctls must return an error and LIRC\_SET\_REC\_FILTER
    shall be used instead.

LIRC\_SET\_MEASURE\_CARRIER\_MODE
    Enable (1)/disable (0) measure mode. If enabled, from the next key
    press on, the driver will send LIRC\_MODE2\_FREQUENCY packets. By
    default this should be turned off.

LIRC\_SET\_REC\_{DUTY\_CYCLE,CARRIER}\_RANGE
    To set a range use
    LIRC\_SET\_REC\_DUTY\_CYCLE\_RANGE/LIRC\_SET\_REC\_CARRIER\_RANGE
    with the lower bound first and later
    LIRC\_SET\_REC\_DUTY\_CYCLE/LIRC\_SET\_REC\_CARRIER with the upper
    bound.

LIRC\_NOTIFY\_DECODE
    This ioctl is called by lircd whenever a successful decoding of an
    incoming IR signal could be done. This can be used by supporting
    hardware to give visual feedback to the user e.g. by flashing a LED.

LIRC\_SETUP\_{START,END}
    Setting of several driver parameters can be optimized by
    encapsulating the according ioctl calls with
    LIRC\_SETUP\_START/LIRC\_SETUP\_END. When a driver receives a
    LIRC\_SETUP\_START ioctl it can choose to not commit further setting
    changes to the hardware until a LIRC\_SETUP\_END is received. But
    this is open to the driver implementation and every driver must also
    handle parameter changes which are not encapsulated by
    LIRC\_SETUP\_START and LIRC\_SETUP\_END. Drivers can also choose to
    ignore these ioctls.

LIRC\_SET\_WIDEBAND\_RECEIVER
    Some receivers are equipped with special wide band receiver which is
    intended to be used to learn output of existing remote. Calling that
    ioctl with (1) will enable it, and with (0) disable it. This might
    be useful of receivers that have otherwise narrow band receiver that
    prevents them to be used with some remotes. Wide band receiver might
    also be more precise On the other hand its disadvantage it usually
    reduced range of reception. Note: wide band receiver might be
    implictly enabled if you enable carrier reports. In that case it
    will be disabled as soon as you disable carrier reports. Trying to
    disable wide band receiver while carrier reports are active will do
    nothing.

RETURN-VALUE
