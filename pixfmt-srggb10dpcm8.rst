V4L2\_PIX\_FMT\_SBGGR10DPCM8 ('bBA8'), V4L2\_PIX\_FMT\_SGBRG10DPCM8
('bGA8'), V4L2\_PIX\_FMT\_SGRBG10DPCM8 ('BD10'),
V4L2\_PIX\_FMT\_SRGGB10DPCM8 ('bRA8'),
MANVOL
V4L2\_PIX\_FMT\_SBGGR10DPCM8
V4L2\_PIX\_FMT\_SGBRG10DPCM8
V4L2\_PIX\_FMT\_SGRBG10DPCM8
V4L2\_PIX\_FMT\_SRGGB10DPCM8
10-bit Bayer formats compressed to 8 bits
Description
===========

These four pixel formats are raw sRGB / Bayer formats with 10 bits per
colour compressed to 8 bits each, using DPCM compression. DPCM,
differential pulse-code modulation, is lossy. Each colour component
consumes 8 bits of memory. In other respects this format is similar to
?.
