ioctl VIDIOC\_SUBDEV\_ENUM\_MBUS\_CODE
MANVOL
VIDIOC\_SUBDEV\_ENUM\_MBUS\_CODE
Enumerate media bus formats
int
ioctl
int
fd
int
request
struct v4l2\_subdev\_mbus\_code\_enum \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_SUBDEV\_ENUM\_MBUS\_CODE

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

To enumerate media bus formats available at a given sub-device pad
applications initialize the pad, which and index fields of
V4L2-SUBDEV-MBUS-CODE-ENUM and call the ``VIDIOC_SUBDEV_ENUM_MBUS_CODE``
ioctl with a pointer to this structure. Drivers fill the rest of the
structure or return an EINVAL if either the pad or index are invalid.
All media bus formats are enumerable by beginning at index zero and
incrementing by one until EINVAL is returned.

Available media bus formats may depend on the current 'try' formats at
other pads of the sub-device, as well as on the current active links.
See VIDIOC-SUBDEV-G-FMT for more information about the try formats.

+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | pad           | Pad number as reported by the media controller API.                                    |
+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | index         | Number of the format in the enumeration, set by the application.                       |
+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | code          | The media bus format code, as defined in ?.                                            |
+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | which         | Media bus format codes to be enumerated, from V4L2-SUBDEV-FORMAT-WHENCE.               |
+-----------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32   | reserved[8]   | Reserved for future extensions. Applications and drivers must set the array to zero.   |
+-----------+---------------+----------------------------------------------------------------------------------------+

Table: struct v4l2\_subdev\_mbus\_code\_enum

RETURN-VALUE

EINVAL
    The V4L2-SUBDEV-MBUS-CODE-ENUM pad references a non-existing pad, or
    the index field is out of bounds.
