ioctl VIDIOC\_ENUM\_FRAMEINTERVALS
MANVOL
VIDIOC\_ENUM\_FRAMEINTERVALS
Enumerate frame intervals
int
ioctl
int
fd
int
request
struct v4l2\_frmivalenum \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_ENUM\_FRAMEINTERVALS

``argp``
    Pointer to a V4L2-FRMIVALENUM structure that contains a pixel format
    and size and receives a frame interval.

Description
===========

This ioctl allows applications to enumerate all frame intervals that the
device supports for the given pixel format and frame size.

The supported pixel formats and frame sizes can be obtained by using the
VIDIOC-ENUM-FMT and VIDIOC-ENUM-FRAMESIZES functions.

The return value and the content of the v4l2\_frmivalenum.type field
depend on the type of frame intervals the device supports. Here are the
semantics of the function for the different cases:

-  **Discrete:** The function returns success if the given index value
   (zero-based) is valid. The application should increase the index by
   one for each call until ``EINVAL`` is returned. The
   \`v4l2\_frmivalenum.type\` field is set to
   \`V4L2\_FRMIVAL\_TYPE\_DISCRETE\` by the driver. Of the union only
   the \`discrete\` member is valid.

-  **Step-wise:** The function returns success if the given index value
   is zero and ``EINVAL`` for any other index value. The
   v4l2\_frmivalenum.type field is set to ``V4L2_FRMIVAL_TYPE_STEPWISE``
   by the driver. Of the union only the stepwise member is valid.

-  **Continuous:** This is a special case of the step-wise type above.
   The function returns success if the given index value is zero and
   ``EINVAL`` for any other index value. The v4l2\_frmivalenum.type
   field is set to ``V4L2_FRMIVAL_TYPE_CONTINUOUS`` by the driver. Of
   the union only the stepwise member is valid and the step value is set
   to 1.

When the application calls the function with index zero, it must check
the type field to determine the type of frame interval enumeration the
device supports. Only for the ``V4L2_FRMIVAL_TYPE_DISCRETE`` type does
it make sense to increase the index value to receive more frame
intervals.

Note that the order in which the frame intervals are returned has no
special meaning. In particular does it not say anything about potential
default frame intervals.

Applications can assume that the enumeration data does not change
without any interaction from the application itself. This means that the
enumeration data is consistent if the application does not perform any
other ioctl calls while it runs the frame interval enumeration.

Notes
=====

-  **Frame intervals and frame rates:** The V4L2 API uses frame
   intervals instead of frame rates. Given the frame interval the frame
   rate can be computed as follows:

   ::

       frame_rate = 1 / frame_interval

Structs
=======

In the structs below, *IN* denotes a value that has to be filled in by
the application, *OUT* denotes values that the driver fills in. The
application should zero out all members except for the *IN* fields.

+--------------+--------+---------------------------------+
| V4L2-FRACT   | min    | Minimum frame interval [s].     |
+--------------+--------+---------------------------------+
| V4L2-FRACT   | max    | Maximum frame interval [s].     |
+--------------+--------+---------------------------------+
| V4L2-FRACT   | step   | Frame interval step size [s].   |
+--------------+--------+---------------------------------+

Table: struct v4l2\_frmival\_stepwise

+-----------+-------------------------+------------+------------------------------------------------------------------------------+
| \_\_u32   | index                   |            | IN: Index of the given frame interval in the enumeration.                    |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
| \_\_u32   | pixel\_format           |            | IN: Pixel format for which the frame intervals are enumerated.               |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
| \_\_u32   | width                   |            | IN: Frame width for which the frame intervals are enumerated.                |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
| \_\_u32   | height                  |            | IN: Frame height for which the frame intervals are enumerated.               |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
| \_\_u32   | type                    |            | OUT: Frame interval type the device supports.                                |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
| union     |                         |            | OUT: Frame interval with the given index.                                    |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
|           | V4L2-FRACT              | discrete   | Frame interval [s].                                                          |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
|           | V4L2-FRMIVAL-STEPWISE   | stepwise   |                                                                              |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
| \_\_u32   | reserved[2]             |            | Reserved space for future use. Must be zeroed by drivers and applications.   |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+

Table: struct v4l2\_frmivalenum

Enums
=====

+------------------------------------+-----+-------------------------------------+
| ``V4L2_FRMIVAL_TYPE_DISCRETE``     | 1   | Discrete frame interval.            |
+------------------------------------+-----+-------------------------------------+
| ``V4L2_FRMIVAL_TYPE_CONTINUOUS``   | 2   | Continuous frame interval.          |
+------------------------------------+-----+-------------------------------------+
| ``V4L2_FRMIVAL_TYPE_STEPWISE``     | 3   | Step-wise defined frame interval.   |
+------------------------------------+-----+-------------------------------------+

Table: enum v4l2\_frmivaltypes

RETURN-VALUE
