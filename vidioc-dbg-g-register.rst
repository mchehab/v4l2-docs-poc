ioctl VIDIOC\_DBG\_G\_REGISTER, VIDIOC\_DBG\_S\_REGISTER
MANVOL
VIDIOC\_DBG\_G\_REGISTER
VIDIOC\_DBG\_S\_REGISTER
Read or write hardware registers
int
ioctl
int
fd
int
request
struct v4l2\_dbg\_register \*
argp
int
ioctl
int
fd
int
request
const struct v4l2\_dbg\_register \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_DBG\_G\_REGISTER, VIDIOC\_DBG\_S\_REGISTER

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

For driver debugging purposes these ioctls allow test applications to
access hardware registers directly. Regular applications must not use
them.

Since writing or even reading registers can jeopardize the system
security, its stability and damage the hardware, both ioctls require
superuser privileges. Additionally the Linux kernel must be compiled
with the ``CONFIG_VIDEO_ADV_DEBUG`` option to enable these ioctls.

To write a register applications must initialize all fields of a
V4L2-DBG-REGISTER except for size and call ``VIDIOC_DBG_S_REGISTER``
with a pointer to this structure. The match.type and match.addr or
match.name fields select a chip on the TV card, the reg field specifies
a register number and the val field the value to be written into the
register.

To read a register applications must initialize the match.type,
match.addr or match.name and reg fields, and call
``VIDIOC_DBG_G_REGISTER`` with a pointer to this structure. On success
the driver stores the register value in the val field and the size (in
bytes) of the value in size.

When match.type is ``V4L2_CHIP_MATCH_BRIDGE``, match.addr selects the
nth non-sub-device chip on the TV card. The number zero always selects
the host chip, ⪚ the chip connected to the PCI or USB bus. You can find
out which chips are present with the VIDIOC-DBG-G-CHIP-INFO ioctl.

When match.type is ``V4L2_CHIP_MATCH_SUBDEV``, match.addr selects the
nth sub-device.

These ioctls are optional, not all drivers may support them. However
when a driver supports these ioctls it must also support
VIDIOC-DBG-G-CHIP-INFO. Conversely it may support
``VIDIOC_DBG_G_CHIP_INFO`` but not these ioctls.

``VIDIOC_DBG_G_REGISTER`` and ``VIDIOC_DBG_S_REGISTER`` were introduced
in Linux 2.6.21, but their API was changed to the one described here in
kernel 2.6.29.

We recommended the v4l2-dbg utility over calling these ioctls directly.
It is available from the LinuxTV v4l-dvb repository; see
https://linuxtv.org/repo/ for access instructions.

+-----------+---------------+---------------------------------------+-----------------------------------------------------------------------------------------+
| \_\_u32   | type          | See ? for a list of possible types.   |
+-----------+---------------+---------------------------------------+-----------------------------------------------------------------------------------------+
| union     | (anonymous)   |
+-----------+---------------+---------------------------------------+-----------------------------------------------------------------------------------------+
|           | \_\_u32       | addr                                  | Match a chip by this number, interpreted according to the type field.                   |
+-----------+---------------+---------------------------------------+-----------------------------------------------------------------------------------------+
|           | char          | name[32]                              | Match a chip by this name, interpreted according to the type field. Currently unused.   |
+-----------+---------------+---------------------------------------+-----------------------------------------------------------------------------------------+

Table: struct v4l2\_dbg\_match

+---------------------------+---------+------------------------------------------------------------+
| struct v4l2\_dbg\_match   | match   | How to match the chip, see ?.                              |
+---------------------------+---------+------------------------------------------------------------+
| \_\_u32                   | size    | The register size in bytes.                                |
+---------------------------+---------+------------------------------------------------------------+
| \_\_u64                   | reg     | A register number.                                         |
+---------------------------+---------+------------------------------------------------------------+
| \_\_u64                   | val     | The value read from, or to be written into the register.   |
+---------------------------+---------+------------------------------------------------------------+

Table: struct v4l2\_dbg\_register

+------------------------------+-----+-----------------------------------------------------------------------------------------+
| ``V4L2_CHIP_MATCH_BRIDGE``   | 0   | Match the nth chip on the card, zero for the bridge chip. Does not match sub-devices.   |
+------------------------------+-----+-----------------------------------------------------------------------------------------+
| ``V4L2_CHIP_MATCH_SUBDEV``   | 4   | Match the nth sub-device.                                                               |
+------------------------------+-----+-----------------------------------------------------------------------------------------+

Table: Chip Match Types

RETURN-VALUE

EPERM
    Insufficient permissions. Root privileges are required to execute
    these ioctls.
