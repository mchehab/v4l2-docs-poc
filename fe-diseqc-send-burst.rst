ioctl FE\_DISEQC\_SEND\_BURST
MANVOL
FE\_DISEQC\_SEND\_BURST
Sends a 22KHz tone burst for 2x1 mini DiSEqC satellite selection.
int
ioctl
int
fd
int
request
enum fe\_sec\_mini\_cmd \*
tone
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_DISEQC\_SEND\_BURST

``tone``
    pointer to FE-SEC-MINI-CMD

Description
===========

This ioctl is used to set the generation of a 22kHz tone burst for mini
DiSEqC satellite selection for 2x1 switches. This call requires
read/write permissions.

It provides support for what's specified at `Digital Satellite Equipment
Control (DiSEqC) - Simple "ToneBurst" Detection Circuit
specification. <http://www.eutelsat.com/files/contributed/satellites/pdf/Diseqc/associated%20docs/simple_tone_burst_detec.pdf>`__

RETURN-VALUE-DVB
enum fe\_sec\_mini\_cmd
=======================

+------------------+------------------------------------------------------------------+
| ID               | Description                                                      |
+==================+==================================================================+
| ``SEC_MINI_A``   | Sends a mini-DiSEqC 22kHz '0' Tone Burst to select satellite-A   |
+------------------+------------------------------------------------------------------+
| ``SEC_MINI_B``   | Sends a mini-DiSEqC 22kHz '1' Data Burst to select satellite-B   |
+------------------+------------------------------------------------------------------+

Table: enum fe\_sec\_mini\_cmd
