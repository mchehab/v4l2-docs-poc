The DVB CA device controls the conditional access hardware. It can be
accessed through ``/dev/dvb/adapter?/ca?``. Data types and and ioctl
definitions can be accessed by including ``linux/dvb/ca.h`` in your
application.

CA Data Types
=============

ca\_slot\_info\_t
-----------------

::

    typedef struct ca_slot_info {
        int num;               /⋆ slot number ⋆/

        int type;              /⋆ CA interface this slot supports ⋆/
    #define CA_CI            1     /⋆ CI high level interface ⋆/
    #define CA_CI_LINK       2     /⋆ CI link layer level interface ⋆/
    #define CA_CI_PHYS       4     /⋆ CI physical layer level interface ⋆/
    #define CA_DESCR         8     /⋆ built-in descrambler ⋆/
    #define CA_SC          128     /⋆ simple smart card interface ⋆/

        unsigned int flags;
    #define CA_CI_MODULE_PRESENT 1 /⋆ module (or card) inserted ⋆/
    #define CA_CI_MODULE_READY   2
    } ca_slot_info_t;

ca\_descr\_info\_t
------------------

::

    typedef struct ca_descr_info {
        unsigned int num;  /⋆ number of available descramblers (keys) ⋆/
        unsigned int type; /⋆ type of supported scrambling system ⋆/
    #define CA_ECD           1
    #define CA_NDS           2
    #define CA_DSS           4
    } ca_descr_info_t;

ca\_caps\_t
-----------

::

    typedef struct ca_caps {
        unsigned int slot_num;  /⋆ total number of CA card and module slots ⋆/
        unsigned int slot_type; /⋆ OR of all supported types ⋆/
        unsigned int descr_num; /⋆ total number of descrambler slots (keys) ⋆/
        unsigned int descr_type;/⋆ OR of all supported types ⋆/
     } ca_cap_t;

ca\_msg\_t
----------

::

    /⋆ a message to/from a CI-CAM ⋆/
    typedef struct ca_msg {
        unsigned int index;
        unsigned int type;
        unsigned int length;
        unsigned char msg[256];
    } ca_msg_t;

ca\_descr\_t
------------

::

    typedef struct ca_descr {
        unsigned int index;
        unsigned int parity;
        unsigned char cw[8];
    } ca_descr_t;

ca-pid
------

::

    typedef struct ca_pid {
        unsigned int pid;
        int index;      /⋆ -1 == disable⋆/
    } ca_pid_t;

CA Function Calls
=================

open()
------

DESCRIPTION

+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| This system call opens a named ca device (e.g. /dev/ost/ca) for subsequent use.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| When an open() call has succeeded, the device will be ready for use. The significance of blocking or non-blocking mode is described in the documentation for functions where there is a difference. It does not affect the semantics of the open() call itself. A device opened in blocking mode can later be put into non-blocking mode (and vice versa) using the F\_SETFL command of the fcntl system call. This is a standard system call, documented in the Linux manual page for fcntl. Only one user can open the CA Device in O\_RDWR mode. All other attempts to open the device in this mode will fail, and an error code will be returned.   |
+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

SYNOPSIS

+------------------------------------------------+
| int open(const char ⋆deviceName, int flags);   |
+------------------------------------------------+

PARAMETERS

+---------------------------+-----------------------------------------+
| const char \*deviceName   | Name of specific video device.          |
+---------------------------+-----------------------------------------+
| int flags                 | A bit-wise OR of the following flags:   |
+---------------------------+-----------------------------------------+
|                           | O\_RDONLY read-only access              |
+---------------------------+-----------------------------------------+
|                           | O\_RDWR read/write access               |
+---------------------------+-----------------------------------------+
|                           | O\_NONBLOCK open in non-blocking mode   |
+---------------------------+-----------------------------------------+
|                           | (blocking mode is the default)          |
+---------------------------+-----------------------------------------+

RETURN VALUE

+-------------+---------------------------------------+
| ENODEV      | Device driver not loaded/available.   |
+-------------+---------------------------------------+
| EINTERNAL   | Internal error.                       |
+-------------+---------------------------------------+
| EBUSY       | Device or resource busy.              |
+-------------+---------------------------------------+
| EINVAL      | Invalid argument.                     |
+-------------+---------------------------------------+

close()
-------

DESCRIPTION

+-------------------------------------------------------------+
| This system call closes a previously opened audio device.   |
+-------------------------------------------------------------+

SYNOPSIS

+----------------------+
| int close(int fd);   |
+----------------------+

PARAMETERS

+----------+----------------------------------------------------------+
| int fd   | File descriptor returned by a previous call to open().   |
+----------+----------------------------------------------------------+

RETURN VALUE

+---------+-------------------------------------------+
| EBADF   | fd is not a valid open file descriptor.   |
+---------+-------------------------------------------+

CA\_RESET
---------

DESCRIPTION

+---------------------------------------------------------+
| This ioctl is undocumented. Documentation is welcome.   |
+---------------------------------------------------------+

SYNOPSIS

+-------------------------------------------+
| int ioctl(fd, int request = CA\_RESET);   |
+-------------------------------------------+

PARAMETERS

+---------------+----------------------------------------------------------+
| int fd        | File descriptor returned by a previous call to open().   |
+---------------+----------------------------------------------------------+
| int request   | Equals CA\_RESET for this command.                       |
+---------------+----------------------------------------------------------+

RETURN-VALUE-DVB
CA\_GET\_CAP
------------

DESCRIPTION

+---------------------------------------------------------+
| This ioctl is undocumented. Documentation is welcome.   |
+---------------------------------------------------------+

SYNOPSIS

+--------------------------------------------------------------+
| int ioctl(fd, int request = CA\_GET\_CAP, ca\_caps\_t \*);   |
+--------------------------------------------------------------+

PARAMETERS

+------------------+----------------------------------------------------------+
| int fd           | File descriptor returned by a previous call to open().   |
+------------------+----------------------------------------------------------+
| int request      | Equals CA\_GET\_CAP for this command.                    |
+------------------+----------------------------------------------------------+
| ca\_caps\_t \*   | Undocumented.                                            |
+------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
CA\_GET\_SLOT\_INFO
-------------------

DESCRIPTION

+---------------------------------------------------------+
| This ioctl is undocumented. Documentation is welcome.   |
+---------------------------------------------------------+

SYNOPSIS

+---------------------------------------------------------------------------+
| int ioctl(fd, int request = CA\_GET\_SLOT\_INFO, ca\_slot\_info\_t \*);   |
+---------------------------------------------------------------------------+

PARAMETERS

+------------------------+----------------------------------------------------------+
| int fd                 | File descriptor returned by a previous call to open().   |
+------------------------+----------------------------------------------------------+
| int request            | Equals CA\_GET\_SLOT\_INFO for this command.             |
+------------------------+----------------------------------------------------------+
| ca\_slot\_info\_t \*   | Undocumented.                                            |
+------------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
CA\_GET\_DESCR\_INFO
--------------------

DESCRIPTION

+---------------------------------------------------------+
| This ioctl is undocumented. Documentation is welcome.   |
+---------------------------------------------------------+

SYNOPSIS

+-----------------------------------------------------------------------------+
| int ioctl(fd, int request = CA\_GET\_DESCR\_INFO, ca\_descr\_info\_t \*);   |
+-----------------------------------------------------------------------------+

PARAMETERS

+-------------------------+----------------------------------------------------------+
| int fd                  | File descriptor returned by a previous call to open().   |
+-------------------------+----------------------------------------------------------+
| int request             | Equals CA\_GET\_DESCR\_INFO for this command.            |
+-------------------------+----------------------------------------------------------+
| ca\_descr\_info\_t \*   | Undocumented.                                            |
+-------------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
CA\_GET\_MSG
------------

DESCRIPTION

+---------------------------------------------------------+
| This ioctl is undocumented. Documentation is welcome.   |
+---------------------------------------------------------+

SYNOPSIS

+-------------------------------------------------------------+
| int ioctl(fd, int request = CA\_GET\_MSG, ca\_msg\_t \*);   |
+-------------------------------------------------------------+

PARAMETERS

+-----------------+----------------------------------------------------------+
| int fd          | File descriptor returned by a previous call to open().   |
+-----------------+----------------------------------------------------------+
| int request     | Equals CA\_GET\_MSG for this command.                    |
+-----------------+----------------------------------------------------------+
| ca\_msg\_t \*   | Undocumented.                                            |
+-----------------+----------------------------------------------------------+

RETURN-VALUE-DVB
CA\_SEND\_MSG
-------------

DESCRIPTION

+---------------------------------------------------------+
| This ioctl is undocumented. Documentation is welcome.   |
+---------------------------------------------------------+

SYNOPSIS

+--------------------------------------------------------------+
| int ioctl(fd, int request = CA\_SEND\_MSG, ca\_msg\_t \*);   |
+--------------------------------------------------------------+

PARAMETERS

+-----------------+----------------------------------------------------------+
| int fd          | File descriptor returned by a previous call to open().   |
+-----------------+----------------------------------------------------------+
| int request     | Equals CA\_SEND\_MSG for this command.                   |
+-----------------+----------------------------------------------------------+
| ca\_msg\_t \*   | Undocumented.                                            |
+-----------------+----------------------------------------------------------+

RETURN-VALUE-DVB
CA\_SET\_DESCR
--------------

DESCRIPTION

+---------------------------------------------------------+
| This ioctl is undocumented. Documentation is welcome.   |
+---------------------------------------------------------+

SYNOPSIS

+-----------------------------------------------------------------+
| int ioctl(fd, int request = CA\_SET\_DESCR, ca\_descr\_t \*);   |
+-----------------------------------------------------------------+

PARAMETERS

+-------------------+----------------------------------------------------------+
| int fd            | File descriptor returned by a previous call to open().   |
+-------------------+----------------------------------------------------------+
| int request       | Equals CA\_SET\_DESCR for this command.                  |
+-------------------+----------------------------------------------------------+
| ca\_descr\_t \*   | Undocumented.                                            |
+-------------------+----------------------------------------------------------+

RETURN-VALUE-DVB
CA\_SET\_PID
------------

DESCRIPTION

+---------------------------------------------------------+
| This ioctl is undocumented. Documentation is welcome.   |
+---------------------------------------------------------+

SYNOPSIS

+-------------------------------------------------------------+
| int ioctl(fd, int request = CA\_SET\_PID, ca\_pid\_t \*);   |
+-------------------------------------------------------------+

PARAMETERS

+-----------------+----------------------------------------------------------+
| int fd          | File descriptor returned by a previous call to open().   |
+-----------------+----------------------------------------------------------+
| int request     | Equals CA\_SET\_PID for this command.                    |
+-----------------+----------------------------------------------------------+
| ca\_pid\_t \*   | Undocumented.                                            |
+-----------------+----------------------------------------------------------+

RETURN-VALUE-DVB
