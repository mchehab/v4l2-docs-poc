ioctl FE\_DISEQC\_RESET\_OVERLOAD
MANVOL
FE\_DISEQC\_RESET\_OVERLOAD
Restores the power to the antenna subsystem, if it was powered off due
to power overload.
int
ioctl
int
fd
int
request
NULL
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_DISEQC\_RESET\_OVERLOAD

Description
===========

If the bus has been automatically powered off due to power overload,
this ioctl call restores the power to the bus. The call requires
read/write access to the device. This call has no effect if the device
is manually powered off. Not all DVB adapters support this ioctl.

RETURN-VALUE-DVB
