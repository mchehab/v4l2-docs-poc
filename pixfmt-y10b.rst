V4L2\_PIX\_FMT\_Y10BPACK ('Y10B')
MANVOL
V4L2\_PIX\_FMT\_Y10BPACK
Grey-scale image as a bit-packed array
Description
===========

This is a packed grey-scale image format with a depth of 10 bits per
pixel. Pixels are stored in a bit-packed array of 10bit bits per pixel,
with no padding between them and with the most significant bits coming
first from the left.

**Bit-packed representation.**

pixels cross the byte boundary and have a ratio of 5 bytes for each 4
pixels.

+--------------------------------------------------------------------------+
| Y'\ :sub:`00[9:2]`                                                       |
+--------------------------------------------------------------------------+
