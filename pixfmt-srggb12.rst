V4L2\_PIX\_FMT\_SRGGB12 ('RG12'), V4L2\_PIX\_FMT\_SGRBG12 ('BA12'),
V4L2\_PIX\_FMT\_SGBRG12 ('GB12'), V4L2\_PIX\_FMT\_SBGGR12 ('BG12'),
MANVOL
V4L2\_PIX\_FMT\_SRGGB12
V4L2\_PIX\_FMT\_SGRBG12
V4L2\_PIX\_FMT\_SGBRG12
V4L2\_PIX\_FMT\_SBGGR12
12-bit Bayer formats expanded to 16 bits
Description
===========

These four pixel formats are raw sRGB / Bayer formats with 12 bits per
colour. Each colour component is stored in a 16-bit word, with 4 unused
high bits filled with zeros. Each n-pixel row contains n/2 green samples
and n/2 blue or red samples, with alternating red and blue rows. Bytes
are stored in memory in little endian order. They are conventionally
described as GRGR... BGBG..., RGRG... GBGB..., etc. Below is an example
of one of these formats

**Byte Order..**

Each cell is one byte, high 6 bits in high bytes are 0.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 8:                                                               |
+--------------------------------------------------------------------------+
| start + 16:                                                              |
+--------------------------------------------------------------------------+
| start + 24:                                                              |
+--------------------------------------------------------------------------+
