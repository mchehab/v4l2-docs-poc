ioctl VIDIOC\_LOG\_STATUS
MANVOL
VIDIOC\_LOG\_STATUS
Log driver status information
int
ioctl
int
fd
int
request
Description
===========

As the video/audio devices become more complicated it becomes harder to
debug problems. When this ioctl is called the driver will output the
current device status to the kernel log. This is particular useful when
dealing with problems like no sound, no video and incorrectly tuned
channels. Also many modern devices autodetect video and audio standards
and this ioctl will report what the device thinks what the standard is.
Mismatches may give an indication where the problem is.

This ioctl is optional and not all drivers support it. It was introduced
in Linux 2.6.15.

RETURN-VALUE
