ioctl VIDIOC\_G\_EDID, VIDIOC\_S\_EDID
MANVOL
VIDIOC\_G\_EDID
VIDIOC\_S\_EDID
VIDIOC\_SUBDEV\_G\_EDID
VIDIOC\_SUBDEV\_S\_EDID
Get or set the EDID of a video receiver/transmitter
int
ioctl
int
fd
int
request
struct v4l2\_edid \*
argp
int
ioctl
int
fd
int
request
struct v4l2\_edid \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_EDID, VIDIOC\_S\_EDID, VIDIOC\_SUBDEV\_G\_EDID,
    VIDIOC\_SUBDEV\_S\_EDID

``argp``

Description
===========

These ioctls can be used to get or set an EDID associated with an input
from a receiver or an output of a transmitter device. They can be used
with subdevice nodes (/dev/v4l-subdevX) or with video nodes
(/dev/videoX).

When used with video nodes the pad field represents the input (for video
capture devices) or output (for video output devices) index as is
returned by VIDIOC-ENUMINPUT and VIDIOC-ENUMOUTPUT respectively. When
used with subdevice nodes the pad field represents the input or output
pad of the subdevice. If there is no EDID support for the given pad
value, then the EINVAL will be returned.

To get the EDID data the application has to fill in the pad,
start\_block, blocks and edid fields and call ``VIDIOC_G_EDID``. The
current EDID from block start\_block and of size blocks will be placed
in the memory edid points to. The edid pointer must point to memory at
least blocks \* 128 bytes large (the size of one block is 128 bytes).

If there are fewer blocks than specified, then the driver will set
blocks to the actual number of blocks. If there are no EDID blocks
available at all, then the error code ENODATA is set.

If blocks have to be retrieved from the sink, then this call will block
until they have been read.

If start\_block and blocks are both set to 0 when ``VIDIOC_G_EDID`` is
called, then the driver will set blocks to the total number of available
EDID blocks and it will return 0 without copying any data. This is an
easy way to discover how many EDID blocks there are. Note that if there
are no EDID blocks available at all, then the driver will set blocks to
0 and it returns 0.

To set the EDID blocks of a receiver the application has to fill in the
pad, blocks and edid fields and set start\_block to 0. It is not
possible to set part of an EDID, it is always all or nothing. Setting
the EDID data is only valid for receivers as it makes no sense for a
transmitter.

The driver assumes that the full EDID is passed in. If there are more
EDID blocks than the hardware can handle then the EDID is not written,
but instead the error code E2BIG is set and blocks is set to the maximum
that the hardware supports. If start\_block is any value other than 0
then the error code EINVAL is set.

To disable an EDID you set blocks to 0. Depending on the hardware this
will drive the hotplug pin low and/or block the source from reading the
EDID data in some way. In any case, the end result is the same: the EDID
is no longer available.

+-------------+----------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32     | pad            | Pad for which to get/set the EDID blocks. When used with a video device node the pad represents the input or output index as returned by VIDIOC-ENUMINPUT and VIDIOC-ENUMOUTPUT respectively.             |
+-------------+----------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32     | start\_block   | Read the EDID from starting with this block. Must be 0 when setting the EDID.                                                                                                                             |
+-------------+----------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32     | blocks         | The number of blocks to get or set. Must be less or equal to 256 (the maximum number of blocks as defined by the standard). When you set the EDID and blocks is 0, then the EDID is disabled or erased.   |
+-------------+----------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32     | reserved[5]    | Reserved for future extensions. Applications and drivers must set the array to zero.                                                                                                                      |
+-------------+----------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u8 \*   | edid           | Pointer to memory that contains the EDID. The minimum size is blocks \* 128.                                                                                                                              |
+-------------+----------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_edid

RETURN-VALUE

ENODATA
    The EDID data is not available.

E2BIG
    The EDID data you provided is more than the hardware can handle.
