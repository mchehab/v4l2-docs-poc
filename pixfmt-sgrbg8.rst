V4L2\_PIX\_FMT\_SGRBG8 ('GRBG')
MANVOL
V4L2\_PIX\_FMT\_SGRBG8
Bayer RGB format
Description
===========

This is commonly the native format of digital cameras, reflecting the
arrangement of sensors on the CCD device. Only one red, green or blue
value is given for each pixel. Missing components must be interpolated
from neighbouring pixels. From left to right the first row consists of a
green and blue value, the second row of a red and green value. This
scheme repeats to the right and down for every two columns and rows.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 4:                                                               |
+--------------------------------------------------------------------------+
| start + 8:                                                               |
+--------------------------------------------------------------------------+
| start + 12:                                                              |
+--------------------------------------------------------------------------+
