ioctl VIDIOC\_SUBSCRIBE\_EVENT, VIDIOC\_UNSUBSCRIBE\_EVENT
MANVOL
VIDIOC\_SUBSCRIBE\_EVENT
VIDIOC\_UNSUBSCRIBE\_EVENT
Subscribe or unsubscribe event
int
ioctl
int
fd
int
request
struct v4l2\_event\_subscription \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_SUBSCRIBE\_EVENT, VIDIOC\_UNSUBSCRIBE\_EVENT

``argp``

Description
===========

Subscribe or unsubscribe V4L2 event. Subscribed events are dequeued by
using the VIDIOC-DQEVENT ioctl.

+-----------+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | type          | Type of the event, see ?. Note that ``V4L2_EVENT_ALL`` can be used with VIDIOC\_UNSUBSCRIBE\_EVENT for unsubscribing all events at once.                         |
+-----------+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | id            | ID of the event source. If there is no ID associated with the event source, then set this to 0. Whether or not an event needs an ID depends on the event type.   |
+-----------+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | flags         | Event flags, see ?.                                                                                                                                              |
+-----------+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | reserved[5]   | Reserved for future extensions. Drivers and applications must set the array to zero.                                                                             |
+-----------+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_event\_subscription

+----------------------------------------+----------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_EVENT_SUB_FL_SEND_INITIAL``     | 0x0001   | When this event is subscribed an initial event will be sent containing the current status. This only makes sense for events that are triggered by a status change such as ``V4L2_EVENT_CTRL``. Other events will ignore this flag.                                                                                                                                                                                                                                      |
+----------------------------------------+----------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_EVENT_SUB_FL_ALLOW_FEEDBACK``   | 0x0002   | If set, then events directly caused by an ioctl will also be sent to the filehandle that called that ioctl. For example, changing a control using VIDIOC-S-CTRL will cause a V4L2\_EVENT\_CTRL to be sent back to that same filehandle. Normally such events are suppressed to prevent feedback loops where an application changes a control to a one value and then another, and then receives an event telling it that that control has changed to the first value.   |
|                                        |          |                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
|                                        |          | Since it can't tell whether that event was caused by another application or by the VIDIOC-S-CTRL call it is hard to decide whether to set the control to the value in the event, or ignore it.                                                                                                                                                                                                                                                                          |
|                                        |          |                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
|                                        |          | Think carefully when you set this flag so you won't get into situations like that.                                                                                                                                                                                                                                                                                                                                                                                      |
+----------------------------------------+----------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: Event Flags

RETURN-VALUE
