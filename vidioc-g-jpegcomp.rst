ioctl VIDIOC\_G\_JPEGCOMP, VIDIOC\_S\_JPEGCOMP
MANVOL
VIDIOC\_G\_JPEGCOMP
VIDIOC\_S\_JPEGCOMP
int
ioctl
int
fd
int
request
v4l2\_jpegcompression \*
argp
int
ioctl
int
fd
int
request
const v4l2\_jpegcompression \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_JPEGCOMP, VIDIOC\_S\_JPEGCOMP

``argp``

Description
===========

These ioctls are **deprecated**. New drivers and applications should use
`JPEG class controls <#jpeg-controls>`__ for image quality and JPEG
markers control.

[to do]

Ronald Bultje elaborates:

APP is some application-specific information. The application can set it
itself, and it'll be stored in the JPEG-encoded fields (eg; interlacing
information for in an AVI or so). COM is the same, but it's comments,
like 'encoded by me' or so.

jpeg\_markers describes whether the huffman tables, quantization tables
and the restart interval information (all JPEG-specific stuff) should be
stored in the JPEG-encoded fields. These define how the JPEG field is
encoded. If you omit them, applications assume you've used standard
encoding. You usually do want to add them.

+-----------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| int       | quality         | Deprecated. If ```                                                                                                                                                |
|           |                 |         V4L2_CID_JPEG_COMPRESSION_QUALITY`` <#jpeg-quality-control>`__ control is exposed by a driver applications should use it instead and ignore this field.   |
+-----------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| int       | APPn            |                                                                                                                                                                   |
+-----------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| int       | APP\_len        |                                                                                                                                                                   |
+-----------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| char      | APP\_data[60]   |                                                                                                                                                                   |
+-----------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| int       | COM\_len        |                                                                                                                                                                   |
+-----------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| char      | COM\_data[60]   |                                                                                                                                                                   |
+-----------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | jpeg\_markers   | See ?. Deprecated. If ```                                                                                                                                         |
|           |                 |         V4L2_CID_JPEG_ACTIVE_MARKER`` <#jpeg-active-marker-control>`__ control is exposed by a driver applications should use it instead and ignore this field.   |
+-----------+-----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_jpegcompression

+----------------------------+----------+--------------------------------------------+
| ``V4L2_JPEG_MARKER_DHT``   | (1<<3)   | Define Huffman Tables                      |
+----------------------------+----------+--------------------------------------------+
| ``V4L2_JPEG_MARKER_DQT``   | (1<<4)   | Define Quantization Tables                 |
+----------------------------+----------+--------------------------------------------+
| ``V4L2_JPEG_MARKER_DRI``   | (1<<5)   | Define Restart Interval                    |
+----------------------------+----------+--------------------------------------------+
| ``V4L2_JPEG_MARKER_COM``   | (1<<6)   | Comment segment                            |
+----------------------------+----------+--------------------------------------------+
| ``V4L2_JPEG_MARKER_APP``   | (1<<7)   | App segment, driver will always use APP0   |
+----------------------------+----------+--------------------------------------------+

Table: JPEG Markers Flags

RETURN-VALUE
