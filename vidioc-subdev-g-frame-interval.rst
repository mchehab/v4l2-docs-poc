ioctl VIDIOC\_SUBDEV\_G\_FRAME\_INTERVAL,
VIDIOC\_SUBDEV\_S\_FRAME\_INTERVAL
MANVOL
VIDIOC\_SUBDEV\_G\_FRAME\_INTERVAL
VIDIOC\_SUBDEV\_S\_FRAME\_INTERVAL
Get or set the frame interval on a subdev pad
int
ioctl
int
fd
int
request
struct v4l2\_subdev\_frame\_interval \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_SUBDEV\_G\_FRAME\_INTERVAL,
    VIDIOC\_SUBDEV\_S\_FRAME\_INTERVAL

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

These ioctls are used to get and set the frame interval at specific
subdev pads in the image pipeline. The frame interval only makes sense
for sub-devices that can control the frame period on their own. This
includes, for instance, image sensors and TV tuners. Sub-devices that
don't support frame intervals must not implement these ioctls.

To retrieve the current frame interval applications set the pad field of
a V4L2-SUBDEV-FRAME-INTERVAL to the desired pad number as reported by
the media controller API. When they call the
``VIDIOC_SUBDEV_G_FRAME_INTERVAL`` ioctl with a pointer to this
structure the driver fills the members of the interval field.

To change the current frame interval applications set both the pad field
and all members of the interval field. When they call the
``VIDIOC_SUBDEV_S_FRAME_INTERVAL`` ioctl with a pointer to this
structure the driver verifies the requested interval, adjusts it based
on the hardware capabilities and configures the device. Upon return the
V4L2-SUBDEV-FRAME-INTERVAL contains the current frame interval as would
be returned by a ``VIDIOC_SUBDEV_G_FRAME_INTERVAL`` call.

Drivers must not return an error solely because the requested interval
doesn't match the device capabilities. They must instead modify the
interval to match what the hardware can provide. The modified interval
should be as close as possible to the original request.

Sub-devices that support the frame interval ioctls should implement them
on a single pad only. Their behaviour when supported on multiple pads of
the same sub-device is not defined.

+--------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32      | pad           | Pad number as reported by the media controller API.                                    |
+--------------+---------------+----------------------------------------------------------------------------------------+
| V4L2-FRACT   | interval      | Period, in seconds, between consecutive video frames.                                  |
+--------------+---------------+----------------------------------------------------------------------------------------+
| \_\_u32      | reserved[9]   | Reserved for future extensions. Applications and drivers must set the array to zero.   |
+--------------+---------------+----------------------------------------------------------------------------------------+

Table: struct v4l2\_subdev\_frame\_interval

RETURN-VALUE

EBUSY
    The frame interval can't be changed because the pad is currently
    busy. This can be caused, for instance, by an active video stream on
    the pad. The ioctl must not be retried without performing another
    action to fix the problem first. Only returned by
    ``VIDIOC_SUBDEV_S_FRAME_INTERVAL``

EINVAL
    The V4L2-SUBDEV-FRAME-INTERVAL pad references a non-existing pad, or
    the pad doesn't support frame intervals.
