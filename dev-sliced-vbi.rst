VBI stands for Vertical Blanking Interval, a gap in the sequence of
lines of an analog video signal. During VBI no picture information is
transmitted, allowing some time while the electron beam of a cathode ray
tube TV returns to the top of the screen.

Sliced VBI devices use hardware to demodulate data transmitted in the
VBI. V4L2 drivers shall *not* do this by software, see also the `raw VBI
interface <#raw-vbi>`__. The data is passed as short packets of fixed
size, covering one scan line each. The number of packets per video frame
is variable.

Sliced VBI capture and output devices are accessed through the same
character special files as raw VBI devices. When a driver supports both
interfaces, the default function of a ``/dev/vbi`` device is *raw* VBI
capturing or output, and the sliced VBI function is only available after
calling the VIDIOC-S-FMT ioctl as defined below. Likewise a
``/dev/video`` device may support the sliced VBI API, however the
default function here is video capturing or output. Different file
descriptors must be used to pass raw and sliced VBI data simultaneously,
if this is supported by the driver.

Querying Capabilities
=====================

Devices supporting the sliced VBI capturing or output API set the
``V4L2_CAP_SLICED_VBI_CAPTURE`` or ``V4L2_CAP_SLICED_VBI_OUTPUT`` flag
respectively, in the capabilities field of V4L2-CAPABILITY returned by
the VIDIOC-QUERYCAP ioctl. At least one of the read/write, streaming or
asynchronous `I/O methods <#io>`__ must be supported. Sliced VBI devices
may have a tuner or modulator.

Supplemental Functions
======================

Sliced VBI devices shall support `video input or output <#video>`__ and
`tuner or modulator <#tuner>`__ ioctls if they have these capabilities,
and they may support `control <#control>`__ ioctls. The `video
standard <#standard>`__ ioctls provide information vital to program a
sliced VBI device, therefore must be supported.

Sliced VBI Format Negotiation
=============================

To find out which data services are supported by the hardware
applications can call the VIDIOC-G-SLICED-VBI-CAP ioctl. All drivers
implementing the sliced VBI interface must support this ioctl. The
results may differ from those of the VIDIOC-S-FMT ioctl when the number
of VBI lines the hardware can capture or output per frame, or the number
of services it can identify on a given line are limited. For example on
PAL line 16 the hardware may be able to look for a VPS or Teletext
signal, but not both at the same time.

To determine the currently selected services applications set the type
field of V4L2-FORMAT to `` V4L2_BUF_TYPE_SLICED_VBI_CAPTURE`` or ``
V4L2_BUF_TYPE_SLICED_VBI_OUTPUT``, and the VIDIOC-G-FMT ioctl fills the
fmt.sliced member, a V4L2-SLICED-VBI-FORMAT.

Applications can request different parameters by initializing or
modifying the fmt.sliced member and calling the VIDIOC-S-FMT ioctl with
a pointer to the v4l2\_format structure.

The sliced VBI API is more complicated than the raw VBI API because the
hardware must be told which VBI service to expect on each scan line. Not
all services may be supported by the hardware on all lines (this is
especially true for VBI output where Teletext is often unsupported and
other services can only be inserted in one specific line). In many
cases, however, it is sufficient to just set the service\_set field to
the required services and let the driver fill the service\_lines array
according to hardware capabilities. Only if more precise control is
needed should the programmer set the service\_lines array explicitly.

The VIDIOC-S-FMT ioctl modifies the parameters according to hardware
capabilities. When the driver allocates resources at this point, it may
return an EBUSY if the required resources are temporarily unavailable.
Other resource allocation points which may return EBUSY can be the
VIDIOC-STREAMON ioctl and the first FUNC-READ, FUNC-WRITE and
FUNC-SELECT call.

+--------------------+--------------------+--------------+--------------+--------------+
| \_\_u32            | service\_set       | If           |
|                    |                    | service\_set |
|                    |                    | is non-zero  |
|                    |                    | when passed  |
|                    |                    | with         |
|                    |                    | VIDIOC-S-FMT |
|                    |                    | or           |
|                    |                    | VIDIOC-TRY-F |
|                    |                    | MT,          |
|                    |                    | the          |
|                    |                    | service\_lin |
|                    |                    | es           |
|                    |                    | array will   |
|                    |                    | be filled by |
|                    |                    | the driver   |
|                    |                    | according to |
|                    |                    | the services |
|                    |                    | specified in |
|                    |                    | this field.  |
|                    |                    | For example, |
|                    |                    | if           |
|                    |                    | service\_set |
|                    |                    | is           |
|                    |                    | initialized  |
|                    |                    | with         |
|                    |                    | ``V4L2_SLICE |
|                    |                    | D_TELETEXT_B |
|                    |                    |  | V4L2_SLIC |
|                    |                    | ED_WSS_625`` |
|                    |                    | ,            |
|                    |                    | a driver for |
|                    |                    | the cx25840  |
|                    |                    | video        |
|                    |                    | decoder sets |
|                    |                    | lines 7-22   |
|                    |                    | of both      |
|                    |                    | fields [1]_  |
|                    |                    | to           |
|                    |                    | ``V4L2_SLICE |
|                    |                    | D_TELETEXT_B |
|                    |                    | ``           |
|                    |                    | and line 23  |
|                    |                    | of the first |
|                    |                    | field to     |
|                    |                    | ``V4L2_SLICE |
|                    |                    | D_WSS_625``. |
|                    |                    | If           |
|                    |                    | service\_set |
|                    |                    | is set to    |
|                    |                    | zero, then   |
|                    |                    | the values   |
|                    |                    | of           |
|                    |                    | service\_lin |
|                    |                    | es           |
|                    |                    | will be used |
|                    |                    | instead.     |
|                    |                    |              |
|                    |                    | On return    |
|                    |                    | the driver   |
|                    |                    | sets this    |
|                    |                    | field to the |
|                    |                    | union of all |
|                    |                    | elements of  |
|                    |                    | the returned |
|                    |                    | service\_lin |
|                    |                    | es           |
|                    |                    | array. It    |
|                    |                    | may contain  |
|                    |                    | less         |
|                    |                    | services     |
|                    |                    | than         |
|                    |                    | requested,   |
|                    |                    | perhaps just |
|                    |                    | one, if the  |
|                    |                    | hardware     |
|                    |                    | cannot       |
|                    |                    | handle more  |
|                    |                    | services     |
|                    |                    | simultaneous |
|                    |                    | ly.          |
|                    |                    | It may be    |
|                    |                    | empty (zero) |
|                    |                    | if none of   |
|                    |                    | the          |
|                    |                    | requested    |
|                    |                    | services are |
|                    |                    | supported by |
|                    |                    | the          |
|                    |                    | hardware.    |
+--------------------+--------------------+--------------+--------------+--------------+
| \_\_u16            | service\_lines[2][ | Applications |
|                    | 24]                | initialize   |
|                    |                    | this array   |
|                    |                    | with sets of |
|                    |                    | data         |
|                    |                    | services the |
|                    |                    | driver shall |
|                    |                    | look for or  |
|                    |                    | insert on    |
|                    |                    | the          |
|                    |                    | respective   |
|                    |                    | scan line.   |
|                    |                    | Subject to   |
|                    |                    | hardware     |
|                    |                    | capabilities |
|                    |                    | drivers      |
|                    |                    | return the   |
|                    |                    | requested    |
|                    |                    | set, a       |
|                    |                    | subset,      |
|                    |                    | which may be |
|                    |                    | just a       |
|                    |                    | single       |
|                    |                    | service, or  |
|                    |                    | an empty     |
|                    |                    | set. When    |
|                    |                    | the hardware |
|                    |                    | cannot       |
|                    |                    | handle       |
|                    |                    | multiple     |
|                    |                    | services on  |
|                    |                    | the same     |
|                    |                    | line the     |
|                    |                    | driver shall |
|                    |                    | choose one.  |
|                    |                    | No           |
|                    |                    | assumptions  |
|                    |                    | can be made  |
|                    |                    | on which     |
|                    |                    | service the  |
|                    |                    | driver       |
|                    |                    | chooses.     |
|                    |                    |              |
|                    |                    | Data         |
|                    |                    | services are |
|                    |                    | defined in   |
|                    |                    | ?. Array     |
|                    |                    | indices map  |
|                    |                    | to ITU-R     |
|                    |                    | line numbers |
|                    |                    | (see also ?  |
|                    |                    | and ?) as    |
|                    |                    | follows:     |
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | Element      | 525 line     | 625 line     |
|                    |                    |              | systems      | systems      |
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | service\_lin | 1            | 1            |
|                    |                    | es[0][1]     |              |              |
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | service\_lin | 23           | 23           |
|                    |                    | es[0][23]    |              |              |
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | service\_lin | 264          | 314          |
|                    |                    | es[1][1]     |              |              |
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | service\_lin | 286          | 336          |
|                    |                    | es[1][23]    |              |              |
+--------------------+--------------------+--------------+--------------+--------------+
|                    |                    | Drivers must |
|                    |                    | set          |
|                    |                    | service\_lin |
|                    |                    | es[0][0]     |
|                    |                    | and          |
|                    |                    | service\_lin |
|                    |                    | es[1][0]     |
|                    |                    | to zero. The |
|                    |                    | ``V4L2_VBI_I |
|                    |                    | TU_525_F1_ST |
|                    |                    | ART``,       |
|                    |                    | ``V4L2_VBI_I |
|                    |                    | TU_525_F2_ST |
|                    |                    | ART``,       |
|                    |                    | ``V4L2_VBI_I |
|                    |                    | TU_625_F1_ST |
|                    |                    | ART``        |
|                    |                    | and          |
|                    |                    | ``V4L2_VBI_I |
|                    |                    | TU_625_F2_ST |
|                    |                    | ART``        |
|                    |                    | defines give |
|                    |                    | the start    |
|                    |                    | line numbers |
|                    |                    | for each     |
|                    |                    | field for    |
|                    |                    | each 525 or  |
|                    |                    | 625 line     |
|                    |                    | format as a  |
|                    |                    | convenience. |
|                    |                    | Don't forget |
|                    |                    | that ITU     |
|                    |                    | line         |
|                    |                    | numbering    |
|                    |                    | starts at 1, |
|                    |                    | not 0.       |
+--------------------+--------------------+--------------+--------------+--------------+
| \_\_u32            | io\_size           | Maximum      |
|                    |                    | number of    |
|                    |                    | bytes passed |
|                    |                    | by one       |
|                    |                    | FUNC-READ or |
|                    |                    | FUNC-WRITE   |
|                    |                    | call, and    |
|                    |                    | the buffer   |
|                    |                    | size in      |
|                    |                    | bytes for    |
|                    |                    | the          |
|                    |                    | VIDIOC-QBUF  |
|                    |                    | and          |
|                    |                    | VIDIOC-DQBUF |
|                    |                    | ioctl.       |
|                    |                    | Drivers set  |
|                    |                    | this field   |
|                    |                    | to the size  |
|                    |                    | of           |
|                    |                    | V4L2-SLICED- |
|                    |                    | VBI-DATA     |
|                    |                    | times the    |
|                    |                    | number of    |
|                    |                    | non-zero     |
|                    |                    | elements in  |
|                    |                    | the returned |
|                    |                    | service\_lin |
|                    |                    | es           |
|                    |                    | array (that  |
|                    |                    | is the       |
|                    |                    | number of    |
|                    |                    | lines        |
|                    |                    | potentially  |
|                    |                    | carrying     |
|                    |                    | data).       |
+--------------------+--------------------+--------------+--------------+--------------+
| \_\_u32            | reserved[2]        | This array   |
|                    |                    | is reserved  |
|                    |                    | for future   |
|                    |                    | extensions.  |
|                    |                    | Applications |
|                    |                    | and drivers  |
|                    |                    | must set it  |
|                    |                    | to zero.     |
+--------------------+--------------------+--------------+--------------+--------------+

Table: struct v4l2\_sliced\_vbi\_format

+--------------------+-----------+-----------+--------------------+--------------------+
| Symbol             | Value     | Reference | Lines, usually     | Payload            |
+====================+===========+===========+====================+====================+
| ``V4L2_SLICED_TELE | 0x0001    | ?, ?      | PAL/SECAM line     | Last 42 of the 45  |
| TEXT_B``           |           |           | 7-22, 320-335      | byte Teletext      |
| (Teletext System   |           |           | (second field      | packet, that is    |
| B)                 |           |           | 7-22)              | without clock      |
|                    |           |           |                    | run-in and framing |
|                    |           |           |                    | code, lsb first    |
|                    |           |           |                    | transmitted.       |
+--------------------+-----------+-----------+--------------------+--------------------+
| ``V4L2_SLICED_VPS` | 0x0400    | ?         | PAL line 16        | Byte number 3 to   |
| `                  |           |           |                    | 15 according to    |
|                    |           |           |                    | Figure 9 of        |
|                    |           |           |                    | ETS 300 231, lsb   |
|                    |           |           |                    | first transmitted. |
+--------------------+-----------+-----------+--------------------+--------------------+
| ``V4L2_SLICED_CAPT | 0x1000    | ?         | NTSC line 21, 284  | Two bytes in       |
| ION_525``          |           |           | (second field 21)  | transmission       |
|                    |           |           |                    | order, including   |
|                    |           |           |                    | parity bit, lsb    |
|                    |           |           |                    | first transmitted. |
+--------------------+-----------+-----------+--------------------+--------------------+
| ``V4L2_SLICED_WSS_ | 0x4000    | ?, ?      | PAL/SECAM line 23  | ::                 |
| 625``              |           |           |                    |                    |
|                    |           |           |                    |     Byte         0 |
|                    |           |           |                    |                  1 |
|                    |           |           |                    |           msb      |
|                    |           |           |                    |     lsb  msb       |
|                    |           |           |                    |      lsb           |
|                    |           |           |                    |      Bit  7 6 5 4  |
|                    |           |           |                    | 3 2 1 0  x x 13 12 |
|                    |           |           |                    |  11 10 9           |
+--------------------+-----------+-----------+--------------------+--------------------+
| ``V4L2_SLICED_VBI_ | 0x1000    | Set of    |
| 525``              |           | services  |
|                    |           | applicabl |
|                    |           | e         |
|                    |           | to 525    |
|                    |           | line      |
|                    |           | systems.  |
+--------------------+-----------+-----------+--------------------+--------------------+
| ``V4L2_SLICED_VBI_ | 0x4401    | Set of    |
| 625``              |           | services  |
|                    |           | applicabl |
|                    |           | e         |
|                    |           | to 625    |
|                    |           | line      |
|                    |           | systems.  |
+--------------------+-----------+-----------+--------------------+--------------------+

Table: Sliced VBI services

Drivers may return an EINVAL when applications attempt to read or write
data without prior format negotiation, after switching the video
standard (which may invalidate the negotiated VBI parameters) and after
switching the video input (which may change the video standard as a side
effect). The VIDIOC-S-FMT ioctl may return an EBUSY when applications
attempt to change the format while i/o is in progress (between a
VIDIOC-STREAMON and VIDIOC-STREAMOFF call, and after the first FUNC-READ
or FUNC-WRITE call).

Reading and writing sliced VBI data
===================================

A single FUNC-READ or FUNC-WRITE call must pass all data belonging to
one video frame. That is an array of v4l2\_sliced\_vbi\_data structures
with one or more elements and a total size not exceeding io\_size bytes.
Likewise in streaming I/O mode one buffer of io\_size bytes must contain
data of one video frame. The id of unused v4l2\_sliced\_vbi\_data
elements must be zero.

+-----------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | id         | A flag from ? identifying the type of data in this packet. Only a single bit must be set. When the id of a captured packet is zero, the packet is empty and the contents of other fields are undefined. Applications shall ignore empty packets. When the id of a packet for output is zero the contents of the data field are undefined and the driver must no longer insert data on the requested field and line.   |
+-----------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | field      | The video field number this data has been captured from, or shall be inserted at. ``0`` for the first field, ``1`` for the second field.                                                                                                                                                                                                                                                                              |
+-----------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | line       | The field (as opposed to frame) line number this data has been captured from, or shall be inserted at. See ? and ? for valid values. Sliced VBI capture devices can set the line number of all packets to ``0`` if the hardware cannot reliably identify scan lines. The field number must always be valid.                                                                                                           |
+-----------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | reserved   | This field is reserved for future extensions. Applications and drivers must set it to zero.                                                                                                                                                                                                                                                                                                                           |
+-----------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u8    | data[48]   | The packet payload. See ? for the contents and number of bytes passed for each data type. The contents of padding bytes at the end of this array are undefined, drivers and applications shall ignore them.                                                                                                                                                                                                           |
+-----------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_sliced\_vbi\_data

Packets are always passed in ascending line number order, without
duplicate line numbers. The FUNC-WRITE function and the VIDIOC-QBUF
ioctl must return an EINVAL when applications violate this rule. They
must also return an EINVAL when applications pass an incorrect field or
line number, or a combination of field, line and id which has not been
negotiated with the VIDIOC-G-FMT or VIDIOC-S-FMT ioctl. When the line
numbers are unknown the driver must pass the packets in transmitted
order. The driver can insert empty packets with id set to zero anywhere
in the packet array.

To assure synchronization and to distinguish from frame dropping, when a
captured frame does not carry any of the requested data services drivers
must pass one or more empty packets. When an application fails to pass
VBI data in time for output, the driver must output the last VPS and WSS
packet again, and disable the output of Closed Caption and Teletext
data, or output data which is ignored by Closed Caption and Teletext
decoders.

A sliced VBI device may support `read/write <#rw>`__ and/or streaming
(`memory mapping <#mmap>`__ and/or `user pointer <#userp>`__) I/O. The
latter bears the possibility of synchronizing video and VBI data by
using buffer timestamps.

Sliced VBI Data in MPEG Streams
===============================

If a device can produce an MPEG output stream, it may be capable of
providing `negotiated sliced VBI
services <#sliced-vbi-format-negotitation>`__ as data embedded in the
MPEG stream. Users or applications control this sliced VBI data
insertion with the
`V4L2\_CID\_MPEG\_STREAM\_VBI\_FMT <#v4l2-mpeg-stream-vbi-fmt>`__
control.

If the driver does not provide the
`V4L2\_CID\_MPEG\_STREAM\_VBI\_FMT <#v4l2-mpeg-stream-vbi-fmt>`__
control, or only allows that control to be set to ```
V4L2_MPEG_STREAM_VBI_FMT_NONE`` <#v4l2-mpeg-stream-vbi-fmt>`__, then the
device cannot embed sliced VBI data in the MPEG stream.

The `V4L2\_CID\_MPEG\_STREAM\_VBI\_FMT <#v4l2-mpeg-stream-vbi-fmt>`__
control does not implicitly set the device driver to capture nor cease
capturing sliced VBI data. The control only indicates to embed sliced
VBI data in the MPEG stream, if an application has negotiated sliced VBI
service be captured.

It may also be the case that a device can embed sliced VBI data in only
certain types of MPEG streams: for example in an MPEG-2 PS but not an
MPEG-2 TS. In this situation, if sliced VBI data insertion is requested,
the sliced VBI data will be embedded in MPEG stream types when
supported, and silently omitted from MPEG stream types where sliced VBI
data insertion is not supported by the device.

The following subsections specify the format of the embedded sliced VBI
data.

MPEG Stream Embedded, Sliced VBI Data Format: NONE
--------------------------------------------------

The ```
V4L2_MPEG_STREAM_VBI_FMT_NONE`` <#v4l2-mpeg-stream-vbi-fmt>`__ embedded
sliced VBI format shall be interpreted by drivers as a control to cease
embedding sliced VBI data in MPEG streams. Neither the device nor driver
shall insert "empty" embedded sliced VBI data packets in the MPEG stream
when this format is set. No MPEG stream data structures are specified
for this format.

MPEG Stream Embedded, Sliced VBI Data Format: IVTV
--------------------------------------------------

The ```
V4L2_MPEG_STREAM_VBI_FMT_IVTV`` <#v4l2-mpeg-stream-vbi-fmt>`__ embedded
sliced VBI format, when supported, indicates to the driver to embed up
to 36 lines of sliced VBI data per frame in an MPEG-2 *Private Stream 1
PES* packet encapsulated in an MPEG-2 *Program Pack* in the MPEG stream.

*Historical context*: This format specification originates from a
custom, embedded, sliced VBI data format used by the ``ivtv`` driver.
This format has already been informally specified in the kernel sources
in the file ``Documentation/video4linux/cx2341x/README.vbi`` . The
maximum size of the payload and other aspects of this format are driven
by the CX23415 MPEG decoder's capabilities and limitations with respect
to extracting, decoding, and displaying sliced VBI data embedded within
an MPEG stream.

This format's use is *not* exclusive to the ``ivtv`` driver *nor*
exclusive to CX2341x devices, as the sliced VBI data packet insertion
into the MPEG stream is implemented in driver software. At least the
``cx18`` driver provides sliced VBI data insertion into an MPEG-2 PS in
this format as well.

The following definitions specify the payload of the MPEG-2 *Private
Stream 1 PES* packets that contain sliced VBI data when
```V4L2_MPEG_STREAM_VBI_FMT_IVTV`` <#v4l2-mpeg-stream-vbi-fmt>`__ is
set. (The MPEG-2 *Private Stream 1 PES* packet header and encapsulating
MPEG-2 *Program Pack* header are not detailed here. Please refer to the
MPEG-2 specifications for details on those packet headers.)

The payload of the MPEG-2 *Private Stream 1 PES* packets that contain
sliced VBI data is specified by V4L2-MPEG-VBI-FMT-IVTV. The payload is
variable length, depending on the actual number of lines of sliced VBI
data present in a video frame. The payload may be padded at the end with
unspecified fill bytes to align the end of the payload to a 4-byte
boundary. The payload shall never exceed 1552 bytes (2 fields with 18
lines/field with 43 bytes of data/line and a 4 byte magic number).

+----------+------------------------------------------------------------+--------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u8   | magic[4]                                                   |        | A "magic" constant from ? that indicates this is a valid sliced VBI data payload and also indicates which member of the anonymous union, itv0 or ITV0, to use for the payload data.                          |
+----------+------------------------------------------------------------+--------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| union    | (anonymous)                                                |
+----------+------------------------------------------------------------+--------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|          | struct `v4l2\_mpeg\_vbi\_itv0 <#v4l2-mpeg-vbi-itv0>`__     | itv0   | The primary form of the sliced VBI data payload that contains anywhere from 1 to 35 lines of sliced VBI data. Line masks are provided in this form of the payload indicating which VBI lines are provided.   |
+----------+------------------------------------------------------------+--------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|          | struct `v4l2\_mpeg\_vbi\_ITV0 <#v4l2-mpeg-vbi-itv0-1>`__   | ITV0   | An alternate form of the sliced VBI data payload used when 36 lines of sliced VBI data are present. No line masks are provided in this form of the payload; all valid line mask bits are implcitly set.      |
+----------+------------------------------------------------------------+--------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_mpeg\_vbi\_fmt\_ivtv

+---------------------------------+----------+-------------------------------------------------------------------------------------------------------------------------------+
| Defined Symbol                  | Value    | Description                                                                                                                   |
+=================================+==========+===============================================================================================================================+
| ``V4L2_MPEG_VBI_IVTV_MAGIC0``   | "itv0"   | Indicates the itv0 member of the union in V4L2-MPEG-VBI-FMT-IVTV is valid.                                                    |
+---------------------------------+----------+-------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_MPEG_VBI_IVTV_MAGIC1``   | "ITV0"   | Indicates the ITV0 member of the union in V4L2-MPEG-VBI-FMT-IVTV is valid and that 36 lines of sliced VBI data are present.   |
+---------------------------------+----------+-------------------------------------------------------------------------------------------------------------------------------+

Table: Magic Constants for V4L2-MPEG-VBI-FMT-IVTV magic field

+---------------------------------------------------------------------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_le32                                                            | linemask[2]   | Bitmasks indicating the VBI service lines present. These linemask values are stored in little endian byte order in the MPEG stream. Some reference linemask bit positions with their corresponding VBI line number and video field are given below. b\ :sub:`0` indicates the least significant bit of a linemask value:                                                                                                                                                                                                                                                                                              |
|                                                                     |               |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
|                                                                     |               | ::                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|                                                                     |               |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
|                                                                     |               |     linemask[0] b0:       line  6     first field                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
|                                                                     |               |     linemask[0] b17:      line 23     first field                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
|                                                                     |               |     linemask[0] b18:      line  6     second field                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|                                                                     |               |     linemask[0] b31:      line 19     second field                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|                                                                     |               |     linemask[1] b0:       line 20     second field                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|                                                                     |               |     linemask[1] b3:       line 23     second field                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|                                                                     |               |     linemask[1] b4-b31:    unused and set to 0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
+---------------------------------------------------------------------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| struct `v4l2\_mpeg\_vbi\_itv0\_line <#v4l2-mpeg-vbi-itv0-line>`__   | line[35]      | This is a variable length array that holds from 1 to 35 lines of sliced VBI data. The sliced VBI data lines present correspond to the bits set in the linemask array, starting from b\ :sub:`0` of linemask[0] up through b\ :sub:`31` of linemask[0], and from b\ :sub:`0` of linemask[1] up through b :sub:`3` of linemask[1]. line[0] corresponds to the first bit found set in the linemask array, line[1] corresponds to the second bit found set in the linemask array, etc. If no linemask array bits are set, then line[0] may contain one line of unspecified data that should be ignored by applications.   |
+---------------------------------------------------------------------+---------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_mpeg\_vbi\_itv0

+---------------------------------------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| struct `v4l2\_mpeg\_vbi\_itv0\_line <#v4l2-mpeg-vbi-itv0-line>`__   | line[36]   | A fixed length array of 36 lines of sliced VBI data. line[0] through line[17] correspond to lines 6 through 23 of the first field. line[18] through line[35] corresponds to lines 6 through 23 of the second field.   |
+---------------------------------------------------------------------+------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_mpeg\_vbi\_ITV0

+----------+------------+--------------------------------------------------------------------------------------------------+
| \_\_u8   | id         | A line identifier value from ? that indicates the type of sliced VBI data stored on this line.   |
+----------+------------+--------------------------------------------------------------------------------------------------+
| \_\_u8   | data[42]   | The sliced VBI data for the line.                                                                |
+----------+------------+--------------------------------------------------------------------------------------------------+

Table: struct v4l2\_mpeg\_vbi\_itv0\_line

+--------------------------------------+---------+--------------------------------------------------------------------------------------------+
| Defined Symbol                       | Value   | Description                                                                                |
+======================================+=========+============================================================================================+
| ``V4L2_MPEG_VBI_IVTV_TELETEXT_B``    | 1       | Refer to `Sliced VBI services <#vbi-services2>`__ for a description of the line payload.   |
+--------------------------------------+---------+--------------------------------------------------------------------------------------------+
| ``V4L2_MPEG_VBI_IVTV_CAPTION_525``   | 4       | Refer to `Sliced VBI services <#vbi-services2>`__ for a description of the line payload.   |
+--------------------------------------+---------+--------------------------------------------------------------------------------------------+
| ``V4L2_MPEG_VBI_IVTV_WSS_625``       | 5       | Refer to `Sliced VBI services <#vbi-services2>`__ for a description of the line payload.   |
+--------------------------------------+---------+--------------------------------------------------------------------------------------------+
| ``V4L2_MPEG_VBI_IVTV_VPS``           | 7       | Refer to `Sliced VBI services <#vbi-services2>`__ for a description of the line payload.   |
+--------------------------------------+---------+--------------------------------------------------------------------------------------------+

Table: Line Identifiers for struct
`v4l2\_mpeg\_vbi\_itv0\_line <#v4l2-mpeg-vbi-itv0-line>`__ id field

.. [1]
   According to `ETS 300 706 <#ets300706>`__ lines 6-22 of the first
   field and lines 5-22 of the second field may carry Teletext data.
