ioctl VIDIOC\_DBG\_G\_CHIP\_INFO
MANVOL
VIDIOC\_DBG\_G\_CHIP\_INFO
Identify the chips on a TV card
int
ioctl
int
fd
int
request
struct v4l2\_dbg\_chip\_info \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_DBG\_G\_CHIP\_INFO

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

For driver debugging purposes this ioctl allows test applications to
query the driver about the chips present on the TV card. Regular
applications must not use it. When you found a chip specific bug, please
contact the linux-media mailing list (V4L-ML) so it can be fixed.

Additionally the Linux kernel must be compiled with the
``CONFIG_VIDEO_ADV_DEBUG`` option to enable this ioctl.

To query the driver applications must initialize the match.type and
match.addr or match.name fields of a V4L2-DBG-CHIP-INFO and call
``VIDIOC_DBG_G_CHIP_INFO`` with a pointer to this structure. On success
the driver stores information about the selected chip in the name and
flags fields.

When match.type is ``V4L2_CHIP_MATCH_BRIDGE``, match.addr selects the
nth bridge 'chip' on the TV card. You can enumerate all chips by
starting at zero and incrementing match.addr by one until
``VIDIOC_DBG_G_CHIP_INFO`` fails with an EINVAL. The number zero always
selects the bridge chip itself, ⪚ the chip connected to the PCI or USB
bus. Non-zero numbers identify specific parts of the bridge chip such as
an AC97 register block.

When match.type is ``V4L2_CHIP_MATCH_SUBDEV``, match.addr selects the
nth sub-device. This allows you to enumerate over all sub-devices.

On success, the name field will contain a chip name and the flags field
will contain ``V4L2_CHIP_FL_READABLE`` if the driver supports reading
registers from the device or ``V4L2_CHIP_FL_WRITABLE`` if the driver
supports writing registers to the device.

We recommended the v4l2-dbg utility over calling this ioctl directly. It
is available from the LinuxTV v4l-dvb repository; see
https://linuxtv.org/repo/ for access instructions.

+-----------+---------------+---------------------------------------+-----------------------------------------------------------------------------------------+
| \_\_u32   | type          | See ? for a list of possible types.   |
+-----------+---------------+---------------------------------------+-----------------------------------------------------------------------------------------+
| union     | (anonymous)   |
+-----------+---------------+---------------------------------------+-----------------------------------------------------------------------------------------+
|           | \_\_u32       | addr                                  | Match a chip by this number, interpreted according to the type field.                   |
+-----------+---------------+---------------------------------------+-----------------------------------------------------------------------------------------+
|           | char          | name[32]                              | Match a chip by this name, interpreted according to the type field. Currently unused.   |
+-----------+---------------+---------------------------------------+-----------------------------------------------------------------------------------------+

Table: struct v4l2\_dbg\_match

+---------------------------+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| struct v4l2\_dbg\_match   | match         | How to match the chip, see ?.                                                                                                                                                                  |
+---------------------------+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| char                      | name[32]      | The name of the chip.                                                                                                                                                                          |
+---------------------------+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32                   | flags         | Set by the driver. If ``V4L2_CHIP_FL_READABLE`` is set, then the driver supports reading registers from the device. If ``V4L2_CHIP_FL_WRITABLE`` is set, then it supports writing registers.   |
+---------------------------+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32                   | reserved[8]   | Reserved fields, both application and driver must set these to 0.                                                                                                                              |
+---------------------------+---------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_dbg\_chip\_info

+------------------------------+-----+-----------------------------------------------------------------------------------------+
| ``V4L2_CHIP_MATCH_BRIDGE``   | 0   | Match the nth chip on the card, zero for the bridge chip. Does not match sub-devices.   |
+------------------------------+-----+-----------------------------------------------------------------------------------------+
| ``V4L2_CHIP_MATCH_SUBDEV``   | 4   | Match the nth sub-device.                                                               |
+------------------------------+-----+-----------------------------------------------------------------------------------------+

Table: Chip Match Types

RETURN-VALUE

EINVAL
    The match\_type is invalid or no device could be matched.
