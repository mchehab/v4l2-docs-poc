ioctl FE\_SET\_PROPERTY, FE\_GET\_PROPERTY
MANVOL
FE\_SET\_PROPERTY
FE\_GET\_PROPERTY
FE\_SET\_PROPERTY sets one or more frontend properties.
FE\_GET\_PROPERTY returns one or more frontend properties.
int
ioctl
int
fd
int
request
struct dtv\_properties \*
argp
Arguments
=========

``fd``
    FE\_FD

``request``
    FE\_SET\_PROPERTY, FE\_GET\_PROPERTY

``argp``
    pointer to DTV-PROPERTIES

Description
===========

All DVB frontend devices support the ``FE_SET_PROPERTY`` and
``FE_GET_PROPERTY`` ioctls. The supported properties and statistics
depends on the delivery system and on the device:

-  ``FE_SET_PROPERTY:``

   -  This ioctl is used to set one or more frontend properties.

   -  This is the basic command to request the frontend to tune into
      some frequency and to start decoding the digital TV signal.

   -  This call requires read/write access to the device.

   -  At return, the values are updated to reflect the actual parameters
      used.

-  ``FE_GET_PROPERTY:``

   -  This ioctl is used to get properties and statistics from the
      frontend.

   -  No properties are changed, and statistics aren't reset.

   -  This call only requires read-only access to the device.

RETURN-VALUE-DVB
