ioctl VIDIOC\_G\_CTRL, VIDIOC\_S\_CTRL
MANVOL
VIDIOC\_G\_CTRL
VIDIOC\_S\_CTRL
Get or set the value of a control
int
ioctl
int
fd
int
request
struct v4l2\_control \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_CTRL, VIDIOC\_S\_CTRL

``argp``

Description
===========

To get the current value of a control applications initialize the id
field of a struct v4l2\_control and call the ``VIDIOC_G_CTRL`` ioctl
with a pointer to this structure. To change the value of a control
applications initialize the id and value fields of a struct
v4l2\_control and call the ``VIDIOC_S_CTRL`` ioctl.

When the id is invalid drivers return an EINVAL. When the value is out
of bounds drivers can choose to take the closest valid value or return
an ERANGE, whatever seems more appropriate. However, ``VIDIOC_S_CTRL``
is a write-only ioctl, it does not return the actual new value. If the
value is inappropriate for the control (e.g. if it refers to an
unsupported menu index of a menu control), then EINVAL is returned as
well.

These ioctls work only with user controls. For other control classes the
VIDIOC-G-EXT-CTRLS, VIDIOC-S-EXT-CTRLS or VIDIOC-TRY-EXT-CTRLS must be
used.

+-----------+---------+---------------------------------------------------+
| \_\_u32   | id      | Identifies the control, set by the application.   |
+-----------+---------+---------------------------------------------------+
| \_\_s32   | value   | New value or current value.                       |
+-----------+---------+---------------------------------------------------+

Table: struct v4l2\_control

RETURN-VALUE

EINVAL
    The V4L2-CONTROL id is invalid or the value is inappropriate for the
    given control (i.e. if a menu item is selected that is not supported
    by the driver according to VIDIOC-QUERYMENU).

ERANGE
    The V4L2-CONTROL value is out of bounds.

EBUSY
    The control is temporarily not changeable, possibly because another
    applications took over control of the device function this control
    belongs to.

EACCES
    Attempt to set a read-only control or to get a write-only control.
