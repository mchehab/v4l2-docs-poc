ioctl VIDIOC\_G\_ENC\_INDEX
MANVOL
VIDIOC\_G\_ENC\_INDEX
Get meta data about a compressed video stream
int
ioctl
int
fd
int
request
struct v4l2\_enc\_idx \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_ENC\_INDEX

``argp``

Description
===========

The ``VIDIOC_G_ENC_INDEX`` ioctl provides meta data about a compressed
video stream the same or another application currently reads from the
driver, which is useful for random access into the stream without
decoding it.

To read the data applications must call ``VIDIOC_G_ENC_INDEX`` with a
pointer to a V4L2-ENC-IDX. On success the driver fills the entry array,
stores the number of elements written in the entries field, and
initializes the entries\_cap field.

Each element of the entry array contains meta data about one picture. A
``VIDIOC_G_ENC_INDEX`` call reads up to ``V4L2_ENC_IDX_ENTRIES`` entries
from a driver buffer, which can hold up to entries\_cap entries. This
number can be lower or higher than ``V4L2_ENC_IDX_ENTRIES``, but not
zero. When the application fails to read the meta data in time the
oldest entries will be lost. When the buffer is empty or no
capturing/encoding is in progress, entries will be zero.

Currently this ioctl is only defined for MPEG-2 program streams and
video elementary streams.

+----------------------+-------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32              | entries                             | The number of entries the driver stored in the entry array.                                                                                   |
+----------------------+-------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32              | entries\_cap                        | The number of entries the driver can buffer. Must be greater than zero.                                                                       |
+----------------------+-------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32              | reserved[4]                         | Reserved for future extensions. Drivers must set the array to zero.                                                                           |
+----------------------+-------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------+
| V4L2-ENC-IDX-ENTRY   | entry[``V4L2_ENC_IDX_ENTRIES``\ ]   | Meta data about a compressed video stream. Each element of the array corresponds to one picture, sorted in ascending order by their offset.   |
+----------------------+-------------------------------------+-----------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_enc\_idx

+-----------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u64   | offset        | The offset in bytes from the beginning of the compressed video stream to the beginning of this picture, that is a *PES packet header* as defined in ? or a *picture header* as defined in ?. When the encoder is stopped, the driver resets the offset to zero.   |
+-----------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u64   | pts           | The 33 bit *Presentation Time Stamp* of this picture as defined in ?.                                                                                                                                                                                             |
+-----------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | length        | The length of this picture in bytes.                                                                                                                                                                                                                              |
+-----------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | flags         | Flags containing the coding type of this picture, see ?.                                                                                                                                                                                                          |
+-----------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | reserved[2]   | Reserved for future extensions. Drivers must set the array to zero.                                                                                                                                                                                               |
+-----------+---------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_enc\_idx\_entry

+-------------------------------+--------+---------------------------------------------------------------------------+
| ``V4L2_ENC_IDX_FRAME_I``      | 0x00   | This is an Intra-coded picture.                                           |
+-------------------------------+--------+---------------------------------------------------------------------------+
| ``V4L2_ENC_IDX_FRAME_P``      | 0x01   | This is a Predictive-coded picture.                                       |
+-------------------------------+--------+---------------------------------------------------------------------------+
| ``V4L2_ENC_IDX_FRAME_B``      | 0x02   | This is a Bidirectionally predictive-coded picture.                       |
+-------------------------------+--------+---------------------------------------------------------------------------+
| ``V4L2_ENC_IDX_FRAME_MASK``   | 0x0F   | *AND* the flags field with this mask to obtain the picture coding type.   |
+-------------------------------+--------+---------------------------------------------------------------------------+

Table: Index Entry Flags

RETURN-VALUE
