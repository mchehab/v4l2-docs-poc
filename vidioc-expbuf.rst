ioctl VIDIOC\_EXPBUF
MANVOL
VIDIOC\_EXPBUF
Export a buffer as a DMABUF file descriptor.
int
ioctl
int
fd
int
request
struct v4l2\_exportbuffer \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_EXPBUF

``argp``

Description
===========

    **Note**

    This is an `experimental <#experimental>`__ interface and may change
    in the future.

This ioctl is an extension to the `memory mapping <#mmap>`__ I/O method,
therefore it is available only for ``V4L2_MEMORY_MMAP`` buffers. It can
be used to export a buffer as a DMABUF file at any time after buffers
have been allocated with the VIDIOC-REQBUFS ioctl.

To export a buffer, applications fill V4L2-EXPORTBUFFER. The type field
is set to the same buffer type as was previously used with
V4L2-REQUESTBUFFERS type. Applications must also set the index field.
Valid index numbers range from zero to the number of buffers allocated
with VIDIOC-REQBUFS (V4L2-REQUESTBUFFERS count) minus one. For the
multi-planar API, applications set the plane field to the index of the
plane to be exported. Valid planes range from zero to the maximal number
of valid planes for the currently active format. For the single-planar
API, applications must set plane to zero. Additional flags may be posted
in the flags field. Refer to a manual for open() for details. Currently
only O\_CLOEXEC, O\_RDONLY, O\_WRONLY, and O\_RDWR are supported. All
other fields must be set to zero. In the case of multi-planar API, every
plane is exported separately using multiple ``VIDIOC_EXPBUF`` calls.

After calling ``VIDIOC_EXPBUF`` the fd field will be set by a driver.
This is a DMABUF file descriptor. The application may pass it to other
DMABUF-aware devices. Refer to `DMABUF importing <#dmabuf>`__ for
details about importing DMABUF files into V4L2 nodes. It is recommended
to close a DMABUF file when it is no longer used to allow the associated
memory to be reclaimed.

Examples
========

::

    int buffer_export(int v4lfd, V4L2-BUF-TYPE bt, int index, int *dmafd)
    {
        V4L2-EXPORTBUFFER expbuf;

        memset(&expbuf, 0, sizeof(expbuf));
        expbuf.type = bt;
        expbuf.index = index;
        if (ioctl(v4lfd, VIDIOC-EXPBUF, &expbuf) == -1) {
            perror("VIDIOC_EXPBUF");
            return -1;
        }

        *dmafd = expbuf.fd;

        return 0;
    }
          

::

    int buffer_export_mp(int v4lfd, V4L2-BUF-TYPE bt, int index,
        int dmafd[], int n_planes)
    {
        int i;

        for (i = 0; i < n_planes; ++i) {
            V4L2-EXPORTBUFFER expbuf;

            memset(&expbuf, 0, sizeof(expbuf));
            expbuf.type = bt;
            expbuf.index = index;
            expbuf.plane = i;
            if (ioctl(v4lfd, VIDIOC-EXPBUF, &expbuf) == -1) {
                perror("VIDIOC_EXPBUF");
                while (i)
                    close(dmafd[--i]);
                return -1;
            }
            dmafd[i] = expbuf.fd;
        }

        return 0;
    }
          

+-----------+----------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | type           | Type of the buffer, same as V4L2-FORMAT type or V4L2-REQUESTBUFFERS type, set by the application. See ?                                                                                                                         |
+-----------+----------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | index          | Number of the buffer, set by the application. This field is only used for `memory mapping <#mmap>`__ I/O and can range from zero to the number of buffers allocated with the VIDIOC-REQBUFS and/or VIDIOC-CREATE-BUFS ioctls.   |
+-----------+----------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | plane          | Index of the plane to be exported when using the multi-planar API. Otherwise this value must be set to zero.                                                                                                                    |
+-----------+----------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | flags          | Flags for the newly created file, currently only ``O_CLOEXEC``, ``O_RDONLY``, ``O_WRONLY``, and ``O_RDWR`` are supported, refer to the manual of open() for more details.                                                       |
+-----------+----------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_s32   | fd             | The DMABUF file descriptor associated with a buffer. Set by the driver.                                                                                                                                                         |
+-----------+----------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | reserved[11]   | Reserved field for future use. Drivers and applications must set the array to zero.                                                                                                                                             |
+-----------+----------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_exportbuffer

RETURN-VALUE

EINVAL
    A queue is not in MMAP mode or DMABUF exporting is not supported or
    flags or type or index or plane fields are invalid.
