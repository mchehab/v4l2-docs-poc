V4L2\_PIX\_FMT\_NV12M ('NM12'), V4L2\_PIX\_FMT\_NV21M ('NM21'),
V4L2\_PIX\_FMT\_NV12MT\_16X16
MANVOL
V4L2\_PIX\_FMT\_NV12M
V4L2\_PIX\_FMT\_NV21M
V4L2\_PIX\_FMT\_NV12MT\_16X16
Variation of
V4L2\_PIX\_FMT\_NV12
and
V4L2\_PIX\_FMT\_NV21
with planes non contiguous in memory.
Description
===========

This is a multi-planar, two-plane version of the YUV 4:2:0 format. The
three components are separated into two sub-images or planes.
``V4L2_PIX_FMT_NV12M`` differs from ``V4L2_PIX_FMT_NV12
`` in that the two planes are non-contiguous in memory, i.e. the chroma
plane do not necessarily immediately follows the luma plane. The
luminance data occupies the first plane. The Y plane has one byte per
pixel. In the second plane there is a chrominance data with alternating
chroma samples. The CbCr plane is the same width, in bytes, as the Y
plane (and of the image), but is half as tall in pixels. Each CbCr pair
belongs to four pixels. For example, Cb\ :sub:`0`/Cr:sub:`0` belongs to
Y'\ :sub:`00`, Y'\ :sub:`01`, Y'\ :sub:`10`, Y'\ :sub:`11`.
``V4L2_PIX_FMT_NV12MT_16X16`` is the tiled version of
``V4L2_PIX_FMT_NV12M`` with 16x16 macroblock tiles. Here pixels are
arranged in 16x16 2D tiles and tiles are arranged in linear order in
memory. ``V4L2_PIX_FMT_NV21M`` is the same as ``V4L2_PIX_FMT_NV12M``
except the Cb and Cr bytes are swapped, the CrCb plane starts with a Cr
byte.

``V4L2_PIX_FMT_NV12M`` is intended to be used only in drivers and
applications that support the multi-planar API, described in ?.

If the Y plane has pad bytes after each row, then the CbCr plane has as
many pad bytes after its rows.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start0 + 0:                                                              |
+--------------------------------------------------------------------------+
| start0 + 4:                                                              |
+--------------------------------------------------------------------------+
| start0 + 8:                                                              |
+--------------------------------------------------------------------------+
| start0 + 12:                                                             |
+--------------------------------------------------------------------------+
+--------------------------------------------------------------------------+
| start1 + 0:                                                              |
+--------------------------------------------------------------------------+
| start1 + 4:                                                              |
+--------------------------------------------------------------------------+

**Color Sample Location..**

+-----+-----+-----+-----+----+-----+-----+-----+
|     | 0   |     | 1   |    | 2   |     | 3   |
+-----+-----+-----+-----+----+-----+-----+-----+
| 0   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
|     |     | C   |     |    |     | C   |     |
+-----+-----+-----+-----+----+-----+-----+-----+
| 1   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
+-----+-----+-----+-----+----+-----+-----+-----+
| 2   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
|     |     | C   |     |    |     | C   |     |
+-----+-----+-----+-----+----+-----+-----+-----+
| 3   | Y   |     | Y   |    | Y   |     | Y   |
+-----+-----+-----+-----+----+-----+-----+-----+
