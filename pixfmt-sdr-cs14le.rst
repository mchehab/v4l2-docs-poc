V4L2\_SDR\_FMT\_CS14LE ('CS14')
MANVOL
V4L2\_SDR\_FMT\_CS14LE
Complex signed 14-bit little endian IQ sample
Description
===========

This format contains sequence of complex number samples. Each complex
number consist two parts, called In-phase and Quadrature (IQ). Both I
and Q are represented as a 14 bit signed little endian number. I value
comes first and Q value after that. 14 bit value is stored in 16 bit
space with unused high bits padded with 0.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 2:                                                               |
+--------------------------------------------------------------------------+
