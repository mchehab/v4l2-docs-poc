ioctl VIDIOC\_ENUM\_FRAMESIZES
MANVOL
VIDIOC\_ENUM\_FRAMESIZES
Enumerate frame sizes
int
ioctl
int
fd
int
request
struct v4l2\_frmsizeenum \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_ENUM\_FRAMESIZES

``argp``
    Pointer to a V4L2-FRMSIZEENUM that contains an index and pixel
    format and receives a frame width and height.

Description
===========

This ioctl allows applications to enumerate all frame sizes (IE width
and height in pixels) that the device supports for the given pixel
format.

The supported pixel formats can be obtained by using the VIDIOC-ENUM-FMT
function.

The return value and the content of the v4l2\_frmsizeenum.type field
depend on the type of frame sizes the device supports. Here are the
semantics of the function for the different cases:

-  **Discrete:** The function returns success if the given index value
   (zero-based) is valid. The application should increase the index by
   one for each call until ``EINVAL`` is returned. The
   v4l2\_frmsizeenum.type field is set to ``V4L2_FRMSIZE_TYPE_DISCRETE``
   by the driver. Of the union only the discrete member is valid.

-  **Step-wise:** The function returns success if the given index value
   is zero and ``EINVAL`` for any other index value. The
   v4l2\_frmsizeenum.type field is set to ``V4L2_FRMSIZE_TYPE_STEPWISE``
   by the driver. Of the union only the stepwise member is valid.

-  **Continuous:** This is a special case of the step-wise type above.
   The function returns success if the given index value is zero and
   ``EINVAL`` for any other index value. The v4l2\_frmsizeenum.type
   field is set to ``V4L2_FRMSIZE_TYPE_CONTINUOUS`` by the driver. Of
   the union only the stepwise member is valid and the step\_width and
   step\_height values are set to 1.

When the application calls the function with index zero, it must check
the type field to determine the type of frame size enumeration the
device supports. Only for the ``V4L2_FRMSIZE_TYPE_DISCRETE`` type does
it make sense to increase the index value to receive more frame sizes.

Note that the order in which the frame sizes are returned has no special
meaning. In particular does it not say anything about potential default
format sizes.

Applications can assume that the enumeration data does not change
without any interaction from the application itself. This means that the
enumeration data is consistent if the application does not perform any
other ioctl calls while it runs the frame size enumeration.

Structs
=======

In the structs below, *IN* denotes a value that has to be filled in by
the application, *OUT* denotes values that the driver fills in. The
application should zero out all members except for the *IN* fields.

+-----------+----------+--------------------------------+
| \_\_u32   | width    | Width of the frame [pixel].    |
+-----------+----------+--------------------------------+
| \_\_u32   | height   | Height of the frame [pixel].   |
+-----------+----------+--------------------------------+

Table: struct v4l2\_frmsize\_discrete

+-----------+----------------+-----------------------------------+
| \_\_u32   | min\_width     | Minimum frame width [pixel].      |
+-----------+----------------+-----------------------------------+
| \_\_u32   | max\_width     | Maximum frame width [pixel].      |
+-----------+----------------+-----------------------------------+
| \_\_u32   | step\_width    | Frame width step size [pixel].    |
+-----------+----------------+-----------------------------------+
| \_\_u32   | min\_height    | Minimum frame height [pixel].     |
+-----------+----------------+-----------------------------------+
| \_\_u32   | max\_height    | Maximum frame height [pixel].     |
+-----------+----------------+-----------------------------------+
| \_\_u32   | step\_height   | Frame height step size [pixel].   |
+-----------+----------------+-----------------------------------+

Table: struct v4l2\_frmsize\_stepwise

+-----------+-------------------------+------------+------------------------------------------------------------------------------+
| \_\_u32   | index                   |            | IN: Index of the given frame size in the enumeration.                        |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
| \_\_u32   | pixel\_format           |            | IN: Pixel format for which the frame sizes are enumerated.                   |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
| \_\_u32   | type                    |            | OUT: Frame size type the device supports.                                    |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
| union     |                         |            | OUT: Frame size with the given index.                                        |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
|           | V4L2-FRMSIZE-DISCRETE   | discrete   |                                                                              |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
|           | V4L2-FRMSIZE-STEPWISE   | stepwise   |                                                                              |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+
| \_\_u32   | reserved[2]             |            | Reserved space for future use. Must be zeroed by drivers and applications.   |
+-----------+-------------------------+------------+------------------------------------------------------------------------------+

Table: struct v4l2\_frmsizeenum

Enums
=====

+------------------------------------+-----+---------------------------------+
| ``V4L2_FRMSIZE_TYPE_DISCRETE``     | 1   | Discrete frame size.            |
+------------------------------------+-----+---------------------------------+
| ``V4L2_FRMSIZE_TYPE_CONTINUOUS``   | 2   | Continuous frame size.          |
+------------------------------------+-----+---------------------------------+
| ``V4L2_FRMSIZE_TYPE_STEPWISE``     | 3   | Step-wise defined frame size.   |
+------------------------------------+-----+---------------------------------+

Table: enum v4l2\_frmsizetypes

RETURN-VALUE
