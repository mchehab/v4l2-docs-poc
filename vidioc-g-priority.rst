ioctl VIDIOC\_G\_PRIORITY, VIDIOC\_S\_PRIORITY
MANVOL
VIDIOC\_G\_PRIORITY
VIDIOC\_S\_PRIORITY
Query or request the access priority associated with a file descriptor
int
ioctl
int
fd
int
request
enum v4l2\_priority \*
argp
int
ioctl
int
fd
int
request
const enum v4l2\_priority \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_PRIORITY, VIDIOC\_S\_PRIORITY

``argp``
    Pointer to an enum v4l2\_priority type.

Description
===========

To query the current access priority applications call the
``VIDIOC_G_PRIORITY`` ioctl with a pointer to an enum v4l2\_priority
variable where the driver stores the current priority.

To request an access priority applications store the desired priority in
an enum v4l2\_priority variable and call ``VIDIOC_S_PRIORITY`` ioctl
with a pointer to this variable.

+---------------------------------+-----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_PRIORITY_UNSET``         | 0   |                                                                                                                                                                                                                                                                                 |
+---------------------------------+-----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_PRIORITY_BACKGROUND``    | 1   | Lowest priority, usually applications running in background, for example monitoring VBI transmissions. A proxy application running in user space will be necessary if multiple applications want to read from a device at this priority.                                        |
+---------------------------------+-----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_PRIORITY_INTERACTIVE``   | 2   |                                                                                                                                                                                                                                                                                 |
+---------------------------------+-----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_PRIORITY_DEFAULT``       | 2   | Medium priority, usually applications started and interactively controlled by the user. For example TV viewers, Teletext browsers, or just "panel" applications to change the channel or video controls. This is the default priority unless an application requests another.   |
+---------------------------------+-----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ``V4L2_PRIORITY_RECORD``        | 3   | Highest priority. Only one file descriptor can have this priority, it blocks any other fd from changing device properties. Usually applications which must not be interrupted, like video recording.                                                                            |
+---------------------------------+-----+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: enum v4l2\_priority

RETURN-VALUE

EINVAL
    The requested priority value is invalid.

EBUSY
    Another application already requested higher priority.
