V4L2\_SDR\_FMT\_CS8 ('CS08')
MANVOL
V4L2\_SDR\_FMT\_CS8
Complex signed 8-bit IQ sample
Description
===========

This format contains sequence of complex number samples. Each complex
number consist two parts, called In-phase and Quadrature (IQ). Both I
and Q are represented as a 8 bit signed number. I value comes first and
Q value after that.

**Byte Order..**

Each cell is one byte.

+--------------------------------------------------------------------------+
| start + 0:                                                               |
+--------------------------------------------------------------------------+
| start + 1:                                                               |
+--------------------------------------------------------------------------+
