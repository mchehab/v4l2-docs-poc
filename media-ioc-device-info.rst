ioctl MEDIA\_IOC\_DEVICE\_INFO
MANVOL
MEDIA\_IOC\_DEVICE\_INFO
Query device information
int
ioctl
int
fd
int
request
struct media\_device\_info \*
argp
Arguments
=========

``fd``
    File descriptor returned by ```open()`` <#media-func-open>`__.

``request``
    MEDIA\_IOC\_DEVICE\_INFO

``argp``

Description
===========

All media devices must support the ``MEDIA_IOC_DEVICE_INFO`` ioctl. To
query device information, applications call the ioctl with a pointer to
a MEDIA-DEVICE-INFO. The driver fills the structure and returns the
information to the application. The ioctl never fails.

+-----------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| char      | driver[16]        | Name of the driver implementing the media API as a NUL-terminated ASCII string. The driver version is stored in the driver\_version field.                                   |
|           |                   |                                                                                                                                                                              |
|           |                   | Driver specific applications can use this information to verify the driver identity. It is also useful to work around known bugs, or to identify drivers in error reports.   |
+-----------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| char      | model[32]         | Device model name as a NUL-terminated UTF-8 string. The device version is stored in the device\_version field and is not be appended to the model name.                      |
+-----------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| char      | serial[40]        | Serial number as a NUL-terminated ASCII string.                                                                                                                              |
+-----------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| char      | bus\_info[32]     | Location of the device in the system as a NUL-terminated ASCII string. This includes the bus type name (PCI, USB, ...) and a bus-specific identifier.                        |
+-----------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | media\_version    | Media API version, formatted with the ``KERNEL_VERSION()`` macro.                                                                                                            |
+-----------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | hw\_revision      | Hardware device revision in a driver-specific format.                                                                                                                        |
+-----------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | driver\_version   | Media device driver version, formatted with the ``KERNEL_VERSION()`` macro. Together with the driver field this identifies a particular driver.                              |
+-----------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32   | reserved[31]      | Reserved for future extensions. Drivers and applications must set this array to zero.                                                                                        |
+-----------+-------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct media\_device\_info

The serial and bus\_info fields can be used to distinguish between
multiple instances of otherwise identical hardware. The serial number
takes precedence when provided and can be assumed to be unique. If the
serial number is an empty string, the bus\_info field can be used
instead. The bus\_info field is guaranteed to be unique, but can vary
across reboots or device unplug/replug.

RETURN-VALUE
