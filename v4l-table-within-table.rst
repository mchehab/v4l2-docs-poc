V4l2 CID table example
======================

.. tabularcolumns:: |l|l|p{20cm}|
.. table::
   :class: description

   ==============================  ==============  =========
   ID                              Type            Definition
   ==============================  ==============  =========
   V4L2_CID_COLOR_KILLER           boolean         Enable the color killer (i. e. force a black & white image in case of a weak video signal).
   V4L2_CID_COLORFX                enum            Selects a color effect. The following values are defined:

                                                   ==============================  =====================
                                                   Value                           Definition
                                                   ==============================  =====================
                                                   V4L2_COLORFX_NONE               Color effect is disabled.
                                                   V4L2_COLORFX_ANTIQUE            An aging (old photo) effect.
                                                   V4L2_COLORFX_ART_FREEZE         Frost color effect.
                                                   V4L2_COLORFX_AQUA               Water color, cool tone.
                                                   V4L2_COLORFX_BW                 Black and white.
                                                   V4L2_COLORFX_EMBOSS             Emboss, the highlights and shadows replace light/dark
                                                                                   boundaries and low contrast areas are set to a gray background.
                                                   V4L2_COLORFX_GRASS_GREEN        Grass green.
                                                   V4L2_COLORFX_NEGATIVE           Negative.
                                                   V4L2_COLORFX_SEPIA              Sepia tone.
                                                   V4L2_COLORFX_SKETCH             Sketch.
                                                   V4L2_COLORFX_SKIN_WHITEN        Skin whiten.
                                                   V4L2_COLORFX_SKY_BLUE           Sky blue.
                                                   V4L2_COLORFX_SOLARIZATION       Solarization, the image is partially reversed in tone, only
                                                                                   color values above or below a certain threshold are inverted.
                                                   V4L2_COLORFX_SILHOUETTE         Silhouette (outline).
                                                   V4L2_COLORFX_VIVID              Vivid colors.
                                                   V4L2_COLORFX_SET_CBCR           The Cb and Cr chroma components are replaced by fixed coefficients
                                                                                   determined by V4L2_CID_COLORFX_CBCR control.
                                                   ==============================  =====================
   V4L2_CID_COLORFX_CBCR           integer         Determines the Cb and Cr coefficients for V4L2_COLORFX_SET_CBCR
                                                   color effect. Bits [7:0] of the supplied 32 bit value are interpreted as
                                                   Cr component, bits [15:8] as Cb component and bits [31:16] must be zero.
   V4L2_CID_AUTOBRIGHTNESS         boolean         Enable Automatic Brightness.
   ==============================  ==============  =========
