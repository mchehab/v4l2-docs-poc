ioctl VIDIOC\_G\_CROP, VIDIOC\_S\_CROP
MANVOL
VIDIOC\_G\_CROP
VIDIOC\_S\_CROP
Get or set the current cropping rectangle
int
ioctl
int
fd
int
request
struct v4l2\_crop \*
argp
int
ioctl
int
fd
int
request
const struct v4l2\_crop \*
argp
Arguments
=========

``fd``
    FD

``request``
    VIDIOC\_G\_CROP, VIDIOC\_S\_CROP

``argp``

Description
===========

To query the cropping rectangle size and position applications set the
type field of a v4l2\_crop structure to the respective buffer (stream)
type and call the ``VIDIOC_G_CROP`` ioctl with a pointer to this
structure. The driver fills the rest of the structure or returns the
EINVAL if cropping is not supported.

To change the cropping rectangle applications initialize the type and
V4L2-RECT substructure named c of a v4l2\_crop structure and call the
``VIDIOC_S_CROP`` ioctl with a pointer to this structure.

Do not use the multiplanar buffer types. Use
``V4L2_BUF_TYPE_VIDEO_CAPTURE`` instead of
``V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE`` and use
``V4L2_BUF_TYPE_VIDEO_OUTPUT`` instead of
``V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE``.

The driver first adjusts the requested dimensions against hardware
limits, IE the bounds given by the capture/output window, and it rounds
to the closest possible values of horizontal and vertical offset, width
and height. In particular the driver must round the vertical offset of
the cropping rectangle to frame lines modulo two, such that the field
order cannot be confused.

Second the driver adjusts the image size (the opposite rectangle of the
scaling process, source or target depending on the data direction) to
the closest size possible while maintaining the current horizontal and
vertical scaling factor.

Finally the driver programs the hardware with the actual cropping and
image parameters. ``VIDIOC_S_CROP`` is a write-only ioctl, it does not
return the actual parameters. To query them applications must call
``VIDIOC_G_CROP`` and VIDIOC-G-FMT. When the parameters are unsuitable
the application may modify the cropping or image parameters and repeat
the cycle until satisfactory parameters have been negotiated.

When cropping is not supported then no parameters are changed and
``VIDIOC_S_CROP`` returns the EINVAL.

+-------------+--------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| \_\_u32     | type   | Type of the data stream, set by the application. Only these types are valid here: ``V4L2_BUF_TYPE_VIDEO_CAPTURE``, ``V4L2_BUF_TYPE_VIDEO_OUTPUT`` and ``V4L2_BUF_TYPE_VIDEO_OVERLAY``. See ?.   |
+-------------+--------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| V4L2-RECT   | c      | Cropping rectangle. The same co-ordinate system as for V4L2-CROPCAP bounds is used.                                                                                                             |
+-------------+--------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

Table: struct v4l2\_crop

RETURN-VALUE
